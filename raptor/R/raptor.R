compileFun.raptoR <- function(f) {

  type <- typeof(f)
  if (type == "closure") {
	bc <- .Call('raptoR_compileFunction', PACKAGE = 'raptor', f)
    cfun <- .Internal(bcClose(formals(f), bc, environment(f)))
    attrs <- attributes(f)
    if (! is.null(attrs))
      attributes(cfun) <- attrs
    if (isS4(f))
      cfun <- asS4(cfun)
    cfun
  }
  else if (typeof(f) == "builtin" || type == "special")
    f
  else stop("cannot compile a non-function")
}

compile.raptoR <- function(f) {
  .Call('raptoR_compile', PACKAGE = 'reactor', f)
}


raptoR.env <- new.env()

raptoR.env$origCompile <- NULL
raptoR.env$origCmpfun <- NULL
raptoR.env$origTryCmpfun <- NULL

overrideCompiler.raptoR <- function(restore = FALSE, functions = NULL) {
  assignInNamespace <- function(x, value, ns) {
      ns <- asNamespace(ns)
      assign(x, value, envir = ns, inherits = FALSE)
      invisible(NULL)
  }

  if (is.null(raptoR.env$origCompile)) {
      raptoR.env$origCompile <- compiler:::compile
      raptoR.env$origCmpfun <- compiler:::cmpfun
      raptoR.env$origTryCmpfun <- compiler:::tryCmpfun
  }

  oldCompile <- compiler:::compile
  oldCmpfun <- compiler:::cmpfun
  oldTryCmpfun <- compiler:::tryCmpfun

  if (restore) {
    newCompile <- origCompile
    newCmpfun  <- origCmpfun
    newTryCmpfun <- origTryCmpfun
  } else if(!is.null(functions)) {
    newCompile <- functions$compile
    newCmpfun  <- functions$cmpfun
    newTryCmpfun <- functions$tryCmpfun
  } else {
    newCompile <- function(e, env = .GlobalEnv, options = NULL) {
      raptoR:::compile.raptoR(e)
    }
    newCmpfun <- function (f, options = NULL) {
      raptoR:::compileFun.raptoR(f)
    }
    newTryCmpfun <- function (f, options = NULL) {
      tryCatch(
        raptoR:::compileFun.raptoR(f),
        error = function(e) {
          print(paste0("failed to compile ",e))
          f
        })
    }
  }

  reassign <- function(old, name, new) {
    unlockBindingT <- base::unlockBinding;
    env <- environment(old);
    environment(new) <- env;
    unlockBindingT(name, env);
    assignInNamespace(name, new, ns="compiler");
    assign(name, new, envir=env);
    lockBinding(name, env);
  }

  functions <- c()
  functions$compile <- oldCompile
  functions$cmpfun  <- oldCmpfun
  functions$tryCmpfun <- oldTryCmpfun

  reassign(oldCompile, "compile", newCompile)
  reassign(oldCmpfun, "cmpfun", newCmpfun)
  reassign(oldTryCmpfun, "tryCmpfun", newTryCmpfun)

  functions
}

enableJIT.raptoR <- function(v = 2) {
  overrideCompiler.raptoR()
  enableJIT(v)
}

inspectPackage <- function(pkg) {
  filebase <- file.path(find.package(pkg), "R", pkg)

  printEl <- function(filebase) {
    env <- new.env()
    dbname <- paste0(filebase,".rdb")
    fun <- function(db) {
        vals <- db$vals
        vars <- db$vars
        svars <- sort(vars)
        for (j in seq_along(svars)) {
          i <- match(svars[j], vars)
          data <- lazyLoadDBfetch(vals[i][[1]], dbname, TRUE, db$envhook)
          print(paste0("-- ", vars[i], ":"))
          .Internal(inspect(data))
        }
    }
    lazyLoadDBexec(filebase, fun)
    env
  }
  methods <- printEl(filebase)
}


recompileAllPackages.raptoR <- function() {
  recompilePackage.raptoR(installed.packages(.Library)[,1])
}


recompilePackage.raptoR <- function(pkgs, useRaptor = TRUE) {

  unlazy <- function(filebase) {
    env <- new.env()
    dbname <- paste0(filebase,".rdb")
    fun <- function(db) {
        vals <- db$vals
        vars <- db$vars
        for (i in seq_along(vars)) {
          data <- lazyLoadDBfetch(vals[i][[1]], dbname, TRUE, db$envhook)
          if (typeof(data) == "closure") {
            attrs <- attributes(data)
            s4_   <- isS4(data)
            # Remove bytecode
            # this looks stupid but it works since body<-( is not the same as body( for bytecode
            body(data) <- body(data)
            if (! is.null(attrs))
              attributes(data) <- attrs
            if (s4_)
              data <- asS4(data)
          }
          assign(vars[i], data, envir=env)
        }
    }
    lazyLoadDBexec(filebase, fun)
    env
  }

  methods <- new.env()

  for (pkg in pkgs) {
    filebase <- file.path(find.package(pkg), "R", pkg)
    .Internal(lazyLoadDBflush(paste0(filebase, ".rdb")))
    if (file.exists(paste0(filebase,".rdx")))
      assign(pkg, unlazy(filebase), methods)
  }

  for (pkg in pkgs) {
    if (exists(pkg, methods)) {
      print(paste0("compiling ", pkg))
      filebase <- file.path(find.package(pkg), "R", pkg)
      oldCmpP <- compiler:::compilePKGS(1)
      if (useRaptor)
        oldCompiler <- overrideCompiler.raptoR()
      tools:::makeLazyLoadDB(get(pkg, methods), filebase, TRUE)
      if (useRaptor)
        overrideCompiler.raptoR(functions = oldCompiler)
      compiler:::compilePKGS(oldCmpP)
      .Internal(lazyLoadDBflush(paste0(filebase, ".rdb")))
    }
  }

  NULL
}
