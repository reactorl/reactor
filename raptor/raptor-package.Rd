\name{raptor-package}
\alias{raptor-package}
\alias{raptor}
\docType{package}
\title{
\packageTitle{raptor}
}
\description{
\packageDescription{raptor}
}
\details{

The DESCRIPTION file:
\packageDESCRIPTION{raptor}
\packageIndices{raptor}
~~ An overview of how to use the package, including the most important ~~
~~ functions ~~
}
\author{
\packageAuthor{raptor}

Maintainer: \packageMaintainer{raptor}
}
\references{
~~ Literature or other references for background information ~~
}
~~ Optionally other standard keywords, one per line, from file KEYWORDS in ~~
~~ the R documentation directory ~~
\keyword{ package }
\seealso{
~~ Optional links to other man pages, e.g. ~~
~~ \code{\link[<pkg>:<pkg>-package]{<pkg>}} ~~
}
\examples{
%% ~~ simple examples of the most important functions ~~
}
