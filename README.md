# README #

### Building raptor & flair ###

First get the reactor repo from bitbucket:

```
#!bash
git clone https://bitbucket.org/reactorl/reactor.git
cd reactor
```

Also install the GNU-R submodule, and update it so that R will be built too, then get back to master branch:

```
#!bash
git submodule update --init
```

Now invoke cmake and then build gnur, and the packages.

```
#!bash
cmake .
make gnur
make reactor_package
```

Note that when doing this for the first time, it will take a few moments as GNU-R will be built too.