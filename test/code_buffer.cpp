#include "tests.h"

namespace reactor {

void test_codebuffer(SEXP arg) {
  SEXP code = GenericVector(arg)[0];
  CodeBuffer c(code);
  assert_eq(2, c.constants.indexOf(Rf_install("a")),
            "codebuffer expected to have sym a at pos 2");
}
}
