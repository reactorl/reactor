require(compiler)
require(reactor)

sample_asts <- list(
  quote(if(a) 1),
  quote(if(a) a else a),
  quote((if(a) a else a) + 1),
  quote(f(a)),
  quote(for(i in a) (a + 1)))

sample_bc <- lapply(sample_asts, compile)
sample_bc_R <- lapply(sample_asts, compile.raptoR)

sample_cfg <- list(
  function(a) if (a) 1 else 3,
  function(a, b) for(i in a) if (b) c(),
  function() { b <- 2; a <- 1; b; a; b; }
)

sample_cfg <- lapply(sample_cfg, cmpfun)

fixtures <- list(
  compile(quote({
    a <- 1L
    b <- 2
  })),
  sample_bc,
  sample_bc_R,
  sample_cfg
)
