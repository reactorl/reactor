#include "tests.h"

#include "flair/scheduler.h"
#include "flair/analysis/PcMap.h"
#include "flair/optimization/Dummy.h"

namespace reactor {

class PcMapperTest  {
public:
  static void test(CodeBuffer & code) {
    Scheduler s(code);

    optimization::Dummy d(s);
    d.run(s.pcMapper());

    s.schedule();
    s.invoke();

    const analysis::PcMapper &map = s.pcMapper();

    for (size_t i = 0; i < map.size(); ++i) {
      size_t pc = map.pcOf(i);
      assert_(map.idxOf(pc) == i);
    }
    assert_(map.idxOf(1) == 0);
    size_t last_pc = map.pcOf(map.size() - 1);
    assert_(last_pc = map.pcOf(map.idxOf(last_pc)));
    assert_(map.idxOf(1) == 0);
    assert_(last_pc = map.pcOf(map.idxOf(last_pc)));

    size_t apc = map.pcOf(map.size() / 2);
    assert_(apc = map.pcOf(map.idxOf(apc)));

  }
};

void test_pc(SEXP arg) {
  auto arg1 = GenericVector(arg)[0];
  for (auto c : GenericVector(arg1)) {
    CodeBuffer code(c);
    PcMapperTest::test(code);
  }
}

}
