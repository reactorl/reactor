#include "tests.h"

#include "flair/printer.h"
#include "flair/scheduler.h"
#include "flair/optimizations.h"
#include "flair/optimization/Test.h"

namespace reactor {

void test_analysis(SEXP fixtures) {
  for (auto f : GenericVector(GenericVector(fixtures)[0])) {
    CodeBuffer cb(f);

    Printer::execute(cb);

    Scheduler scheduler(cb);
    optimization::TestOpt lv(scheduler);
    scheduler.schedule();

    scheduler.invoke();

    std::cout << std::endl;
  }
}
}
