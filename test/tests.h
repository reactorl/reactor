#ifndef TESTS_H
#define TESTS_H

#include <iostream>

#include "r/vector.h"
#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "assert.h"

namespace reactor {

extern "C" {
extern void test_codebuffer(SEXP);
extern void test_pc(SEXP);
extern void test_analysis(SEXP);
}
}

#endif
