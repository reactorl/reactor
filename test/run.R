source('test/fixtures.R')
source('test/tests.R')

dyn.load('libbasetests.so')

for (test in tests) {
  a <- test[2][[1]]
  if (typeof(a) != "list")
    a <- as.list(a)

  .Call(test[[1]], test[2][[1]])
}

"done"
