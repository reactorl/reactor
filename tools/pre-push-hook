#!/bin/bash

protected_branch='master'

policy='[Policy] Never force push or delete the '$protected_branch' branch! (Prevented with pre-push hook.)'

current_branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')

push_command=$(ps -ocommand= -p $PPID)

is_destructive='force|delete|\-f'

will_remove_protected_branch=':'$protected_branch

do_exit(){
  echo $policy
  exit 1
}


if [[ $push_command =~ $is_destructive ]] && [ $current_branch = $protected_branch ]; then
  do_exit
fi

if [[ $push_command =~ $is_destructive ]] && [[ $push_command =~ $protected_branch ]]; then
  do_exit
fi

if [[ $push_command =~ $will_remove_protected_branch ]]; then
  do_exit
fi

unset do_exit

if [ $current_branch = $protected_branch ] || [[ $push_command =~ $protected_branch ]]; then

  SCRIPT=$(readlink -f "$0")
  SCRIPTPATH=$(dirname "$SCRIPT")

  if test -f $SCRIPTPATH/../local/hook-config.local; then 
    source $SCRIPTPATH/../local/hook-config.local;
   else
    CMAKE_BUILD_DIR="..";
  fi
  cd $SCRIPTPATH/$CMAKE_BUILD_DIR

  git status --porcelain | grep "^[ \?]" | grep -v "^ M gnur"
  if test "$?" -eq 0; then
    echo "You have unstaged changes. Precommit hook can only run in a clean repository."
    echo "Please stash unstaged changes and files you don't want to commit using"
    echo "git stash"
    exit 5
  fi

  $SCRIPTPATH/all-clang-format

  if test "$?" -ne 0; then
    exit 3
  fi


  CMAKE_BUILD_DIR="..";

  if test -f $SCRIPTPATH/hook-config.local; then 
    source $SCRIPTPATH/hook-config.local;
  fi

  cd $SCRIPTPATH/$CMAKE_BUILD_DIR


  make -j8 test_all

  if test "$?" -ne 0; then
    echo "make test_all failed. Please fix first."
    exit 4
  fi

fi

exit 0
