#!/bin/bash

export R_DEFAULT_PACKAGES="reactor,compiler,methods,datasets,utils,grDevices,graphics,stats"
export R_LIBS_USER="$1"
$2 "${@:3}"
