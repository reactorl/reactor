#ifndef OPTIMIZATIONS_H
#define OPTIMIZATIONS_H

#include <list>

#include "scheduler.h"

namespace reactor {

/** Base class for all optimizations.

  The class provides shorthand access to the scheduler's patch so that each
  optimization can simply modify the code.
 */
class Optimization : public Scheduler::Pass {
public:
protected:
  /** Returns the code buffer object the optimization operates on.
   */
  CodeBuffer &code() { return scheduler().code(); }

  /** Creates the optimization and adds it to the specified scheduler.
   */
  Optimization(Scheduler &scheduler) : Scheduler::Pass(scheduler) {
    scheduler.optimizations_.push_back(this);
  }

  /** Marks instruction with given pc to be erased.
   */
  void erase(size_t pc) { scheduler().patch_->erase(pc); }

  /** Returns a code chunk that will be inserted at the given pc. The optimizer
   * may instructions to it.
   */
  CodeBuffer::Code &insertChunk(size_t pc) {
    return scheduler().patch_->insertChunk(pc);
  }

  /** Adds a constant to the patched code and returns its index.

    Note that compared to the instructions, constants are always added
    immediately. Should this requirement ever pose a problem, we can easily
    change it and add all the constants at the end too and give offsetted
    indices here. It's just this is easier now.
   */
  size_t addConstant(SEXP constant) {
    return scheduler().patch_->addConstant(constant);
  }

  /** Converts the given chunk local address to a patch chunk local address.

    Chunk local addresses are larger than the original code size, therefore the
    original code size is just added to the address and then returned.
   */
  size_t chunkLocal(size_t pc) { return code().code.size() + pc; }

  /** Applies the changes by the optimizations so far directly.

    Note that this triggers immediate recomputation of all analyses required by
    the current patch and should therefore be only used if the optimizations
    needs to see its results in the middle of the process.
   */
  void applyChanges() { scheduler().applyChanges(); }

  /** Returns the patch associated with the scheduler.
   */
  Patch &patch() { return *scheduler().patch_; }
};

// --------------------------------------------------------------------------------------------------------------------

/** Per-instruction level optimizer class.

  This is base class for optimizations that work on per instruction base. These
  must also inherit from a dispatcher, which the Optimizer uses to dispatch on
  the instructions. These handlers are then responsible for performing the
  optimizations.

*/
template <typename OPTIMIZER> class Optimizer : public Optimization {
public:
  /** Invokes the optimizer.

    The optimizer will be dispatched for each valid instruction in the original
    code (a valid instruction is an instruction not scheduled for deletion).
   */
  void invoke() override {
    OPTIMIZER *optimizer = reinterpret_cast<OPTIMIZER *>(this);
    CodeBuffer &cb = code();
    for (pc_ = 1; pc_ < cb.code.size(); pc_ += cb.code.instructionSize(pc_))
      if (patch().isValid(pc_))
        optimizer->dispatch(scheduler(), cb, pc_);
  }

protected:
  Optimizer(Scheduler &scheduler) : Optimization(scheduler) {}

  /** Returns the pc of the current instruction.
   */
  size_t pc() const { return pc_; }

private:
  size_t pc_;
};

} // namespace reactor

#endif // OPTIMIZATIONS_H
