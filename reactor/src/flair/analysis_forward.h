#ifndef ANALYSIS_FORWARD_H
#define ANALYSIS_FORWARD_H

#include "analysis_abstract.h"
#include "analysis/MergePoints.h"

namespace reactor {

/** Forward analysis Control flow.

  Instruction fetching for forward analysis.

*/

template <typename ANALYSIS> class Analysis::ForwardControlFlow {
public:
  std::set<size_t> start(const analysis::MergePoints &mergePoints) const {
    return {1};
  }

  void scheduleNext(ANALYSIS *analysis) {
    controlFlow_.dispatch(*analysis, analysis->scheduler().code(),
                          analysis->pc());
  }

  bool isEntryPoint(const analysis::MergePoints &mergePoints, size_t pc) const {
    return pc == 1;
  }

  analysis::RCursor reverseIterator(const analysis::PcMapper &map,
                                    size_t pc) const {
    return map.riterator(pc);
  }
  analysis::Cursor iterator(const analysis::PcMapper &map, size_t pc,
                            size_t target) const {
    return map.iterator(pc, target);
  }

private:
  class ControlFlow : public ins::Dispatcher<ControlFlow, ANALYSIS> {
    typedef ins::Dispatcher<ControlFlow, ANALYSIS> Super;

  public:
    using typename Super::Driver;
    static void initialize() {
      Super::template bind<Jump>(&ControlFlow::jump);
      Super::template bind<ConditionalJump1>(&ControlFlow::conditionalJump1);
      Super::template bind<ConditionalJump2>(&ControlFlow::conditionalJump2);
      Super::template bind<STARTFOR_OP::Predicate>(&ControlFlow::STARTFOR);
      Super::template bind<RETURN_OP::Predicate>(&ControlFlow::RETURN);
      Super::template bind<RETURNJMP_OP::Predicate>(&ControlFlow::RETURNJMP);
      Super::template bind<Instruction::Predicate>(&ControlFlow::instruction);
    }

  private:
    /** Callback to driver to determine whether it can decide if the jump will
      be taken or not.

      If the jump can be resolved, we can avoid cloning the current state as
      well as exploring the apparently dead branch.
     */
    void conditionalJump(Driver &driver, encoding::OpcodeOnly const &ins,
                         size_t target) {
      driver.scheduleBranch(target, driver.jumpHints(driver.pc()));
      instruction(driver, ins);
    }

    /** Uncodnitional jump is always taken.
     */
    void jump(Driver &driver, encoding::R1 const &ins) {
      driver.schedule(ins.arg1);
    }

    /** Conditional jump can be taken always, sometimes, or never.
      */
    void conditionalJump1(Driver &driver, encoding::R1 const &ins) {
      conditionalJump(driver, ins, ins.arg1);
    }

    /** Conditional jump can be taken always, sometimes, or never.
      */
    void conditionalJump2(Driver &driver, encoding::R2 const &ins) {
      conditionalJump(driver, ins, ins.arg2);
    }

    void STARTFOR(Driver &driver, encoding::R3 const &ins) {
      driver.schedule(ins.arg3);
    }

    /** If the instruction is return, the next instruction should not be
     * scheduled.
     */
    void RETURN(Driver &driver, RETURN_OP::Encoding const &ins) {
      // pass
    }

    /** If the instruction is return, the next instruction should not be
     * scheduled.
     */
    void RETURNJMP(Driver &driver, RETURNJMP_OP::Encoding const &ins) {
      // pass
    }

    /** If the instruction is not jump, or it is not return, the next
     * instruction should be immediately scheduled.
     */
    void instruction(Driver &driver, encoding::OpcodeOnly const &ins) {
      driver.schedule(
          driver.pc() +
          driver.scheduler().code().code.instructionSize(driver.pc()));
    }
  };

  ControlFlow controlFlow_;
};

} // namespace reactor

#endif // ANALYSIS_FORWARD_H
