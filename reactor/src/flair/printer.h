#ifndef PRINTER
#define PRINTER

// TODO unify the printer classes

#include <iostream>
#include <iomanip>

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"
#include "flair/pass.h"
#include "r/vector.h"
#include "bytecode/printer.h"

namespace reactor {

class Printer {

public:
  static void execute(std::ostream &stream, CodeBuffer const &code) {
    BytecodePrinter p(stream);
    // ok here, perhaps we need const cursors
    Pass::Sequential pass(code);
    pass.execute(p);
    stream << "Constants:" << std::endl;
    for (size_t i = 0; i < code.constants.size(); ++i) {
      stream << std::setw(5) << i << " ";
      SEXP cval = code.constants[i];
      switch (TYPEOF(cval)) {
      case REALSXP: {
        RealVector v(cval);
        stream << "double vector (" << v.size() << ") ";
        for (auto j : v)
          stream << j << ", ";
        break;
      }
      case INTSXP: {
        IntVector v(cval);
        stream << "integer vector (" << v.size() << ") ";
        for (auto j : v)
          stream << j << ", ";
        break;
      }
      case SYMSXP: {
        stream << "symbol " << PRINTNAME(cval);
        break;
      }
      case LANGSXP: {
        stream << "language";
        // TODO
        break;
      }
      case BCODESXP: {
        stream << "bytecode";
        // TODO
        break;
      }
      }
      stream << std::endl;
    }
  }

  static void execute(CodeBuffer const &code) { execute(std::cout, code); }
};

} // namespace flair

#endif // PRINTER
