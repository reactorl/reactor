#ifndef INLINER_H
#define INLINER_H

#include <climits>

#include "reactor.h"

#include "bytecode/dispatcher.h"
#include "flair/pass.h"
#include "call_site_discovery.h"
#include "formals.h"
#include "raptor/fixup.h"
#include "local_vars.h"

namespace reactor {

class JumpCorrector : public ins::Dispatcher<JumpCorrector, Pass::Sequential> {
public:
  JumpCorrector(size_t jumpMin, size_t jumpMax, size_t diff)
      : jumpMin_(jumpMin), jumpMax_(jumpMax), diff_(diff) {}

  JumpCorrector(size_t jumpMin, size_t diff)
      : jumpMin_(jumpMin), jumpMax_(UINT32_MAX), diff_(diff) {}

  static void initialize() {
    bind<GOTO_OP::Predicate>(&JumpCorrector::jump);
    bind<BRIFNOT_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<AND1ST_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<OR1ST_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTSUBSET_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTSUBSET2_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTC_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTFOR_OP::Predicate>(&JumpCorrector::conditionalJump3);
    bind<STEPFOR_OP::Predicate>(&JumpCorrector::jump);
    bind<STARTSUBASSIGN_N_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTSUBASSIGN2_N_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTSUBSET_N_OP::Predicate>(&JumpCorrector::conditionalJump);
    bind<STARTSUBSET2_N_OP::Predicate>(&JumpCorrector::conditionalJump);
  }

private:
  size_t fixup_(size_t old) {
    if (old < jumpMin_ or old >= jumpMax_)
      return old;
    return old + diff_;
  }

  void jump(Driver &d, encoding::R1 const &ins) {
    const_cast<encoding::R1 &>(ins).arg1 = fixup_(ins.arg1);
  }

  void conditionalJump(Driver &d, encoding::R2 const &ins) {
    const_cast<encoding::R2 &>(ins).arg2 = fixup_(ins.arg2);
  }

  void conditionalJump3(Driver &d, encoding::R3 const &ins) {
    const_cast<encoding::R3 &>(ins).arg3 = fixup_(ins.arg3);
  }

  const size_t jumpMin_;
  const size_t jumpMax_;
  const int diff_;
};

class StaticInliner : public ins::Dispatcher<StaticInliner, Pass::Sequential> {

public:
  static void inlineCallsite(CodeBuffer &target, Callsite2 const &callsite,
                             SEXP callee) {
    CodeBuffer cb(callee);
    StaticInliner inliner(target, callsite, callee);
    // merge the constant pools of the caller and callee
    inliner.mergeConstantPools(cb);

    // Mangle the callee's local variables
    inliner.mangleLocalVariables(cb);

    // remove the old callsite and insert the argument stores and default values
    inliner.prepareCallsite();

    // inline the code from the callee
    Pass::Sequential(cb).execute(inliner);
    // remove the call instruction and perform the fixup
    inliner.fixup();
  }

  static void initialize() {
    bind<RETURN_OP::Predicate>(&StaticInliner::RETURN);
    bind<GOTO_OP::Predicate>(&StaticInliner::jump);
    bind<BRIFNOT_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<AND1ST_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<OR1ST_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTSUBSET_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTSUBSET2_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTC_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTFOR_OP::Predicate>(&StaticInliner::conditionalJump3);
    bind<STEPFOR_OP::Predicate>(&StaticInliner::jump);
    bind<STARTSUBASSIGN_N_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTSUBASSIGN2_N_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTSUBSET_N_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<STARTSUBSET2_N_OP::Predicate>(&StaticInliner::conditionalJump);
    bind<GETVAR_OP::Predicate>(&StaticInliner::getVariable);
    bind<GETVAR_MISSOK_OP::Predicate>(&StaticInliner::getVariable);
    bind<SETVAR_OP::Predicate>(&StaticInliner::setVariable);
    bind<STARTASSIGN_OP::Predicate>(&StaticInliner::complexAssignment);
    bind<ENDASSIGN_OP::Predicate>(&StaticInliner::complexAssignment);
    bind<InstructionR0::Predicate>(&StaticInliner::R0);
    bind<InstructionR1::Predicate>(&StaticInliner::R1);
    bind<InstructionR2::Predicate>(&StaticInliner::R2);
    bind<InstructionR3::Predicate>(&StaticInliner::R3);
    bind<InstructionR4::Predicate>(&StaticInliner::R4);
  }

private:
  StaticInliner(CodeBuffer &target, Callsite2 const &callsite, SEXP callee)
      : target_(target), callsite_(callsite), formals_(callee), returnFixup_(),
        returnLabel_(returnFixup_.addLabel()) {}

  void prepareCallsite() {
    // match arguments for the call
    matchArguments();
    // get rid of GETFUN_OP instruction
    diff_ = 0;
    target_.code.erase(callsite_.getfun(), diff_);
    // convert all arguments into stack loads
    for (size_t i = 0; i < callsite_.arguments(); ++i)
      inlineArgument(i);
    // store the arguments
    storeArguments();
    // emit default values for remaining ones (if any)
    storeDefaults();
    // set the pc offset to the CALL_OP instruction beginning where the inlined
    // code will be added
    pcOffset_ = callsite_.call() + diff_;
  }

  void matchArguments() {
    formalNames_.insert(formalNames_.begin(), formals_.size(), false);
    // attach names to the actual arguments where known - first pass when
    // keyword arguments are matches
    argNames_.resize(callsite_.arguments());
    for (size_t i = 0; i < callsite_.arguments(); ++i) {
      SEXP name = callsite_.name(i, target_);
      argNames_[i] = name;
      if (name != nullptr)
        formalNames_[formals_.getIndex(name)] = true;
    }
    // second pass, match arguments positionally
    size_t fi = 0;
    for (size_t i = 0; i < argNames_.size(); ++i) {
      while (fi < formals_.size() and formalNames_[fi] == true)
        ++fi;
      if (fi == formals_.size())
        break;
      if (argNames_[i] == nullptr) {
        argNames_[i] = formals_.name(fi);
        formalNames_[fi] = true;
        while (fi < formals_.size() and formalNames_[fi] == true)
          ++fi;
      }
    }
    // we should now have names for all arguments
  }

  /** Changes the code dealing with the index-th argument so that it is
    compatible with the inlined function.

    Translates arguments pushing to stack pushing and removes all tags.
   */
  void inlineArgument(size_t index) {
    size_t pc = callsite_.argument(index);
    switch (target_.code.opcode(pc, diff_)) {
    case Opcode::PUSHARG_OP:
      target_.code.erase(pc, diff_);
      break;
    case Opcode::PUSHCONSTARG_OP: {
      PUSHCONSTARG_OP::Encoding const &ins =
          target_.code.get<PUSHCONSTARG_OP::Encoding>(pc, diff_);
      target_.code.replace(pc, diff_, LDCONST_OP::create(ins.arg1));
      break;
    }
    case Opcode::PUSHTRUEARG_OP:
      target_.code.replace(pc, diff_, LDTRUE_OP::create());
      break;
    case Opcode::PUSHFALSEARG_OP:
      target_.code.replace(pc, diff_, LDFALSE_OP::create());
      break;
    case Opcode::PUSHNULLARG_OP:
      target_.code.replace(pc, diff_, LDNULL_OP::create());
      break;
    case Opcode::MAKEPROM_OP: {
      MAKEPROM_OP::Encoding const &ins =
          target_.code.get<MAKEPROM_OP::Encoding>(pc, diff_);
      target_.code.replace(pc, diff_, CREATE_PROMISE_OP::create(ins.arg1));
      break;
    }
    default:
      UNREACHABLE;
    }
    if (callsite_.isKeywordArgument(index))
      target_.code.erase(callsite_.name(index), diff_);
  }

  void storeArguments() {
    for (size_t i = argNames_.size() - 1; i < argNames_.size(); --i) {
      SEXP name = argNames_[i];
      target_.code.insert(
          callsite_.call(), diff_,
          SETVAR_OP::create(convertVariable(target_.constants.add(name))));
      target_.code.insert(callsite_.call(), diff_, POP_OP::create());
    }
  }

  void storeDefaults() {
    for (size_t i = 0; i < formalNames_.size(); ++i)
      if (not formalNames_[i] and formals_.hasDefaultValue(i)) {
        SEXP val = formals_.defaultValue(i);
        switch (TYPEOF(val)) {
        case LGLSXP: {
          LogicalVector v(val);
          if (v.size() == 1) {
            if (v[0])
              target_.code.insert(callsite_.call(), diff_, LDTRUE_OP::create());
            else
              target_.code.insert(callsite_.call(), diff_,
                                  LDFALSE_OP::create());
            break;
          }
        }
        case INTSXP:
        case REALSXP:
        case CPLXSXP:
        case STRSXP:
          target_.code.insert(callsite_.call(), diff_,
                              LDCONST_OP::create(target_.constants.add(val)));
          break;
        case NILSXP:
          assert_(val == R_NilValue);
          target_.code.insert(callsite_.call(), diff_, LDNULL_OP::create());
          break;
        default:
          // anything else is a promise, so get the bytecode SEXP, store as
          // constant and make promise out of it
          target_.code.insert(callsite_.call(), diff_,
                              CREATE_PROMISE_OP::create(target_.constants.add(
                                  compileExpression(val))));
          break;
        }
        target_.code.insert(
            callsite_.call(), diff_,
            SETVAR_OP::create(target_.constants.add(formals_.name(i))));
        target_.code.insert(callsite_.call(), diff_, POP_OP::create());
      }
  }

  void fixup() {
    returnFixup_.setLabel(returnLabel_,
                          target_.code.erase(callsite_.call(), diff_));
    returnFixup_.apply(target_);
    // now we must fixup all branches before and after
    // FIXME this only works with arguments to a single call being
    // sequentially stored, while this is the case for the code right from
    // the compiler, it will likely stop holding after any changes. But we
    // need to lay down how to manage code updates and locations before that
    JumpCorrector jc(callsite_.call(), diff_);
    Pass::Sequential(target_, 1, pcOffset_).execute(jc);
    Pass::Sequential(target_, callsite_.call() + diff_).execute(jc);
  }

  /** Creates new temporary name and returns its index in the caller's constant
     pool.
    */
  size_t newTemporary() {
    std::string name = STR("*tmp*" << target_.constants.size());
    return target_.constants.add(getOrCreateSymbol(name));
  }

  /** Mangles callee's local variables.

    Any variables that are both in caller and callee must be mangled. This might
    be a problem for any variables that functions callee calls into superassign
    because the mangling changes the superassignment targets.

    FIXME correct this.

    FIXME also correct the fact that now all local variables are assumed to be
    valid throughout the callee's code.
   */
  void mangleLocalVariables(CodeBuffer const &calleeCode) {
    LocalVariables::Result *callee = LocalVariables::execute(calleeCode);
    LocalVariables::Result *caller = LocalVariables::execute(target_);
    // get the intersection of local variables
    callee->intersectLocalsWith(*caller);
    // for each local variable, create a new temporary
    for (SEXP s : callee->locals())
      variables_[target_.constants.indexOf(s)] = newTemporary();
  }

  /** Merges the constant pools of the caller and callee instructions. All
   * constants from callee's constant pool are added to the caller's constant
   * pool and their incides are recorded in the translation vector. This is
   * useful when any of the callee's constants is already present in caller to
   * prevent duplication.
   */
  void mergeConstantPools(CodeBuffer const &callee) {
    for (SEXP i : callee.constants)
      constantIndices_.push_back(target_.constants.add(i));
  }

  OpcodeRepr convertConstant(OpcodeRepr old) { return constantIndices_[old]; }

  OpcodeRepr convertAddress(OpcodeRepr old) {
    // subtract -1 for the bytecode version identifier in callee's code
    return old + pcOffset_ - 1;
  }

  /** Converts constant index for local variables to the mangled names. The old
     index must be already converted constant index, i.e. index to the target's
     merged constant pool.
    */
  OpcodeRepr convertVariable(OpcodeRepr old) {
    auto iter = variables_.find(old);
    if (iter != variables_.end())
      return iter->second;
    else
      return old;
  }

  template <typename ENCODING> size_t insertAtCall(ENCODING const &ins) {
    return target_.code.insert(callsite_.call(), diff_, ins);
  }

  void RETURN(Driver &d, RETURN_OP::Encoding const &ins) {
    if (not d.isLast())
      returnFixup_.add(insertAtCall(GOTO_OP::create(returnLabel_)));
  }

  void jump(Driver &d, encoding::R1 const &ins) {
    insertAtCall(encoding::R1(ins.opcode, convertAddress(ins.arg1)));
  }

  void conditionalJump(Driver &d, encoding::R2 const &ins) {
    insertAtCall(encoding::R2(ins.opcode, convertConstant(ins.arg1),
                              convertAddress(ins.arg2)));
  }

  void conditionalJump3(Driver &d, encoding::R3 const &ins) {
    insertAtCall(encoding::R3(ins.opcode, convertConstant(ins.arg1),
                              convertConstant(ins.arg2),
                              convertAddress(ins.arg3)));
  }

  void R0(Driver &d, encoding::R0 const &ins) { insertAtCall(ins); }

  void R1(Driver &d, encoding::R1 const &ins) {
    insertAtCall(encoding::R1(ins.opcode, convertConstant(ins.arg1)));
  }

  void R2(Driver &d, encoding::R2 const &ins) {
    insertAtCall(encoding::R2(ins.opcode, convertConstant(ins.arg1),
                              convertConstant(ins.arg2)));
  }

  void R3(Driver &d, encoding::R3 const &ins) {
    insertAtCall(encoding::R3(ins.opcode, convertConstant(ins.arg1),
                              convertConstant(ins.arg2),
                              convertConstant(ins.arg3)));
  }

  void R4(Driver &d, encoding::R4 const &ins) {
    insertAtCall(encoding::R4(
        ins.opcode, convertConstant(ins.arg1), convertConstant(ins.arg2),
        convertConstant(ins.arg3), convertConstant(ins.arg4)));
  }

  void getVariable(Driver &d, encoding::R1 const &ins) {
    insertAtCall(
        encoding::R1(ins.opcode, convertVariable(convertConstant(ins.arg1))));
  }

  void setVariable(Driver &d, encoding::R1 const &ins) {
    insertAtCall(
        encoding::R1(ins.opcode, convertVariable(convertConstant(ins.arg1))));
  }

  void complexAssignment(Driver &d, encoding::R1 const &ins) {
    insertAtCall(
        encoding::R1(ins.opcode, convertVariable(convertConstant(ins.arg1))));
  }

  std::vector<SEXP> argNames_;
  std::vector<bool> formalNames_;
  CodeBuffer &target_;
  Callsite2 const &callsite_;
  Formals const formals_;
  int diff_;

  size_t pcOffset_;
  std::vector<size_t> constantIndices_;
  std::map<size_t, size_t> variables_;
  Fixup returnFixup_;
  const Fixup::Label returnLabel_;
};

} // namespace flair

#endif // INLINER_H
