#ifndef OPT_CONSTANTPROPAGATION_H
#define OPT_CONSTANTPROPAGATION_H

#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"

#include "flair/analysis/ConstantPropagation.h"

#include "flair/optimizations.h"

namespace reactor {
namespace optimization {

class ConstantPropagation
    : public Optimizer<ConstantPropagation>,
      public ins::Dispatcher<ConstantPropagation, Scheduler> {
public:
  typedef analysis::ConstantPropagation::State State;

  char const *name() const override { return "ConstantPropagation"; }

  static void initialize() {
    bind<UnaryOperator>(&ConstantPropagation::unaryOperator);
    bind<BinaryOperator>(&ConstantPropagation::binaryOperator);

    bind<GETVAR_OP::Predicate>(&ConstantPropagation::GETVAR_OP);
  }

  ConstantPropagation(Scheduler &scheduler)
      : Optimizer<ConstantPropagation>(scheduler) {}

protected:
  friend class Scheduler;

  void initializeScheduling() override {
    require(scheduler().constantPropagation());
    preserve(scheduler().mergePoints());
    preserve(scheduler().constantPropagation());
  }

private:
  void unaryOperator(Driver &driver, encoding::R1 const &ins) {
    // get outgoing state
    State state = driver.constantPropagation().state(
        pc() + Instruction::get(ins.opcode).size());
    // if the output is constant, replace with LD_CONST
    if (state[1].isConstant()) {
      // add the constant to the constant pool
      size_t idx = addConstant(state[1]);
      // erase old instruction
      erase(pc());
      // insert the LDCONST_OP and POP_OP for the argument of the erased
      // operator
      CodeBuffer::Code &chunk = insertChunk(pc());
      chunk.append(POP_OP::create());
      chunk.append(LDCONST_OP::create(idx));
    }
  }

  void binaryOperator(Driver &driver, encoding::R1 const &ins) {
    // get outgoing state
    State state = driver.constantPropagation().state(
        pc() + Instruction::get(ins.opcode).size());
    // if the output of the binary instruction was a constant, replace with
    // LD_CONST
    if (state[1].isConstant()) {
      // add the constant to the constant pool
      size_t idx = addConstant(state[1]);
      // erase old binary operator
      erase(pc());
      // insert the LDCONST_OP and POP_OPs for the arguments to the erased
      // binary
      CodeBuffer::Code &chunk = insertChunk(pc());
      chunk.append(POP_OP::create());
      chunk.append(POP_OP::create());
      chunk.append(LDCONST_OP::create(idx));
    }
  }

  void GETVAR_OP(Driver &driver, GETVAR_OP::Encoding const &ins) {
    State::Value const &v =
        driver.constantPropagation().state(pc())[ins.constant1(code())];
    if (v.isConstant()) {
      size_t idx = addConstant(v);
      erase(pc());
      insertChunk(pc()).append(LDCONST_OP::create(idx));
    }
  }
};

} // namespace optimization
} // namespace reactor

#endif // OPT_CONSTANTPROPAGATION_H
