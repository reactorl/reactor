#ifndef OPT_INLINING_H
#define OPT_INLINING_H

#include "bytecode/opcodes.h"
#include "bytecode/instruction.h"
#include "bytecode/rbc.h"

#include "r/closure.h"

#include "flair/optimizations.h"
#include "flair/analysis/CallSiteDiscovery.h"
#include "flair/analysis/FrameVariables.h"

namespace reactor {
namespace optimization {

class Inlining : public Optimization {
public:
  char const *name() const override { return "Inlining"; }

  Inlining(Scheduler &scheduler) : Optimization(scheduler) {}

protected:
  friend class Scheduler;

  void initializeScheduling() override {
    require(scheduler().callSiteDiscovery());
    require(scheduler().frameVariables());
  }

private:
  void inlineCallsite(analysis::CallSite const &callsite,
                      r::Closure const &callee) {
    // create the matched callsite first
    MatchedCallSite mcs(code(), callsite, callee);
    // now for each argument, replace the argument storing with pushing the
    // argument on stack
    ArgumentLoader::inlineArguments(*this, callsite);
    // deal with missing arguments and default values
    if (mcs.missingArguments().size() > 0)
      NOT_IMPLEMENTED;
    // create the new environment at callsite, store the variables in it when
    // replacing the CALL_OP

    // inline the callee in place of CALL_OP

    // get

    // Inliner::inlineCall(*this, callsite.call(), calleeCode);

    // remove the frame in place of CALL_OP
  }

  /** Deals with callsite arguments and converts them one by one to stack
   * pushes.
   */
  class ArgumentLoader : public ins::Dispatcher<ArgumentLoader, Inlining> {
  public:
    /** Inlines the given callee into the caller at the specified location.
     */
    static void inlineArguments(Inlining &inlining,
                                analysis::CallSite const &callsite) {
      ArgumentLoader al;

      for (size_t i = 0; i < callsite.size(); ++i) {
        size_t pc = callsite[i];
        inlining.erase(pc);
        al.setChunk(inlining.insertChunk(pc));
        al.dispatch(inlining, inlining.code(), pc);
        // if there is settag instruction, erase it too
        if (callsite.hasName(i))
          inlining.erase(callsite.name(i));
      }
    }

    static void initialize() {
      bind<MAKEPROM_OP::Predicate>(&ArgumentLoader::MAKEPROM_OP);
      bind<PUSHCONSTARG_OP::Predicate>(&ArgumentLoader::PUSHCONSTARG_OP);
      bind<PUSHARG_OP::Predicate>(&ArgumentLoader::PUSHARG_OP);
      bind<PUSHNULLARG_OP::Predicate>(&ArgumentLoader::PUSHNULLARG_OP);
      bind<PUSHTRUEARG_OP::Predicate>(&ArgumentLoader::PUSHTRUEARG_OP);
      bind<PUSHFALSEARG_OP::Predicate>(&ArgumentLoader::PUSHFALSEARG_OP);
      bind<DOMISSING_OP::Predicate>(&ArgumentLoader::DOMISSING_OP);
    }

  private:
    void setChunk(CodeBuffer::Code &chunk) { chunk_ = &chunk; }

    CodeBuffer::Code &chunk() {
      assert_(chunk_ != nullptr);
      return *chunk_;
    }

    /** MAKEPROM_OP is translated to CREATE_PROMISE_OP special opcode that
      creates promise from its arguments and pushes it on stack.

      TODO: this is not equivalent to what MAKEPROM_OP does, as it does not
      create promises for all functions.
     */
    void MAKEPROM_OP(Driver &driver, MAKEPROM_OP::Encoding &ins) {
      chunk().append(CREATE_PROMISE_OP::create(ins.arg1));
    }

    /** Constant argument push is replaced by LDCONST_OP.
     */
    void PUSHCONSTARG_OP(Driver &driver, PUSHCONSTARG_OP::Encoding &ins) {
      chunk().append(LDCONST_OP::create(ins.arg1));
    }

    /** Pushing an argument is no-op, the argument is already on stack.
     */
    void PUSHARG_OP(Driver &driver, PUSHARG_OP::Encoding &ins) {
      // pass
    }

    /** Pushing null becomes loading null.
     */
    void PUSHNULLARG_OP(Driver &driver, PUSHNULLARG_OP::Encoding &ins) {
      chunk().append(LDNULL_OP::create());
    }

    /** Pushing true becomes loading true.
     */
    void PUSHTRUEARG_OP(Driver &driver, PUSHTRUEARG_OP::Encoding &ins) {
      chunk().append(LDTRUE_OP::create());
    }

    /** Pushing false becomes loading false.
     */
    void PUSHFALSEARG_OP(Driver &driver, PUSHFALSEARG_OP::Encoding &ins) {
      chunk().append(LDFALSE_OP::create());
    }

    /** Missing arguments are not yet implemented.

      It would require proper setting of the arguments list to the new
      environment.
     */
    void DOMISSING_OP(Driver &driver, DOMISSING_OP::Encoding &ins) {
      NOT_IMPLEMENTED;
    }

    /** Chunk to which the actual argument should be translated.
     */
    CodeBuffer::Code *chunk_;
  };

  /** Matches the callsite to the specific callee.

    Matches all arguments to their formal names and determines missing arguments
    so that the call can be properly placed.
   */
  class MatchedCallSite {
  public:
    /** Finds the matching of the callsite arguments and their names to the
      formal arguments of the target closure.

      In R, this is done in 3 stages:

      1) match precisely any named arguments
      2) match prefixes of any named arguments
      3) match remaining arguments positionally

      If after these steps any arguments with default values were not defined,
      their default values will be created.

      TODO this is not the fastest way, but not important now
     */
    MatchedCallSite(CodeBuffer const &caller,
                    analysis::CallSite const &callsite,
                    r::Closure const &closure)
        : callsite_(callsite), closure_(closure),
          argumentNames_(callsite.size()) {
      std::vector<bool> matched(closure.formals().size());
      // first pass - exact matches
      for (size_t i = 0; i < callsite_.size(); ++i) {
        for (size_t j = 0; j < closure.formals().size(); ++j)
          // matched
          if (closure.formals().name(j) == callsite_.name(caller, j)) {
            argumentNames_[i] = closure.formals().name(j);
            matched[j] = true;
            break;
          }
      }
      // second pass - prefix matching
      for (size_t i = 0; i < callsite_.size(); ++i) {
        // check that the argument has not been matched yet
        if (argumentNames_[i] != nullptr)
          continue;
        char const *argName = CHAR(PRINTNAME(callsite_.name(caller, i)));
        int matchedTo = -1;
        for (size_t j = 0; j < closure.formals().size(); ++j) {
          // if the argument has been matched already, ignore it
          if (matched[j] == true)
            continue;
          // try prefix matching it
          char const *formalName = CHAR(PRINTNAME(closure.formals().name(j)));
          // if it is prefix, check that it is not ambiguous
          if (isPrefixOf(argName, formalName)) {
            assert_(matchedTo ==
                    -1); // TODO this should ne proper error, not assertion
            matchedTo = j;
          }
        }
        if (matchedTo != -1) {
          argumentNames_[i] = closure.formals().name(matchedTo);
          matched[static_cast<size_t>(matchedTo)] = true;
        }
      }
      // third pass - remaining arguments are positionally matched
      unsigned formalIdx = 0;
      for (size_t i = 0; i < callsite_.size(); ++i) {
        // ignore already matched arguments
        if (argumentNames_[i] != nullptr)
          continue;
        // find unmatched argument
        while (matched[formalIdx] == true) {
          ++formalIdx;
          assert_(formalIdx < matched.size())
        }
        matched[formalIdx] = true;
        argumentNames_[i] = closure.formals().name(formalIdx);
        if (++formalIdx == matched.size())
          continue;
      }
      // everything else is missing
      for (size_t i = 0; i < matched.size(); ++i) {
        // ignore already matched arguments
        if (matched[i] == true)
          continue;
        missingArguments_.push_back(i);
      }
    }

    std::vector<size_t> const &missingArguments() const {
      return missingArguments_;
    }

  private:
    /** Returns true if first argument is prefix of second one. The arguments
     * are both expected to be null terminated strings.
     */
    bool isPrefixOf(char const *a, char const *b) {
      for (size_t i = 0;; ++i) {
        if (a[i] == 0)
          return true;
        if (a[i] != b[i])
          return false;
      }
    }

    /** Callsite for the call.
     */
    analysis::CallSite const &callsite_;

    /** Callee
     */
    r::Closure const &closure_;

    /** For each callsite argument, contains its name in the callee's frame.
     */
    std::vector<SEXP> argumentNames_;

    /** Contains indices of missing arguments.
     */
    std::vector<size_t> missingArguments_;
  };

  /** Inlines the given callee function to specified position.

    This does not deal with anything else but inlining the code. The arguments,
    frame, etc. should be set up by other parts of the inlining mechanism.
   */
  class Inliner : public ins::Dispatcher<Inliner, Inlining> {
  public:
    /** Inlines the given callee into the caller at the specified location.
     */
    static void inlineCall(Inlining &inlining, size_t callpc,
                           CodeBuffer const &callee) {
      Inliner i(inlining, inlining.insertChunk(callpc), callee);
      // erase old CALL_OP instruction
      inlining.erase(callpc);
      // copy the constants
      i.copyConstants();
      // copy the code
      i.copyCode();
      // fixup unresolved branches and return jumps
      i.fixup();
    }

    static void initialize() {
      bind<RETURN_OP::Predicate>(&Inliner::RETURN_OP);
      // return jmp is the same as it likely jumps to a different place entirely
      bind<Jump>(&Inliner::unconditionalJump);
      bind<ConditionalJump2>(&Inliner::conditionalJump2);
      bind<STARTFOR_OP::Predicate>(&Inliner::STARTFOR);
      bind<InstructionR0::Predicate>(&Inliner::InstructionRO);
      bind<InstructionR1::Predicate>(&Inliner::InstructionR1);
      bind<InstructionR2::Predicate>(&Inliner::InstructionR2);
      bind<InstructionR3::Predicate>(&Inliner::InstructionR3);
      bind<InstructionR4::Predicate>(&Inliner::InstructionR4);
    }

  private:
    Inliner(Inlining &inlining, CodeBuffer::Code &patch,
            CodeBuffer const &callee)
        : inlining_(inlining), patch_(patch), callee_(callee) {}

    /** Puts all constants from the callee to the caller and notes their new
     * indices.
     */
    void copyConstants() {
      for (SEXP c : callee_.constants)
        constants_.push_back(inlining_.addConstant(c));
    }

    /** Copies the code from callee to caller's patch. Updates the constant
     * indices and backward jumps.
     */
    void copyCode() {
      for (pc_ = 1; pc_ < callee_.code.size();
           pc_ += callee_.code.instructionSize(pc_))
        dispatch(inlining_, callee_, pc_);
    }

    /** Fixes chunk local forward jumps that are still unresolved as well as
      return jumps.

      The return jumps all go to chunk local jump of the patch size, that is
      first instruction after the patch.
     */
    void fixup() {
      // fixup local branches
      for (size_t i : fixups_)
        patch_[i] = fixJump(patch_[i]);
      // fixup return jumps
      size_t target = inlining_.chunkLocal(patch_.size());
      for (size_t pc : returnJumps_)
        patch_[pc] = target;
    }

    /** Fixes given jump.

      That is first adjusts it for the changes in the inlined code and then
      converts it to a chunk local label wrt the patch.
     */
    size_t fixJump(size_t address) {
      for (size_t i = 0; i < returnJumps_.size() and returnJumps_[i] < address;
           ++i)
        ++address;
      return inlining_.chunkLocal(address);
    }

    /** Returns the index of the constant in the caller's frame.
     */
    size_t constant(size_t index) { return constants_[index]; }

    /** Converts the jump address to the inlined code.

      If cannot be converted, adds the instruction to the fixup list.
     */
    size_t jump(size_t address, size_t patchAddress) {
      // this is so that we only store the addresses of the targets, not whole
      // instructions
      patchAddress += patch_.size();
      // forward jump - must be fixed later
      if (address > patch_.size()) {
        fixups_.push_back(patchAddress);
        return address;
        // backward jump - depending on the already produced GOTO_OPs increase
        // the address
      } else {
        return fixJump(address);
      }
    }

    void RETURN_OP(Driver &driver, RETURN_OP::Encoding const &ins) {
      // don't do anything if RETURN is the last instruction
      if (pc_ + RETURN_OP::_.size() == callee_.code.size())
        return;
      returnJumps_.push_back(patch_.size() + 1);
      patch_.append(GOTO_OP::create(0));
    }

    void unconditionalJump(Driver &driver, InstructionR1::Encoding const &ins) {
      patch_.append(encoding::R1(ins.opcode, jump(ins.arg1, 1)));
    }

    void conditionalJump2(Driver &driver, InstructionR2::Encoding const &ins) {
      patch_.append(
          encoding::R2(ins.opcode, constant(ins.arg1), jump(ins.arg2, 2)));
    }

    void STARTFOR(Driver &driver, InstructionR3::Encoding const &ins) {
      patch_.append(encoding::R3(ins.opcode, constant(ins.arg1),
                                 constant(ins.arg2), jump(ins.arg3, 3)));
    }

    void InstructionRO(Driver &driver, InstructionR0::Encoding const &ins) {
      patch_.append(encoding::R0(ins.opcode));
    }

    void InstructionR1(Driver &driver, InstructionR1::Encoding const &ins) {
      patch_.append(encoding::R1(ins.opcode, constant(ins.arg1)));
    }

    void InstructionR2(Driver &driver, InstructionR2::Encoding const &ins) {
      patch_.append(
          encoding::R2(ins.opcode, constant(ins.arg1), constant(ins.arg2)));
    }

    void InstructionR3(Driver &driver, InstructionR3::Encoding const &ins) {
      patch_.append(encoding::R3(ins.opcode, constant(ins.arg1),
                                 constant(ins.arg2), constant(ins.arg3)));
    }

    void InstructionR4(Driver &driver, InstructionR4::Encoding const &ins) {
      patch_.append(encoding::R4(ins.opcode, constant(ins.arg1),
                                 constant(ins.arg2), constant(ins.arg3),
                                 constant(ins.arg4)));
    }

    Inlining &inlining_;
    CodeBuffer::Code &patch_;
    CodeBuffer const &callee_;

    size_t pc_;
    /* Pc's of GOTO_OP instructions introduced instead of returns.

      Because each GOTO_OP adds extra size to the inlined code as compared to
      the original code this also serves as a way to determine pc offset for
      local jumps.
     */
    std::vector<size_t> returnJumps_;
    /* List of local jumps that need to be patched when inlining is done.

      Those are local forward jumps where we cannot be sure about the target
      address because RETURN_OP -> GOTO_OP increases codesize.
     */
    std::vector<size_t> fixups_;

    /* For each callee's constants keeps its index in the caller.
     */
    std::vector<size_t> constants_;
  };
};

} // namespace optimization
} // namespace reactor

#endif // OPT_INLINING_H
