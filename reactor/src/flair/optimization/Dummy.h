#ifndef OPT_DUMMY
#define OPT_DUMMY

#include "../optimizations.h"
#include "../analysis.h"

#include <vector>

namespace reactor {
namespace optimization {

class Dummy : public Optimizer<Dummy>,
              public ins::Dispatcher<Dummy, Scheduler> {

public:
  char const *name() const override { return "Dummy"; }

  static void initialize() { bind<Instruction::Predicate>(&Dummy::nop); }

  Dummy(Scheduler &scheduler) : Optimizer<Dummy>(scheduler) {}

  void nop(Driver &d, encoding::OpcodeOnly const &ins) {}

  void run(const Analysis &a) { r_.push_back(&const_cast<Analysis &>(a)); }

protected:
  friend class Scheduler;

  void initializeScheduling() override {
    for (auto a : r_) {
      require(*a);
      preserve(*a);
    }
  }

private:
  std::vector<Analysis *> r_;
};
}
}

#endif
