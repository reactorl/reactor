#ifndef OPT_TEST
#define OPT_TEST

#include "../../bytecode/dispatcher.h"
#include "../../bytecode/rbc.h"
#include "../analysis/FrameVariables.h"
#include "../analysis/SideEffect.h"
#include "../analysis/Liveness.h"
#include "../optimizations.h"

namespace reactor {
namespace optimization {

class TestOpt : public Optimizer<TestOpt>,
                public ins::Dispatcher<TestOpt, Scheduler> {

public:
  char const *name() const override { return "Test"; }

  static void initialize() { bind<Instruction::Predicate>(&TestOpt::instr); }

  TestOpt(Scheduler &scheduler) : Optimizer<TestOpt>(scheduler) {}

protected:
  friend class Scheduler;

  void initializeScheduling() override {
    // TODO : why cast necessary??
    require((const Analysis &)scheduler().sideEffect());
    require((const Analysis &)scheduler().frameVariables());
    require((const Analysis &)scheduler().liveness());
  }

private:
  void instr(Driver &driver, encoding::OpcodeOnly const &ins) {
    auto s = driver.frameVariables().state(pc());
    auto e = driver.sideEffect().state(pc());
    auto l = driver.liveness().state(pc());
    std::cout << pc() << ":" << std::endl;
    std::cout << "a is live "
              << (s.hasName(Rf_install("a")) && l.count(Rf_install("a")))
              << std::endl;
    std::cout << "has a " << s.hasName(Rf_install("a")) << std::endl;
    std::cout << "eff " << e.has(analysis::SideEffectFlag::SIDEEFFECT)
              << std::endl;
    std::cout << "---------------------------" << std::endl;
  }
};
}
}

#endif
