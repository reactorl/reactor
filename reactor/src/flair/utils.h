#ifndef UTILS_H
#define UTILS_H

#include "reactor.h"
#include "assert.h"

#include "bytecode/code_buffer.h"

namespace reactor {

/** Returns true if the given SEXP is a blank symbol.

  False if the symbol is not blank or if it is not symbol at all.
 */
inline bool isEmptySymbol(SEXP symbol) {
  if (TYPEOF(symbol) != SYMSXP)
    return false;
  return PRINTNAME(symbol) == R_BlankString;
}

inline SEXP environment(std::string const &envName) {
  CharacterVector x(1);
  x.set(0, Rf_mkChar(envName.c_str()));
  SEXP call = Rf_install("as.environment");
  SEXP result = Rf_eval(Rf_lang2(call, x), R_GlobalEnv);
  return result;
}

/** Compiles the given expression.

  Currently invokes the R's compile() function.
*/
inline SEXP compileExpression(SEXP language) {
  SEXP env = environment("package:compiler");
  SEXP name = Rf_install("compile");
  SEXP f = Rf_findFun(name, env);
  SEXP call = PROTECT(Rf_lang2(f, language));
  SEXP result = Rf_eval(call, env);
  UNPROTECT(1);
  return result;
}

/** Returns true if the given function has already been compiled, false
 * otherwise.
 */
inline bool isCompiled(SEXP function) {
  assert_(TYPEOF(function) == CLOSXP);
  return TYPEOF(BODY(function)) == BCODESXP;
}

/** Returns a symbol with the given name.

  If such symbol already exists, returns its SEXP, otherwise creates and
 registers new symbol.
 */
inline SEXP getOrCreateSymbol(const std::string &name) {
  return Rf_install(name.c_str());
}

} // namespace flair
#endif // UTILS_H
