#ifndef STACK_DEPTH_H
#define STACK_DEPTH_H

#include <vector>
#include <deque>

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"
#include "flair/pass.h"
#include "utils.h"

namespace reactor {

class StackDepth : public ins::Dispatcher<StackDepth, Pass::Forward<int>>,
                   public Pass::Forward<int> {
public:
  class Result {
  public:
    static const int UNINITIALIZED;
    static const int UNDERRUN;
    static const int INVALID;

    int operator[](size_t pc) const { return depths_[pc]; }

    size_t size() const { return depths_.size(); }

    std::vector<int> const &data() const { return depths_; }

  private:
    friend class StackDepth;

    Result(size_t size) {
      depths_.insert(depths_.begin(), size, UNINITIALIZED);
    }

    int &operator[](size_t pc) { return depths_[pc]; }

    std::vector<int> depths_;
  };

  StackDepth(CodeBuffer const &code)
      : Pass::Forward<int>(code), result_(new Result(code.code.size())) {}

  static Result *analyze(CodeBuffer const &code) {
    StackDepth sd(code);
    sd.execute<StackDepth>(new int(0));
    return sd.result_;
  }

  static void initialize() { bind<Instruction::Predicate>(&StackDepth::INS); }

protected:
  Result *result_;

  void INS(Pass::Forward<int> &l, encoding::OpcodeOnly const &ins) {
    int &incomming = l.state();
    int &state = (*result_)[l.pc()];
    Instruction const &i = Instruction::get(ins.opcode);
    // first update the current state with the incomming state
    if (state != Result::UNINITIALIZED) {
      state = Result::INVALID;
      l.terminate();
    } else {
      if (incomming < static_cast<int>(i.stackOut())) {
        state = Result::UNDERRUN;
        l.terminate();
      } else {
        incomming = incomming - i.stackOut() + i.stackIn();
        state = incomming;
      }
    }
  }
};

} // namespace flair

#endif // STACK_DEPTH_H
