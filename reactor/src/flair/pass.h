#ifndef PASS_H
#define PASS_H

/* This is my playground for passes and better analysis handlers.

  Handler has only the private method handle.



*/

#include <deque>

#include "reactor.h"
#include "bytecode/predicates.h"
#include "bytecode/code_buffer.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"

namespace reactor {

class Pass {
public:
  class Sequential;

  template <typename STATE> class Forward;

  template <typename STATE> class ForwardSinglePass;
};

class Pass::Sequential {
public:
  Sequential(CodeBuffer const &code)
      : code_(code), pc_(1), end_(code.code.size()) {}

  Sequential(CodeBuffer const &code, size_t start)
      : code_(code), pc_(start), end_(code.code.size()) {}

  Sequential(CodeBuffer const &code, size_t start, size_t end)
      : code_(code), pc_(start), end_(end) {}

  template <typename DISPATCHER> void execute(DISPATCHER &dispatcher) {
    while (not eof()) {
      dispatcher.dispatch(*this, code_, pc_);
      next();
    }
  }

  bool eof() const { return pc_ >= end_; }

  void next() {
    if (not eof())
      pc_ = nextPc();
  }

  /** Returns true if the current instruction is the last one in the code
   * buffer.
   */
  bool isLast() const { return (nextPc() == code_.code.size()); }

  void eraseAt(size_t pcErase) {
    if (eof()) {
      return;
    }
    // Cast away const (probably subverting invariant).
    CodeBuffer &codeBuf = const_cast<CodeBuffer &>(code_);
    // Adjust pc_ when past instruction to be deleted.
    if (pc_ > pcErase) {
      pc_ -= Instruction::get(codeBuf.get<encoding::OpcodeOnly>(pcErase).opcode)
                 .size();
    }
    codeBuf.code.erase(pcErase);
  }

  size_t pc() const { return pc_; }

  CodeBuffer const &code() const { return code_; }

protected:
  size_t nextPc() const {
    return pc_ +
           Instruction::get(code_.get<encoding::OpcodeOnly>(pc_).opcode).size();
  }

private:
  CodeBuffer const &code_;
  size_t pc_;
  size_t end_;
};

/** Forward pass.

*/

template <typename STATE> class Pass::Forward : public Pass {
public:
  size_t pc() const { return queue_.begin()->first; }

  STATE const &state() const { return *queue_.begin()->second; }

  STATE &state() { return *queue_.begin()->second; }

  CodeBuffer const &code() const { return code_; }

  void terminate() { insertNextNodes_ = false; }

protected:
  Forward(CodeBuffer const &code) : code_(code) {}

  template <typename DISPATCHER> void execute(STATE *initialState) {
    reset();
    pushImmediate(1, *initialState);
    insertNextNodes_ = true;
    deleteState_ = true;
    while (not eof()) {
      static_cast<DISPATCHER *>(this)->dispatch(*this, code(), pc());
      if (insertNextNodes_)
        dispatcher_.dispatch(*this, code(), pc());
      else
        insertNextNodes_ = true;
      if (deleteState_)
        delete queue_.begin()->second;
      else
        deleteState_ = true;
      next();
    }
  }

  void reset() { queue_.clear(); }

  bool eof() const { return queue_.empty(); }

  void next() { queue_.pop_front(); }

  void pushImmediate(size_t pc, STATE &state) {
    queue_.push_back(std::pair<size_t, STATE *>(pc, &state));
    deleteState_ = false;
  }

  void push(size_t pc, STATE &state) {
    queue_.push_back(std::pair<size_t, STATE *>(pc, new STATE(state)));
  }

  class Dispatcher_
      : public ins::Dispatcher<Dispatcher_, Pass::Forward<STATE>> {
  public:
    typedef ins::Dispatcher<Dispatcher_, Pass::Forward<STATE>> base_;

    static void initialize() {
      base_::template bind<RETURN_OP::Predicate>(&Dispatcher_::RETURN);
      base_::template bind<RETURNJMP_OP::Predicate>(&Dispatcher_::RETURNJMP);
      base_::template bind<GOTO_OP::Predicate>(&Dispatcher_::GOTO);
      base_::template bind<BRIFNOT_OP::Predicate>(&Dispatcher_::BRIFNOT);
      base_::template bind<AND1ST_OP::Predicate>(&Dispatcher_::AND1ST);
      base_::template bind<OR1ST_OP::Predicate>(&Dispatcher_::OR1ST);
      base_::template bind<STARTSUBSET_OP::Predicate>(
          &Dispatcher_::STARTSUBSET);
      base_::template bind<STARTSUBSET2_OP::Predicate>(
          &Dispatcher_::STARTSUBSET2);
      base_::template bind<STARTC_OP::Predicate>(&Dispatcher_::STARTC);
      base_::template bind<STARTFOR_OP::Predicate>(&Dispatcher_::STARTFOR);
      base_::template bind<STEPFOR_OP::Predicate>(&Dispatcher_::STEPFOR);
      base_::template bind<Instruction::Predicate>(&Dispatcher_::INS);
    }

  private:
    size_t next(Pass::Forward<STATE> const &l,
                encoding::OpcodeOnly const &ins) {
      return l.pc() + Instruction::get(ins.opcode).size();
    }

    void INS(Pass::Forward<STATE> &l, encoding::OpcodeOnly const &ins) {
      l.pushImmediate(next(l, ins), l.state());
    }

    void RETURN(Pass::Forward<STATE> &l, RETURN_OP::Encoding const &ins) {
      // do nothing
    }

    void RETURNJMP(Pass::Forward<STATE> &l, RETURNJMP_OP::Encoding const &ins) {
      // do nothing
    }

    void GOTO(Pass::Forward<STATE> &l, GOTO_OP::Encoding const &ins) {
      l.pushImmediate(ins.arg1, l.state());
    }

    void BRIFNOT(Pass::Forward<STATE> &l, BRIFNOT_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void AND1ST(Pass::Forward<STATE> &l, AND1ST_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void OR1ST(Pass::Forward<STATE> &l, OR1ST_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void STARTSUBSET(Pass::Forward<STATE> &l,
                     STARTSUBSET_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void STARTSUBSET2(Pass::Forward<STATE> &l,
                      STARTSUBSET2_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void STARTC(Pass::Forward<STATE> &l, STARTC_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void STARTFOR(Pass::Forward<STATE> &l, STARTFOR_OP::Encoding const &ins) {
      l.push(next(l, ins), l.state());
      l.pushImmediate(ins.arg2, l.state());
    }

    void STEPFOR(Pass::Forward<STATE> &l, STEPFOR_OP::Encoding const &ins) {
      l.pushImmediate(ins.arg1, l.state());
    }
  };

  CodeBuffer const &code_;
  Dispatcher_ dispatcher_;
  bool insertNextNodes_;
  bool deleteState_;
  std::deque<std::pair<size_t, STATE *>> queue_;
};

/** Visits the instructions in forward order of execution, and guarantees that
   each instruction will be visited only once.

  */

template <typename STATE>
class Pass::ForwardSinglePass : public Pass::Forward<STATE> {
public:
  ForwardSinglePass(CodeBuffer const &code) : Pass::Forward<STATE>(code) {
    visited_.insert(visited_.begin(), code.code.size(), false);
  }

  void reset() {
    Pass::Forward<STATE>::reset();
    for (size_t i = 0; i < visited_.size(); ++i)
      visited_[i] = false;
  }

  using Pass::Forward<STATE>::insertNextNodes_;
  using Pass::Forward<STATE>::deleteState_;
  using Pass::Forward<STATE>::dispatcher_;
  using Pass::Forward<STATE>::queue_;
  using Pass::Forward<STATE>::eof;
  using Pass::Forward<STATE>::pc;
  using Pass::Forward<STATE>::code;
  using Pass::Forward<STATE>::next;
  using Pass::Forward<STATE>::push;
  using Pass::Forward<STATE>::pushImmediate;

  template <typename DISPATCHER> void execute(STATE *initialState) {
    reset();
    pushImmediate(1, *initialState);
    insertNextNodes_ = true;
    deleteState_ = true;
    while (not eof()) {
      if (not visited_[pc()]) {
        static_cast<DISPATCHER *>(this)->dispatch(*this, code(), pc());
        if (insertNextNodes_)
          dispatcher_.dispatch(*this, code(), pc());
        else
          insertNextNodes_ = true;
        visited_[pc()] = true;
      }
      if (deleteState_)
        delete queue_.begin()->second;
      else
        deleteState_ = true;
      next();
    }
  }

private:
  // TODO we do not need visited for all pcs, just for mergepoints
  std::vector<bool> visited_;
};

} // namespace reactor

#endif // PASS_H
