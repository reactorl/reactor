#include "reactor.h"

#include "utils.h"
#include "printer.h"
#include "stack_depth.h"
#include "call_site_discovery.h"
#include "inliner.h"
#include "local_vars.h"

#include "scheduler.h"
#include "state.h"

#include "optimization/ConstantPropagation.h"

#include "optimization/Inlining.h"

using namespace std;
using namespace reactor;

REXPORT SEXP flair_sandbox(SEXP bytecode) {
  CodeBuffer cb(bytecode);
  Scheduler scheduler(cb);
  new optimization::ConstantPropagation(scheduler);
  scheduler.schedule();
  scheduler.invoke();
  return cb;
  /*
    CodeBuffer cb(bytecode);
    Printer::execute(cb);
    CallSiteDiscovery::Result *r = CallSiteDiscovery::analyze(cb);
    cout << "Found " << r->size() << " callsites. " << endl;
    StaticInliner::inlineCallsite(cb, (*r)[0], callee);

    cout << " After inlining ... " << endl;
    Printer::execute(cb);
    //    CodeBuffer cb(bytecode);
    // StackDepth::Result * result = StackDepth::analyze(bytecode);
    // SD::initialize();
    delete r;
    return bytecode; */
}
