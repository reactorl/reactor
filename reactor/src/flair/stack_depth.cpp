#include "stack_depth.h"

namespace reactor {

const int StackDepth::Result::UNINITIALIZED = -1;
const int StackDepth::Result::UNDERRUN = -2;
const int StackDepth::Result::INVALID = -3;

} // namespace flair
