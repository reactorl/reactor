#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <vector>
#include <deque>
#include <map>
#include <algorithm>

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"

#include "patch.h"

namespace reactor {

// forward declaration of all analyses

class Analysis;

class Optimization;

namespace analysis {
class MergePoints;
class PcMapper;
class StackDepth;
class ReachingDefinitions;
class ConstantPropagation;
class CallSiteDiscovery;
class FrameVariables;
class Liveness;
class SideEffect;
}

// TODO scheduler should be dependent not on code, but r::Closure ideally
class Scheduler {
public:
  class Pass;

  /** Default constructor.

    Creates objects for all analyses the scheduler works with.
   */
  Scheduler(CodeBuffer &code);

  ~Scheduler() throw();

  /** Placeholder for all analyses. Used in analyses when they preserve all
    passes.

    Should not be obtained by the user directly in any other cases.
   */
  Analysis const &all() const { return *all_; }

  /** Returns the merge points analysis.
   */
  analysis::MergePoints const &mergePoints();

  analysis::PcMapper const &pcMapper();

  analysis::StackDepth const &stackDepth();

  analysis::ReachingDefinitions const &reachingDefinitions();

  analysis::ConstantPropagation const &constantPropagation();

  analysis::CallSiteDiscovery const &callSiteDiscovery();

  analysis::FrameVariables const &frameVariables();

  analysis::SideEffect const &sideEffect();

  analysis::Liveness const &liveness();

  /** Returns the code currently being optimized.
   */
  CodeBuffer &code() const {
    assert_(code_ != nullptr);
    return *code_;
  }

  /** Invokes all sheduled analyses and optimizations on the given code.

   TODO this can be much updated by running multiple rounds of the analyses as
   long as there is any change in the code and so on. For now, it just shows
   the basics. Likewise the patch application can vary - when the scheduler
   realizes that it needs to rerun certain analyses, it might realize that the
   patch should be applied so that the new analyses are on already new code,
   and so on.
 */
  void invoke() {
    // invalidate all analyses
    invalidateAll();
    // create the patch
    patch_ = new Patch(*code_);
    for (Pass *pass : scheduledPasses_)
      invoke(pass);
    // apply and discard the patch
    patch_->apply();
    delete patch_;
    currentPass_ = nullptr;
  }

  /** Determines the dependencies of all analyses and optimizations known and
   * schedules them with minimal invalidations.
   */
  void schedule();

  /** Invokes the given pass and its dependencies regardless of any scheduling.

    However, schedule() method must be called before it anyways so that the
    dependencies of the pass are known and can be processed.
   */
  void invoke(CodeBuffer &code, Pass const &pass) {
    patch_ = new Patch(code);
    code_ = &code;
    invoke(const_cast<Pass *>(&pass));
    if (not patch_->empty())
      patch_->apply();
    delete patch_;
    // keep the code if we want to access the analysis results afterwards
    // code_ = nullptr;
  }

  /** Resets the scheduler.

    Clears its code pointer.
   */
  void reset() { code_ = nullptr; }

  /** Debug ouput for pass initiated messages. Displays the pass name and the
   * message correctly indented.
   */
  void debug(Pass const *pass, std::string const &message) DEBUG_ONLY;

private:
  friend class Optimization;
  friend class Analysis;

  /** Helper function that makes sure the returned analysis is valid, if
   * possible.
       */
  template <typename ANALYSIS>
  ANALYSIS const &invokeIfNeeded(ANALYSIS *analysis) {
    if (code_ != nullptr and analysis->valid_ == false)
      invoke(analysis);
    return *analysis;
  }

  /** Invalidates the given analysis.

    If the analysis is required by the pass currently being executed, the
    analysis is recomputed immediately so that the pass may continue ist
    execution.
   */
  void invalidate(Analysis *what);

  /** Invalidates all analyses at once.

   Reexecutes all analyses required by the current optimization.
   */
  void invalidateAll();

  /** Applies the code changes, renders all analyses as invalid.
   */
  void applyChanges() {
    patch_->apply();
    invalidateAll();
  }

  /** Runs the specified pass.
   */
  void invoke(Pass *pass);

  /** Set of all analyses the scheduler has.
   */
  std::unordered_set<Analysis *> analyses_;

  /** List of all optimizations registered with the scheduler.
   */
  std::vector<Optimization *> optimizations_;

  /** List of the scheduled passes in the order of their execution.
   */
  std::vector<Pass *> scheduledPasses_;

  /** Current pass.

    We need it to keep track of what is executed at the moment and therefore
    manage invalidations properly.
   */
  Pass *currentPass_ = nullptr;

  /** Currently analyzed code.
   */
  CodeBuffer *code_ = nullptr;

  /** Patch used by the scheduled optimizations.
   */
  Patch *patch_ = nullptr;

  // Analyses

  Analysis *all_;
  analysis::MergePoints *mergePoints_;
  analysis::PcMapper *pcMapper_;
  analysis::StackDepth *stackDepth_;
  analysis::ReachingDefinitions *reachingDefinitions_;
  analysis::ConstantPropagation *constantPropagation_;
  analysis::CallSiteDiscovery *callSiteDiscovery_;
  analysis::FrameVariables *frameVariables_;
  analysis::Liveness *liveness_;
  analysis::SideEffect *sideEffect_;
};

// --------------------------------------------------------------------------------------------------------------------

class Scheduler::Pass {
public:
  /** Method called by the scheduler if the pass is to be executed on specified
   * code.
   */
  virtual void invoke() abstract;

  /** Method called by the scheduler after creation of the pass object to
    determine what other passes the pass requires to be valid.

    The method should call scheduler's require() and preserve() methods to allow
    better scheduling.
   */
  virtual void initializeScheduling() abstract;

  /** Returns the name of the pass for debugging.
   */
  virtual char const *name() const abstract;

  virtual ~Pass() throw() {}

protected:
  /** Each pass must be initialized with an existing scheduler that is
   * responsible for its invocation.
   */
  Pass(Scheduler &scheduler) : scheduler_(scheduler) {}

  /** Returns the scheduler responsible for the execution of the pass.
   */
  Scheduler &scheduler() const { return scheduler_; }

  /** Marks the given analysis as a requirement for the current pass.

    The scheduler will always guarantee that the required analyses will be valid
    while the pass executes.

    TODO I am not checking for circular dependencies... Should I?
   */
  void require(Analysis const &analysis) {
    Analysis *a = const_cast<Analysis *>(&analysis);
    assert_eq(requires_.find(a), requires_.end(),
              "An analysis can only be required once");
    assert_neq(a, &scheduler().all(), "Cannot require all analyses");
    requires_.insert(a);
  }

  /** Marks the given analysis as preserved by the pass.

    After the pass finishes, the scheduler invalidates all analyses but the ones
    specified here as being preserved.
   */
  void preserve(Analysis const &analysis) {
    Analysis *a = const_cast<Analysis *>(&analysis);
    if (a == &scheduler().all()) {
      invalidates_.clear();
    } else {
      assert_neq(invalidates_.find(a), invalidates_.end(),
                 "An analysis can only be preserved once");
      invalidates_.erase(a);
    }
  }

private:
  friend class Scheduler;

  /** Scheduler responsible for the pass.

    This provides access to environment elements such as the code being executed
    and so on.
   */
  Scheduler &scheduler_;

  /** Set of all analyses that are required to be valid before the current pass
   * can be executed.
   */
  std::unordered_set<Analysis *> requires_;

  /** Set of all analyses that need to be invalidated by the scheduler after the
   * pass finishes.
   */
  std::unordered_set<Analysis *> invalidates_;
};

} // namespace reactor

#endif // SCHEDULER_H
