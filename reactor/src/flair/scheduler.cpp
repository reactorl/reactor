#include <iostream>

#include "scheduler.h"
#include "analysis.h"
#include "optimizations.h"
#include "reporting.h"

#include "analysis/MergePoints.h"
#include "analysis/PcMap.h"
#include "analysis/StackDepth.h"
#include "analysis/ReachingDefinitions.h"
#include "analysis/ConstantPropagation.h"
#include "analysis/CallSiteDiscovery.h"
#include "analysis/FrameVariables.h"
#include "analysis/SideEffect.h"
#include "analysis/Liveness.h"

using namespace reactor;

Scheduler::Scheduler(CodeBuffer &code)
    : code_(&code), all_(new Analysis::All(*this)),
      mergePoints_(new analysis::MergePoints(*this)),
      pcMapper_(new analysis::PcMapper(*this)),
      stackDepth_(new analysis::StackDepth(*this)),
      reachingDefinitions_(new analysis::ReachingDefinitions(*this)),
      constantPropagation_(new analysis::ConstantPropagation(*this)),
      callSiteDiscovery_(new analysis::CallSiteDiscovery(*this)),
      frameVariables_(new analysis::FrameVariables(*this)),
      liveness_(new analysis::Liveness(*this)),
      sideEffect_(new analysis::SideEffect(*this)) {}

Scheduler::~Scheduler() throw() {
  delete mergePoints_;
  delete stackDepth_;
  delete reachingDefinitions_;
  delete constantPropagation_;
  delete callSiteDiscovery_;
  delete frameVariables_;
  delete liveness_;
  delete sideEffect_;
}

/** Returns the merge points analysis.
 */
analysis::MergePoints const &Scheduler::mergePoints() {
  return invokeIfNeeded(mergePoints_);
}

analysis::PcMapper const &Scheduler::pcMapper() {
  return invokeIfNeeded(pcMapper_);
}

analysis::StackDepth const &Scheduler::stackDepth() {
  return invokeIfNeeded(stackDepth_);
}

analysis::ReachingDefinitions const &Scheduler::reachingDefinitions() {
  return invokeIfNeeded(reachingDefinitions_);
}

analysis::ConstantPropagation const &Scheduler::constantPropagation() {
  return invokeIfNeeded(constantPropagation_);
}

analysis::CallSiteDiscovery const &Scheduler::callSiteDiscovery() {
  return invokeIfNeeded(callSiteDiscovery_);
}

analysis::FrameVariables const &Scheduler::frameVariables() {
  return invokeIfNeeded(frameVariables_);
}

analysis::SideEffect const &Scheduler::sideEffect() {
  return invokeIfNeeded(sideEffect_);
}

analysis::Liveness const &Scheduler::liveness() {
  return invokeIfNeeded(liveness_);
}

#ifdef DEBUG
/** Debug ouput for pass initiated messages. Displays the pass name and the
 * message correctly indented.
 */
void Scheduler::debug(Pass const *pass, std::string const &message) {
  Debug(SCHEDULING, STR(" "
                        << "[" << pass->name() << "] " << message
                        << std::endl));
}
#endif

/** Schedules the given analyses and optimizations so that least invalidations
 * happen.
 */
void Scheduler::schedule() {
  CodeBuffer *old = code_;
  code_ = nullptr; // so that analyses won't get validated when scheduling
  Debug(SCHEDULING, "Scheduling passes...");
  DebugIndent();
  // first set the invalidated analyses of all passes to all registered analyses
  // then run their initializeScheduling methods to remove excessive invalidates
  // and initialize requirements
  for (Analysis *a : analyses_) {
    a->invalidates_ = analyses_;
    Debug(SCHEDULING, STR("initializing scheduling for analyis " << a->name()));
    a->initializeScheduling();
  }
  for (Optimization *o : optimizations_) {
    o->invalidates_ = analyses_;
    Debug(SCHEDULING,
          STR("initializing scheduling for optimization" << o->name()));
    o->initializeScheduling();
  }
  DebugUnindent();
  // now do the scheduling
  // TODO this is dumb at the moment
  for (Optimization *o : optimizations_) {
    Debug(SCHEDULING, STR("optimization " << o->name() << " scheduled"));
    scheduledPasses_.push_back(o);
  }
  code_ = old; // return old code value
}

/** If the analysis is required by the pass currently being executed, the
 * analysis is recomputed immediately so that the pass may continue ist
 * execution.
 */
void Scheduler::invalidate(Analysis *what) {
  Debug(SCHEDULING, STR("Invalidating analysis " << what->name()));
  // if there is no current pass, we are in the automatic invalidation at the
  // end of each pass, don't do anything
  if (currentPass_ == nullptr)
    return;
  // if the invalidated analysis is needed by the current pass, we must
  // reexecute it immediately
  if (currentPass_->requires_.find(what) != currentPass_->requires_.end())
    invoke(what);
}

void Scheduler::invalidateAll() {
  Debug(SCHEDULING, "Invalidating all analyses...");
  DebugIndent();
  Pass *old = currentPass_;
  currentPass_ = nullptr;
  for (Analysis *a : analyses_)
    a->invalidate();
  currentPass_ = old;
  if (currentPass_ != nullptr)
    for (Analysis *a : currentPass_->requires_)
      invoke(a);
  DebugUnindent();
}

/** TODO Currently the code is fool-proof and it makes always sure that all
 * required passes analyses are really valid. It is a question whether we want
 * this, or whether we will believe the scheduling algorithm and assume the
 * analyses to be valid. This may pose a problem later on if an optimization
 * invalidates analysis that it has listed as preserved one.
 */
void Scheduler::invoke(Pass *pass) {
  Debug(SCHEDULING, STR("Invoking pass " << pass->name()));
  DebugIndent();
  Pass *old = currentPass_;
  currentPass_ = pass;
  // check that all required analyses are valid
  for (Analysis *a : pass->requires_)
    if (not a->isValid())
      invoke(a);
  // now run the pass
  pass->invoke();
  // and finally invalidate the analyses not preserved by the pass
  currentPass_ = nullptr; // not to enter cycle
  for (Analysis *a : pass->invalidates_)
    a->invalidate();
  currentPass_ = old;
  DebugUnindent();
}
