#ifndef A_CALLSITEDISCOVERY
#define A_CALLSITEDISCOVERY

#include "../analysis.h"

namespace reactor {
namespace analysis {

/** Callsite information.

  Contains addresses of GETFUN_OP and CALL_OP instructions as well as all
  argument loading and name setting instructions used for the particular call.
 */
class CallSite {
public:
  size_t getfun() const { return getfun_; }

  GETFUN_OP::Encoding const &getfun(CodeBuffer const &code) const {
    return code.get<GETFUN_OP::Encoding>(getfun_);
  }

  size_t call() const { return call_; }

  CALL_OP::Encoding const &call(CodeBuffer const &code) const {
    return code.get<CALL_OP::Encoding>(call_);
  }

  /** Returns the number of arguments.
   */
  size_t size() const { return arguments_.size(); }

  size_t operator[](size_t index) const { return arguments_[index]; }

  /** Returns name of index-th argument at the callsite (the actual SEXP), or
   * nullptr if the argument is unnamed.
   */
  SEXP name(CodeBuffer const &code, size_t index) const {
    return names_[index] == 0 ? nullptr
                              : code.code.get<SETTAG_OP::Encoding>(
                                              names_[index]).constant1(code);
  }

  /** Returns the pc of the SETTAG_OP instruction for the index-th argument.
   */
  size_t name(size_t index) const { return names_[index]; }

  /** Returns true if index-th argument is keyword argument. False otherwise.
   */
  bool hasName(size_t index) const { return names_[index] != 0; }

private:
  friend class CallSiteDiscovery;

  CallSite() : getfun_(0), call_(0) {}

  void reset() {
    getfun_ = 0;
    call_ = 0;
    arguments_.clear();
    names_.clear();
  }

  size_t getfun_;
  size_t call_;
  std::vector<size_t> arguments_;
  std::vector<size_t> names_;

  void addArgument(size_t pc) {
    arguments_.push_back(pc);
    names_.push_back(0);
  }

  void setName(size_t pc) { names_.back() = pc; }
};

/** Finds all callsites in the code.

  Result of this analysis is list of all callsites. A callsite consists of
  corresponding GETFUN_OP and CALL_OP and argument filling instructions.

  Currently the analysis does not support callsites spanning multiple basic
  blocks. This is fine for standard R, but in the future our code might in
  theory generate these?
  */
class CallSiteDiscovery : public Analysis::SequentialPass<CallSiteDiscovery>,
                          public ins::Dispatcher<CallSiteDiscovery, Scheduler> {
public:
  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "CallSiteDiscovery"; }

  static void initialize() {
    bind<GETFUN_OP::Predicate>(&CallSiteDiscovery::GETFUN_OP);
    bind<CALL_OP::Predicate>(&CallSiteDiscovery::CALL_OP);
    bind<SETTAG_OP::Predicate>(&CallSiteDiscovery::SETTAG_OP);
    bind<MAKEPROM_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<PUSHARG_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<PUSHCONSTARG_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<PUSHTRUEARG_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<PUSHFALSEARG_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<PUSHNULLARG_OP::Predicate>(&CallSiteDiscovery::pushArgument);
    bind<DOMISSING_OP::Predicate>(&CallSiteDiscovery::pushArgument);
  }

  size_t size() const { return callsites_.size(); }

  std::vector<CallSite>::const_iterator begin() const {
    return callsites_.begin();
  }

  std::vector<CallSite>::const_iterator end() const { return callsites_.end(); }

  bool isCallsite(size_t pc) const {
    for (CallSite const &c : callsites_)
      if (c.getfun() == pc)
        return true;
    return false;
  }

  CallSite const &callsiteAt(size_t pc) const {
    for (CallSite const &c : callsites_)
      if (c.getfun() == pc)
        return c;
    assert_(false);
  }

protected:
  friend class reactor::Scheduler;

  CallSiteDiscovery(Scheduler &scheduler)
      : Analysis::SequentialPass<CallSiteDiscovery>(scheduler) {}

private:
  void GETFUN_OP(Driver &driver, GETFUN_OP::Encoding const &ins) {
    current_.getfun_ = pc();
  }

  void CALL_OP(Driver &driver, CALL_OP::Encoding const &ins) {
    if (current_.getfun_ != 0) {
      current_.call_ = pc();
      callsites_.push_back(current_);
      current_.reset();
    }
  }

  void SETTAG_OP(Driver &driver, SETTAG_OP::Encoding const &ins) {
    assert_(current_.getfun_ != 0);
    assert_(current_.size() > 0);
    current_.setName(pc());
  }

  void pushArgument(Driver &driver, encoding::OpcodeOnly const &ins) {
    assert_(current_.getfun_ != 0);
    current_.addArgument(pc());
  }

  CallSite current_;

  std::vector<CallSite> callsites_;
};

} // namespace analysis
} // namespace reactor

#endif // A_CALLSITEDISCOVERY
