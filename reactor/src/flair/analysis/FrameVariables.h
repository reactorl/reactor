#ifndef A_FRAMEVARIABLES_H
#define A_FRAMEVARIABLES_H

#include <set>

#include "../scheduler.h"
#include "../analysis_forward.h"

namespace reactor {
namespace analysis {

class FrameVariablesState {
public:
  /** Indicates that any name may be in the frame.
   */
  const SEXP AnyName = nullptr;

  void addLocal(SEXP name) {
    // no point in adding anything already
    if (locals_.size() == 1 and *locals_.begin() == AnyName)
      return;
    // if we are adding AnyName, get rid of all others first
    if (name == AnyName)
      locals_.clear();
    // add the name
    locals_.insert(name);
  }

  void removeLocal(SEXP name) { locals_.erase(name); }

  bool hasName(SEXP name) const {
    if (locals_.size() == 1 and *locals_.begin() == AnyName)
      return true;
    return locals_.find(name) != locals_.end();
  }

protected:
  friend class FrameVariables;
  friend Analysis::Forward<FrameVariables, FrameVariablesState>;

  /** There is nothing to check after frame variables. */
  void checkAfter(encoding::OpcodeOnly const &ins) {}

  bool includes(FrameVariablesState const &other) const {
    if (locals_.size() == 1 and *locals_.begin() == AnyName)
      return true;
    for (SEXP name : other.locals_)
      if (locals_.find(name) == locals_.end())
        return false;
    return true;
  }

  /** A merge is always successful.
   */
  FrameVariablesState *mergeWith(FrameVariablesState const &other) {
    // don't do anything if we already contain all names
    if (locals_.size() == 1 and *locals_.begin() == AnyName)
      return this;
    for (SEXP name : other.locals_)
      if (name == AnyName) {
        locals_.clear();
        locals_.insert(name);
        break; // no point in continuing, we already contain any
      } else {
        locals_.insert(name);
      }
    return this;
  }

private:
  std::set<SEXP> locals_;
};

class FrameVariables
    : public Analysis::Forward<FrameVariables, FrameVariablesState>,
      public ins::Dispatcher<FrameVariables, Scheduler> {
public:
  typedef FrameVariablesState State;

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "FrameVariables"; }

  static void initialize() {
    bind<SETVAR_OP::Predicate>(&FrameVariables::SETVAR_OP);
    bind<ENDASSIGN_OP::Predicate>(&FrameVariables::ENDASSIGN_OP);
    bind<STARTFOR_OP::Predicate>(&FrameVariables::STARTFOR_OP);
  }

protected:
  friend class reactor::Scheduler;

  explicit FrameVariables(Scheduler &scheduler)
      : Analysis::Forward<FrameVariables, FrameVariablesState>(scheduler) {}

  States *initialState() const override { return new States(new State()); }

private:
  void SETVAR_OP(Driver &d, SETVAR_OP::Encoding const &ins) {
    state().addLocal(ins.constant1(d.code()));
  }

  void SETVAR2_OP(Driver &d, SETVAR_OP::Encoding const &ins) {
    // TODO we don't track supers yet
  }

  void ENDASSIGN_OP(Driver &d, ENDASSIGN_OP::Encoding const &ins) {
    state().addLocal(ins.constant1(d.code()));
  }

  void STARTASSIGN2_OP(Driver &d, STARTASSIGN2_OP::Encoding const &ins) {
    // TODO we don't track supers yet
  }

  // This defines the index variable
  void STARTFOR_OP(Driver &d, STARTFOR_OP::Encoding const &ins) {
    state().addLocal(ins.constant2(d.code()));
  }
};

} // namespace analysis
} // namespace reactor

#endif // A_FRAMEVARIABLES_H
