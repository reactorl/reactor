#ifndef A_REACHINGDEFINITIONS_H
#define A_REACHINGDEFINITIONS_H

#include <set>

#include "bytecode/rbc.h"

#include "flair/scheduler.h"
#include "flair/analysis_forward.h"
#include "flair/state.h"

namespace reactor {
namespace analysis {

class ReachingDefinitionsValue_ {
public:
  ReachingDefinitionsValue_() = default;

  explicit ReachingDefinitionsValue_(size_t pc) { definitions_.insert(pc); }

  ReachingDefinitionsValue_(ReachingDefinitionsValue_ const &from) = default;

  /** Reaching definitions abstract values can always merge together.
   */
  bool canMergeWith(ReachingDefinitionsValue_ const &other) const {
    return true;
  }

  /** Merge simply merges the definition pcs from both sets together.
   */
  ReachingDefinitionsValue_ mergeWith(ReachingDefinitionsValue_ const &other) {
    ReachingDefinitionsValue_ result(*this);
    result.definitions_.insert(other.definitions_.begin(),
                               other.definitions_.end());
    return result;
  }

  /** One value is included in another is all its definitions are also in the
   * other.
   */
  bool operator<=(ReachingDefinitionsValue_ const &other) const {
    for (size_t i : definitions_)
      if (other.definitions_.find(i) == other.definitions_.end())
        return false;
    return true;
  }

  /** Sets the definition point for the value to the given pc.
   */
  void set(size_t pc) {
    definitions_.clear();
    definitions_.insert(pc);
  }

  /** Iterator to first reaching definition pc.
   */
  std::set<size_t>::const_iterator begin() const {
    return definitions_.begin();
  }

  /** Iterator after last reaching definition pc.
   */
  std::set<size_t>::const_iterator end() const { return definitions_.end(); }

private:
  /** Set of definitions for the given value. */
  std::set<size_t> definitions_;
};

/** Reaching definitions analysis.

  For each stack position and local variable stores the set of addresses with
  instructions producing the value.

  TODO this does not deal with super assigmments.

  TODO does not deal with environment removals either.

  */
class ReachingDefinitions
    : public Analysis::Forward<ReachingDefinitions,
                               AbstractState<ReachingDefinitionsValue_>>,
      public ins::Dispatcher<ReachingDefinitions, Scheduler> {
public:
  typedef AbstractState<ReachingDefinitionsValue_> State;

  static void initialize() {
    bind<Instruction::Predicate>(&ReachingDefinitions::instruction);
    bind<SETVAR_OP::Predicate>(&ReachingDefinitions::SETVAR);
    bind<STARTASSIGN_OP::Predicate>(&ReachingDefinitions::STARTASSIGN);
    // TODO need to add more specialized instructions here - assignments, etc
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "ReachingDefinitions"; }

protected:
  friend class reactor::Scheduler;

  ReachingDefinitions(Scheduler &scheduler)
      : Analysis::Forward<ReachingDefinitions, State>(scheduler) {}

  States *initialState() const override { return new States(new State()); }

private:
  /** SETVAR handler. Sets the definition of the given variable.
   */
  void SETVAR(Driver &driver, SETVAR_OP::Encoding const &ins) {
    instruction(driver, ins);
    state()[ins.constant1(driver.code())].set(pc());
  }

  /** Assignment start handler.

    TODO is this really where the value gets stored?
   */
  void STARTASSIGN(Driver &driver, STARTASSIGN_OP::Encoding const &ins) {
    instruction(driver, ins);
    state()[ins.constant1(driver.code())].set(pc());
  }

  /** This is generic handler for instructions. For each instruction pops the
   * definitions from the stack and writes new definitions with current pc for
   * the stack writes reported by the instruction.
   */
  void instruction(Driver &driver, encoding::OpcodeOnly const &ins) {
    state().genericStackHandler(ins, State::Value(pc()));
  }
};

} // namespace analysis
} // namespace reactor

#endif // A_REACHINGDEFINITIONS_H
