#ifndef A_SIDEEFFECT_H
#define A_SIDEEFFECT_H

#include "r/rinterface.h"
#include "bytecode/rbc.h"
#include "r/symbols.h"
#include "q/enum_bitset.h"

#include "flair/scheduler.h"
#include "flair/analysis_forward.h"
#include "flair/state.h"

namespace reactor {
namespace analysis {

enum class SideEffectFlag {
  NONE = 0,

  SUBSET = 1 << 0,
  SUBASSIGN = 1 << 1,
  DOLLAR_GETTER = 1 << 2,
  DOLLAR_SETTER = 1 << 3,
  GENERIC_GETTER = 1 << 4,
  GENERIC_SETTER = 1 << 5,
  GETTER = SUBSET | GENERIC_GETTER,
  SETTER = SUBASSIGN | GENERIC_SETTER,

  CALL_OP = 1 << 20,
  CALL_SPECIAL = 1 << 21,
  CALL_BUILTIN = 1 << 22,
  CALL = CALL_OP | CALL_SPECIAL | CALL_BUILTIN,

  ASSIGN = 1 << 25,
  READ = 1 << 26,
  SUPER = 1 << 27,
  SUPER_ASSIGN = ASSIGN | SUPER,

  SIDEEFFECT = SETTER | GETTER | CALL | ASSIGN
};

class SideEffectValue_ {
public:
  typedef SideEffectFlag Flag;
  typedef EnumBitset<Flag> Flags;

  /** Default constructor initializes to top.
   */
  SideEffectValue_() : value_(Flag::NONE) {}

  explicit SideEffectValue_(Flags value) : value_(value) {}

  SideEffectValue_(SideEffectValue_ const &other) : value_(other.value_) {}

  bool canMergeWith(SideEffectValue_ const &other) const { return true; }

  // TODO: this is stupid! We really want to return a pointer type here, but
  // ForwardAnalysis cannot handle it yet
  SideEffectValue_ *mergeWith(SideEffectValue_ const &other) const {
    return new SideEffectValue_(value_ | other.value_);
  }

  bool operator==(SideEffectValue_ const &other) const {
    return value_ == other.value_;
  }

  bool includes(SideEffectValue_ const &other) const {
    return (value_ | other.value_) == value_;
  }

  SideEffectValue_ &operator=(SideEffectValue_ const &other) {
    value_ = other.value_;
    return *this;
  }

  void add(Flag e) { value_ |= e; }

  bool has(Flag e) { return value_.has(e); }

  void checkAfter(encoding::OpcodeOnly const &ins) {}

private:
  Flags value_;
};

class SideEffect : public Analysis::Forward<SideEffect, SideEffectValue_>,
                   public ins::Dispatcher<SideEffect, Scheduler> {
public:
  typedef SideEffectValue_ State;
  typedef SideEffectFlag Flag;

  static void initialize() {
    bind<STARTASSIGN_OP::Predicate>(&SideEffect::flag<Flag::ASSIGN>);
    bind<STARTASSIGN_OP::Predicate>(&SideEffect::flag<Flag::SUPER_ASSIGN>);

    bind<GETTER_CALL_OP::Predicate>(&SideEffect::flag<Flag::GENERIC_GETTER>);
    bind<SETTER_CALL_OP::Predicate>(&SideEffect::flag<Flag::GENERIC_SETTER>);

    bind<STARTSUBSET_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<STARTSUBASSIGN_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);
    bind<STARTSUBSET2_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<STARTSUBASSIGN2_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);

    bind<VECSUBSET_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<VECSUBASSIGN_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);
    bind<VECSUBSET2_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<VECSUBASSIGN2_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);

    bind<MATSUBSET_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<MATSUBASSIGN_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);
    bind<MATSUBSET2_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<MATSUBASSIGN2_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);

    bind<STARTSUBSET_N_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<STARTSUBASSIGN_N_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);
    bind<STARTSUBSET2_N_OP::Predicate>(&SideEffect::flag<Flag::SUBSET>);
    bind<STARTSUBASSIGN2_N_OP::Predicate>(&SideEffect::flag<Flag::SUBASSIGN>);

    bind<DOLLAR_OP::Predicate>(&SideEffect::flag<Flag::DOLLAR_GETTER>);
    bind<DOLLARGETS_OP::Predicate>(&SideEffect::flag<Flag::DOLLAR_SETTER>);

    // bind<GETVAR_OP::Predicate>(&SideEffect::flag<Flag::NONE>);

    bind<SETVAR_OP::Predicate>(&SideEffect::flag<Flag::ASSIGN>);
    bind<SETVAR2_OP::Predicate>(&SideEffect::flag<Flag::SUPER_ASSIGN>);

    bind<CALL_OP::Predicate>(&SideEffect::flag<Flag::CALL_OP>);
    bind<DOTCALL_OP::Predicate>(&SideEffect::flag<Flag::CALL_BUILTIN>);
    bind<CALLBUILTIN_OP::Predicate>(&SideEffect::flag<Flag::CALL_BUILTIN>);
    bind<CALLSPECIAL_OP::Predicate>(&SideEffect::flag<Flag::CALL_SPECIAL>);
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "SideEffect"; }

protected:
  friend class reactor::Scheduler;

  SideEffect(Scheduler &scheduler)
      : Analysis::Forward<SideEffect, State>(scheduler) {}

  States *initialState() const override { return new States(new State()); }

private:
  template <Flag f> void flag(Driver &p, encoding::OpcodeOnly const &ins) {
    state().add(f);
  }
};

} // namespace analysis
} // namespace reactor

#endif // A_CONSTANTPROPAGATION_H
