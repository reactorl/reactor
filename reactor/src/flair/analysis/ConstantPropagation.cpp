#include <iostream>
#include "ConstantPropagation.h"

namespace reactor {
namespace analysis {

const SEXP ConstantPropagationValue_::Null = (SEXP)1;
const SEXP ConstantPropagationValue_::Logical = (SEXP)2;
const SEXP ConstantPropagationValue_::Integer = (SEXP)3;
const SEXP ConstantPropagationValue_::Double = (SEXP)4;
const SEXP ConstantPropagationValue_::Complex = (SEXP)5;
const SEXP ConstantPropagationValue_::Character = (SEXP)6;
const SEXP ConstantPropagationValue_::Symbol = (SEXP)7;

const SEXP ConstantPropagationValue_::SpecialRange = (SEXP)4095;
}
}
