#ifndef A_LIVENESS_H
#define A_LIVENESS_H

#include <set>

#include "../scheduler.h"
#include "../analysis_backward.h"

namespace reactor {
namespace analysis {

class LivenessState : public std::set<SEXP> {
public:
  void checkAfter(encoding::OpcodeOnly const &ins) {}

  bool includes(LivenessState const &other) const {
    for (auto n : other) {
      if (count(n) == 0)
        return false;
    }
    return true;
  }

  LivenessState *mergeWith(LivenessState const &other) {
    insert(other.begin(), other.end());
    return this;
  }
};

// TODO: just a poc, only looks at end of liveness range
class Liveness : public Analysis::Backward<Liveness, LivenessState>,
                 public ins::Dispatcher<Liveness, Scheduler> {
public:
  typedef LivenessState State;

  char const *name() const override { return "Liveness"; }

  static void initialize() { bind<GETVAR_OP::Predicate>(&Liveness::GETVAR_OP); }

protected:
  friend class reactor::Scheduler;

  explicit Liveness(Scheduler &scheduler)
      : Analysis::Backward<Liveness, LivenessState>(scheduler) {}

  States *initialState() const override { return new States(new State()); }

private:
  void GETVAR_OP(Driver &d, SETVAR_OP::Encoding const &ins) {
    state().insert(ins.constant1(d.code()));
  }
};
}
}

#endif
