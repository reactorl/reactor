#ifndef A_MERGEPOINTS_H
#define A_MERGEPOINTS_H

#include "../analysis.h"

#include <set>
#include <map>

namespace reactor {
namespace analysis {

/** Merge points analysis.

  Finds merge points in the code. A merge point is any instruction with two or
  more previous instructions. Each merge point is a basic block beginning, but
  not all basic block beginnings are merge points (consider a basic block with
  only one parent).

  For the purposes of the forward analyses, merge points are all beginnings of
  basic blocks, not just those who have more than one previous instruction. This
  allows faster analysis state reconstruction during the optimization phase.

 */
class MergePoints : public Analysis::SequentialPass<MergePoints>,
                    public ins::Dispatcher<MergePoints, Scheduler> {
public:
  static void initialize() {
    bind<Jump>(&MergePoints::jump);
    bind<ConditionalJump1>(&MergePoints::conditionalJump1);
    bind<ConditionalJump2>(&MergePoints::conditionalJump2);
    bind<STARTFOR_OP::Predicate>(&MergePoints::STARTFOR);
    bind<RETURN_OP::Predicate>(&MergePoints::returns);
    bind<RETURNJMP_OP::Predicate>(&MergePoints::returns);
    bind<Instruction::Predicate>(&MergePoints::instruction);
  }

  void invoke() override {
    // resize the visited, fill it with false
    assert_(visited_.empty());
    visited_.resize(code().code.size(), 0);

    // The first instruction is visited by definition
    nextInstruction(-1, 1);

    // sequential pass over all instructions
    Analysis::SequentialPass<MergePoints>::invoke();

#if 0
    std::cout << "mergepoints at: ";
    for (auto m : in_) {
      std::cout << m.first << " (";
      for (auto i : m.second)
        std::cout << i << ",";
      std::cout << "), ";
    }
    std::cout << std::endl;
#endif

    // clear the visited helper
    visited_.clear();
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "MergePoints"; }

  /** Returns true if the given pc is a merge point. False otherwise.
   */
  bool isMergePoint(size_t pc) const {
    assert_(isValid());
    return in_.count(pc) > 0;
  }

  bool isReturn(size_t pc) const {
    assert_(isValid());
    return exit_.count(pc) > 0;
  }

  const std::set<size_t> dominators(size_t pc) const {
    assert_(isValid());
    assert_(isMergePoint(pc));
    return in_.at(pc);
  }

  const std::set<size_t> &exit() const { return exit_; }

protected:
  friend class reactor::Scheduler;

  MergePoints(Scheduler &scheduler)
      : Analysis::SequentialPass<MergePoints>(scheduler) {}

private:
  /** When instruction is visited for the second time, we know it is a merge
   * point.
   */
  void nextInstruction(size_t from, size_t to) {
    size_t pred = visited_[to];
    if (pred != 0) {
      in_[to].insert(pred);
      in_[to].insert(from);
    } else {
      visited_[to] = from;
    }
  }

  /** Jumps are always considered to be merge points
   */
  void jump(size_t from, size_t to) {
    size_t pred = visited_[to];
    if (pred != 0)
      in_[to].insert(pred);
    else
      visited_[to] = from;
    in_[to].insert(from);
  }

  /** The next instruction will be visited by the main loop of the analysis.
   */
  void instruction(Driver &driver, encoding::OpcodeOnly const &ins) {
    nextInstruction(pc(), pc() + code().code.instructionSize(pc()));
  }

  /** Returns are terminating instructions. */
  void STARTFOR(Driver &driver, encoding::R3 const &ins) {
    jump(pc(), ins.arg3);
  }

  /** Returns are terminating instructions. */
  void returns(Driver &driver, encoding::OpcodeOnly const &ins) {
    exit_.insert(pc());
  }

  void jump(Driver &driver, encoding::R1 const &ins) { jump(pc(), ins.arg1); }

  /** A conditional jump visites the next instruction and the target.
   *  Both of them are treated as jumps, which unconditionally makes every
   *  basic block header a mergePoint.
   */
  void conditionalJump1(Driver &driver, encoding::R1 const &ins) {
    jump(pc(), pc() + code().code.instructionSize(pc()));
    jump(pc(), ins.arg1);
  }

  void conditionalJump2(Driver &driver, encoding::R2 const &ins) {
    jump(pc(), pc() + code().code.instructionSize(pc()));
    jump(pc(), ins.arg2);
  }

  void conditionalJump3(Driver &driver, encoding::R3 const &ins) {
    jump(pc(), pc() + code().code.instructionSize(pc()));
    jump(pc(), ins.arg3);
  }

  // TODO: use a better datastructure here
  std::map<size_t, std::set<size_t>> in_;
  std::vector<size_t> visited_;
  std::set<size_t> exit_;
};

} // namespace analysis
} // namespace reactor
#endif // A_MERGEPOINTS_H
