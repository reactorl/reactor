#ifndef A_STACKDEPTH_H
#define A_STACKDEPTH_H

#include "../analysis_forward.h"

namespace reactor {
namespace analysis {

class StackDepthState {
public:
  static const int UNDERRUN = -1;
  static const int INVALID = -2;
  // not used by the analysis itself, but might be useful for the stack depth of
  // bytecode version identifier
  static const int UNINITIALIZED = -3;

  explicit StackDepthState(int value) : value_(value) {}

  StackDepthState(StackDepthState const &from) = default;

  /** Stack depth state includes each other if they both have the same value.
   */
  bool includes(StackDepthState const &other) { return other.value_ == value_; }

  /** Merger of two states is always INVALID state because two different depths
    are not allowed any any mergepoint.

    However we still check that to be defensive.
   */
  StackDepthState *mergeWith(StackDepthState const &other) {
    if (other.value_ != value_)
      return new StackDepthState(INVALID);
    else
      return this;
  }

  int depth() const { return value_; }

protected:
  friend Analysis::Forward<StackDepth, StackDepthState>;

  /** There is nothing to check after stack depth analysis.

    We might in theory check that the stack depth reported is correct after
    adjusting for the instruction, but since the instruction handler already
    does precisely that and we do not expect this state to be reused elsewhere,
    there is no point checking.
    */
  void checkAfter(encoding::OpcodeOnly const &ins) {}

  void set(int value) { value_ = value; }

  friend class StackDepth;

private:
  int value_;
};

class StackDepth : public Analysis::Forward<StackDepth, StackDepthState>,
                   public ins::Dispatcher<StackDepth, Scheduler> {
public:
  typedef StackDepthState State;

  static void initialize() {
    bind<Instruction::Predicate>(&StackDepth::instruction);
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "StackDepth"; }

protected:
  friend class reactor::Scheduler;

  explicit StackDepth(Scheduler &scheduler)
      : Analysis::Forward<StackDepth, StackDepthState>(scheduler) {}

  States *initialState() const override { return new States(new State(0)); }

private:
  void instruction(Driver &driver, encoding::OpcodeOnly const &ins) {
    State &incomming = state();
    Instruction const &i = Instruction::get(ins.opcode);
    if (incomming.depth() < 0)
      return; // ignore any changes if the state is already an error
    else if (incomming.depth() < static_cast<int>(i.stackOut()))
      incomming.set(State::UNDERRUN);
    else
      incomming.set(incomming.depth() - i.stackOut() + i.stackIn());
  }
};

} // namespace analysis
} // namespace reactor

#endif // A_STACKDEPTH_H
