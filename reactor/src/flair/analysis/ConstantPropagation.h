#ifndef A_CONSTANTPROPAGATION_H
#define A_CONSTANTPROPAGATION_H

#include "r/rinterface.h"
#include "bytecode/rbc.h"
#include "r/symbols.h"

#include "flair/scheduler.h"
#include "flair/analysis_forward.h"
#include "flair/state.h"

namespace reactor {
namespace analysis {

/** Constant propagation value.

  The value determines whether particular object is a constant (SEXP
  corresponding to the actual value is present), or can be anything (nullptr
  instead of SEXP).

  TODO nothing is protected ATM.

  TODO only simple operators are done
 */
class ConstantPropagationValue_ {
public:
  /* TODO This should perhaps go to a shape analysis and we should keep constant
   * propagation fairly minimal?
   */
  static const SEXP Null;
  static const SEXP Logical;
  static const SEXP Integer;
  static const SEXP Double;
  static const SEXP Complex;
  static const SEXP Character;
  static const SEXP Symbol;

  static const SEXP SpecialRange;

  /** Default constructor initializes to top.
   */
  ConstantPropagationValue_() : value_(nullptr) {}

  /** Creates constant propagation value with given SEXP constant.
   */
  explicit ConstantPropagationValue_(SEXP value) : value_(value) {}

  /** Copy constructor just copies the SEXP.
   */
  ConstantPropagationValue_(ConstantPropagationValue_ const &other)
      : value_(other.value_) {}

  /** Constat propagation values can at the moment always merge together.

    TODO we might want to only merge if we don't go the top - but how to handle
    this in the optimizer.
   */
  bool canMergeWith(ConstantPropagationValue_ const &other) const {
    return true;
  }

  /** Merger of two identical constants is the same constant, merger of anything
   * else is top.
   */
  ConstantPropagationValue_ mergeWith(ConstantPropagationValue_ const &other) {
    if (value_ != nullptr and other.value_ != nullptr)
      return ConstantPropagationValue_(equal(value_, other.value_) ? value_
                                                                   : nullptr);
    else
      return ConstantPropagationValue_(nullptr);
  }

  /** One value is included in another if the first one is top, or if they are
   * both same constants.
   */
  bool operator<=(ConstantPropagationValue_ const &other) const {
    if (value_ == nullptr)
      return true;
    if (other.value_ != nullptr)
      return equal(value_, other.value_);
    return false;
  }

  /* Returns true if the value is constant, false otherwise.
   */
  bool isConstant() const { return value_ != nullptr; }

  /** Returns the constat value.

    Nullptr if not a constant.
   */
  SEXP value() const { return value_; }

  /** Assigns the given SEXP as the constant.
   */
  ConstantPropagationValue_ &operator=(SEXP value) {
    value_ = value;
    return *this;
  }

  /** Assignment for constant propagation values.
   */
  ConstantPropagationValue_ &operator=(ConstantPropagationValue_ const &other) {
    value_ = other.value_;
    return *this;
  }

  operator SEXP() const { return value_; }

private:
  SEXP value_;
};

class ConstantPropagation
    : public Analysis::Forward<ConstantPropagation,
                               AbstractState<ConstantPropagationValue_>>,
      public ins::Dispatcher<ConstantPropagation, Scheduler> {
public:
  typedef AbstractState<ConstantPropagationValue_> State;

  static void initialize() {
    bind<LDCONST_OP::Predicate>(&ConstantPropagation::LDCONST_OP);
    bind<SETVAR_OP::Predicate>(&ConstantPropagation::SETVAR_OP);
    bind<GETVAR_OP::Predicate>(&ConstantPropagation::GETVAR_OP);

    bind<UMINUS_OP::Predicate>(&ConstantPropagation::UMINUS_OP);
    bind<UPLUS_OP::Predicate>(&ConstantPropagation::UPLUS_OP);

    bind<ADD_OP::Predicate>(&ConstantPropagation::ADD_OP);
    bind<SUB_OP::Predicate>(&ConstantPropagation::SUB_OP);
    bind<MUL_OP::Predicate>(&ConstantPropagation::MUL_OP);
    bind<DIV_OP::Predicate>(&ConstantPropagation::DIV_OP);
    bind<EXPT_OP::Predicate>(&ConstantPropagation::EXPT_OP);
    bind<SQRT_OP::Predicate>(&ConstantPropagation::SQRT_OP);
    bind<EXP_OP::Predicate>(&ConstantPropagation::EXP_OP);
    bind<EQ_OP::Predicate>(&ConstantPropagation::EQ_OP);
    bind<NE_OP::Predicate>(&ConstantPropagation::NE_OP);
    bind<LT_OP::Predicate>(&ConstantPropagation::LT_OP);
    bind<LE_OP::Predicate>(&ConstantPropagation::LE_OP);
    bind<GE_OP::Predicate>(&ConstantPropagation::GE_OP);
    bind<GT_OP::Predicate>(&ConstantPropagation::GT_OP);
    bind<AND_OP::Predicate>(&ConstantPropagation::AND_OP);
    bind<OR_OP::Predicate>(&ConstantPropagation::OR_OP);

    bind<ISNULL_OP::Predicate>(&ConstantPropagation::ISNULL_OP);
    bind<ISLOGICAL_OP::Predicate>(&ConstantPropagation::ISLOGICAL_OP);
    bind<ISINTEGER_OP::Predicate>(&ConstantPropagation::ISINTEGER_OP);
    bind<ISDOUBLE_OP::Predicate>(&ConstantPropagation::ISDOUBLE_OP);
    bind<ISCOMPLEX_OP::Predicate>(&ConstantPropagation::ISCOMPLEX_OP);
    bind<ISCHARACTER_OP::Predicate>(&ConstantPropagation::ISCHARACTER_OP);
    bind<ISSYMBOL_OP::Predicate>(&ConstantPropagation::ISSYMBOL_OP);
    bind<ISOBJECT_OP::Predicate>(&ConstantPropagation::ISOBJECT_OP);
    bind<ISNUMERIC_OP::Predicate>(&ConstantPropagation::ISNUMERIC_OP);

    bind<CALL_OP::Predicate>(&ConstantPropagation::CALL_OP);
    bind<Instruction::Predicate>(&ConstantPropagation::Instruction);
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "ConstantPropagation"; }

protected:
  friend class reactor::Scheduler;

  ConstantPropagation(Scheduler &scheduler)
      : Analysis::Forward<ConstantPropagation, State>(scheduler) {}

  States *initialState() const override { return new States(new State()); }

private:
  /** LDCONST_OP simply pushes given constant on the stack.
   */
  void LDCONST_OP(Driver &driver, LDCONST_OP::Encoding const &ins) {
    state().push(State::Value(ins.constant1(driver.code())));
  }

  /** SETVAR_OP reads the top of the stack and stores it to the given variable.

    Whatever the abstract state of stack's top was is just copied to the
    variable's value.
   */
  void SETVAR_OP(Driver &driver, SETVAR_OP::Encoding const &ins) {
    state()[ins.constant1(driver.code())] = state()[1];
  }

  /** GETVAR_OP reads the variable and pushes its value on the stack.
   */
  void GETVAR_OP(Driver &driver, GETVAR_OP::Encoding const &ins) {
    state().push(state()[ins.constant1(driver.code())]);
  }

  /** Unary operators.
   */

  void unaryOperator(SEXP symbol) {
    State::Value &x = state()[2];
    // if both inputs are constants, calculate the result
    if (x.isConstant())
      x = Rf_eval(Rf_lang2(symbol, x), R_BaseEnv);
    else
      x = nullptr;
  }

  void UMINUS_OP(Driver &driver, UMINUS_OP::Encoding const &ins) {
    unaryOperator(Symbol::minus);
  }

  void UPLUS_OP(Driver &driver, UPLUS_OP::Encoding const &ins) {
    unaryOperator(Symbol::plus);
  }

  /** Binary operators are constant folded by calling the respective R
    functions.

    TODO Is callback to R really what we want here? It will have the correct
    semantics, but be likely very slow, which might not be what we want from an
    analysis.
   */
  void binaryOperator(SEXP symbol) {
    State::Value &x = state()[2];
    State::Value &y = state()[1];
    // if both inputs are constants, calculate the result
    if (x.isConstant() and y.isConstant())
      x = Rf_eval(Rf_lang3(symbol, x, y), R_BaseEnv);
    else
      x = nullptr;
    state().pop();
  }

  void ADD_OP(Driver &driver, ADD_OP::Encoding const &ins) {
    binaryOperator(Symbol::plus);
  }

  void SUB_OP(Driver &driver, SUB_OP::Encoding const &ins) {
    binaryOperator(Symbol::minus);
  }

  void MUL_OP(Driver &driver, MUL_OP::Encoding const &ins) {
    binaryOperator(Symbol::times);
  }

  void DIV_OP(Driver &driver, DIV_OP::Encoding const &ins) {
    binaryOperator(Symbol::div);
  }

  void EXPT_OP(Driver &driver, EXPT_OP::Encoding const &ins) {
    binaryOperator(Symbol::expt);
  }

  void SQRT_OP(Driver &driver, SQRT_OP::Encoding const &ins) {
    binaryOperator(Symbol::sqrt);
  }

  void EXP_OP(Driver &driver, EXP_OP::Encoding const &ins) {
    binaryOperator(Symbol::exp);
  }

  /** Relational operators. Their result is always logical, otherwise similar to
   * binary operators.
   */
  void relationalOperator(SEXP symbol) {
    State::Value &x = state()[2];
    State::Value &y = state()[1];
    // if both inputs are constants, calculate the result
    if (x.isConstant() and y.isConstant())
      x = Rf_eval(Rf_lang3(symbol, x, y), R_BaseEnv);
    else
      x = State::Value::Logical;
    state().pop();
  }

  void EQ_OP(Driver &driver, EQ_OP::Encoding const &ins) {
    relationalOperator(Symbol::eq);
  }

  void NE_OP(Driver &driver, NE_OP::Encoding const &ins) {
    relationalOperator(Symbol::ne);
  }

  void LT_OP(Driver &driver, LT_OP::Encoding const &ins) {
    relationalOperator(Symbol::lt);
  }

  void LE_OP(Driver &driver, LE_OP::Encoding const &ins) {
    relationalOperator(Symbol::le);
  }

  void GE_OP(Driver &driver, GE_OP::Encoding const &ins) {
    relationalOperator(Symbol::ge);
  }

  void GT_OP(Driver &driver, GT_OP::Encoding const &ins) {
    relationalOperator(Symbol::gt);
  }

  /** Bitwise operators - currently using binary operator methods.
   */

  void AND_OP(Driver &driver, AND_OP::Encoding const &ins) {
    binaryOperator(Symbol::and_);
  }

  void OR_OP(Driver &driver, OR_OP::Encoding const &ins) {
    binaryOperator(Symbol::or_);
  }

  /** Call instruction discards all variables because the frame might have been
   * compromised, but keeps stack.
   */
  void CALL_OP(Driver &driver, CALL_OP::Encoding const &ins) {
    // result of a call is Top
    state().genericStackHandler(ins, State::Value());
    // set all variables to top as we can't be sure whether they have changed or
    // not
    state().setAllVariables(State::Value());
  }

  /** Generic type test.

    First checks if the value is generic type, which is enough to determine for
    the is... questions. Then checks the actual constant. In the worst case the
    result of this operation will always be Logical.
   */
  void test(Rboolean (*testFunction)(SEXP), SEXP type = nullptr) {
    bool result = false;
    if (type != nullptr and state()[1] <= State::Value::SpecialRange) {
      if (state()[1] == type)
        result = true;
      else
        result = false;
    } else if (state()[1].isConstant()) {
      result = testFunction(state()[1]);
    } else {
      state()[1] = State::Value::Logical; // it can be any logical now
    }
    state()[1] = LogicalVector({result});
  }

  void ISNULL_OP(Driver &driver, ISNULL_OP::Encoding const &ins) {
    test(Rf_isNull, State::Value::Null);
  }

  void ISLOGICAL_OP(Driver &driver, ISLOGICAL_OP::Encoding const &ins) {
    test(Rf_isLogical, State::Value::Logical);
  }

  void ISINTEGER_OP(Driver &driver, ISINTEGER_OP::Encoding const &ins) {
    test(Rf_isInteger, State::Value::Integer);
  }

  void ISDOUBLE_OP(Driver &driver, ISDOUBLE_OP::Encoding const &ins) {
    test(Rf_isReal, State::Value::Double);
  }

  void ISCOMPLEX_OP(Driver &driver, ISCOMPLEX_OP::Encoding const &ins) {
    test(Rf_isComplex, State::Value::Complex);
  }

  void ISCHARACTER_OP(Driver &driver, ISCHARACTER_OP::Encoding const &ins) {
    test([](SEXP c) { return rbool(isType<sexp_t::Char>(c)); },
         State::Value::Character);
  }

  void ISSYMBOL_OP(Driver &driver, ISSYMBOL_OP::Encoding const &ins) {
    test(Rf_isSymbol, State::Value::Symbol);
  }

  void ISOBJECT_OP(Driver &driver, ISOBJECT_OP::Encoding const &ins) {
    test(Rf_isObject);
  }

  /** Is numeric checks whether the stored value is either integer or double
    first.

    If the stack top is constant it is then checked too.
   */
  void ISNUMERIC_OP(Driver &driver, ISNUMERIC_OP::Encoding const &ins) {
    if (state()[1] <= State::Value::SpecialRange) {
      if (state()[1] == State::Value::Integer or
          state()[1] == State::Value::Double)
        state()[1] = LogicalVector({true});
      else
        state()[1] = LogicalVector({false});
    } else {
      test(Rf_isNumeric);
    }
  }

  /** For generic instruction, non-const values are created on stack. This is a
   * failsafe for instructions we do not want to bother with.
   */
  void Instruction(Driver &driver, encoding::OpcodeOnly const &ins) {
    state().genericStackHandler(ins, State::Value());
  }
};

} // namespace analysis
} // namespace reactor

#endif // A_CONSTANTPROPAGATION_H
