#ifndef PC_MAP_H
#define PC_MAP_H

#include "../analysis.h"

namespace reactor {

class PcMapperTest;

namespace analysis {

class Cursor;
class RCursor;

class PcMapper : public Analysis::SequentialPass<PcMapper>,
                 public ins::Dispatcher<PcMapper, Scheduler> {
public:
  static void initialize() {
    bind<Instruction::Predicate>(&PcMapper::recordPc);
  }

  char const *name() const override { return "PcMapper"; }

  void invoke() override {
    instruction.clear();
    Analysis::SequentialPass<PcMapper>::invoke();
  }

  bool isValidPc(size_t pc) const {
    for (auto p : instruction)
      if (p == pc)
        return true;
    return false;
  }

  size_t size() const { return instruction.size(); }

  inline Cursor iterator(size_t pc) const;
  inline RCursor riterator(size_t pc) const;

  inline Cursor iterator(size_t pc, size_t target) const;
  inline RCursor riterator(size_t pc, size_t target) const;

protected:
  size_t maxPc() const { return size() > 0 ? *instruction.rbegin() : 0; }

  friend reactor::Scheduler;
  friend PcMapperTest;
  friend Cursor;
  friend RCursor;

  PcMapper(Scheduler &scheduler)
      : Analysis::SequentialPass<PcMapper>(scheduler) {}

  size_t pcOf(size_t i) const {
    assert_(i < size());
    return instruction[i];
  }

private:
  size_t idxOf(size_t pc) const {
    assert_(pc <= maxPc());
    assert_(pc >= 1);
    assert_(isValidPc(pc));

    return find(pc);
  }

  size_t find(size_t pc) const {
    size_t i = pc / 2 < size() ? pc / 2 : size() - 1;

    size_t p = pcOf(i);

    if (p < pc) {
      while (p < pc) {
        i++;
        if (i == size())
          return i - 1;
        p = pcOf(i);
      }
      return i;
    }

    while (p > pc) {
      i--;
      p = pcOf(i);
    }
    return i;
  }

  void recordPc(Driver &d, encoding::OpcodeOnly const &ins) {
    instruction.push_back(pc());
  }

  std::vector<size_t> instruction;
};

class Cursor {
public:
  Cursor(const PcMapper &map, size_t pc, size_t target)
      : map(map), cur_(map.find(pc)), target(target) {}
  Cursor(const PcMapper &map, size_t pc) : Cursor(map, pc, map.maxPc()) {}

  size_t cur() const { return eof() ? -1 : map.pcOf(cur_); }

  size_t operator*() const { return cur(); }

  bool eof() const { return cur_ == map.size(); }

  bool done() const { return eof() || cur() >= target; }

  Cursor &operator++() {
    cur_++;
    return *this;
  }

protected:
  const PcMapper &map;
  size_t cur_;
  const size_t target;
};

class RCursor : public Cursor {
public:
  RCursor(const PcMapper &map, size_t pc, size_t target)
      : Cursor(map, pc, target) {}
  RCursor(const PcMapper &map, size_t pc) : RCursor(map, pc, 1) {}

  bool eof() const { return cur_ == 0; }

  bool done() const { return eof() || cur() <= target; }

  RCursor &operator++() {
    cur_--;
    return *this;
  }
};

Cursor PcMapper::iterator(size_t pc) const { return Cursor(*this, pc); }

RCursor PcMapper::riterator(size_t pc) const { return RCursor(*this, pc); }

Cursor PcMapper::iterator(size_t pc, size_t target) const {
  return Cursor(*this, pc, target);
}

RCursor PcMapper::riterator(size_t pc, size_t target) const {
  return RCursor(*this, pc, target);
}
}
}

#endif
