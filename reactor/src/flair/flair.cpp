
#include <iostream>
#include <vector>

#include "bytecode/code_buffer.h"
#include "bytecode/rbc.h"
#include "r/vector.h"
#include "raptor/fixup.h"

#include "utils.h"

#include "inliner.h"
#include "printer.h"
#include "bc_verifier.h"
#include "stack_depth.h"
#include "invisible.h"

#include "scheduler.h"
#include "analysis/MergePoints.h"
#include "analysis/StackDepth.h"

using namespace std;

using namespace reactor;

/** Prints the given bytecode in flair's format.

  If the argument is a compiled function, the function prints its bytecode
  instead. Returns the bytecode object it printed.
  */
REXPORT SEXP flair_disassemble(SEXP bytecode) {
  Printer::execute(bytecode);
  return bytecode;
}

REXPORT SEXP flair_mergePoints(SEXP bytecode) {
  CodeBuffer code(bytecode);
  Scheduler s(code);
  vector<size_t> mergePoints;
  analysis::MergePoints const &mp = s.mergePoints();
  for (size_t i = 0; i < code.code.size(); ++i)
    if (mp.isMergePoint(i))
      mergePoints.push_back(i);
  IntVector result(mergePoints.size());
  for (size_t i = 0; i < mergePoints.size(); ++i)
    result.set(i, mergePoints[i]);
  return result;
}

REXPORT SEXP flair_stackDepth(SEXP bytecode) {
  CodeBuffer code(bytecode);
  Scheduler s(code);
  analysis::StackDepth const &sd = s.stackDepth();
  IntVector result(code.code.size() + 1);
  result.set(0, analysis::StackDepth::State::UNINITIALIZED);
  for (size_t i = 1; i <= code.code.size(); ++i)
    result.set(i, sd.state(i).depth());
  return result;
}

/** Verifies the given bytecode.

  Checks that for any given pc, stack depth is always statically known and
  non-negative.
  */
REXPORT SEXP flair_verify(SEXP bytecode) {
  CodeBuffer b(bytecode);
  LogicalVector result(1);
  result.set(0, BCVerifier::check(b) ? 1 : 0);
  return result;
}

/** Returns the result of the stack depth analysis for the given bytecode.

  Returns an array of integers as long as the original code. Each element
  contains the calculated stack depth at that particular location. -1 means that
  the location is not a beginning of an instruction, or that the instruction was
  never visited. -2 stands for stack underrun, i.e. the stack depth would have
  been smaller than the number of stack elements the instruction pops and -3
  means that two execution paths arrived to the instruction with different
  stack depths.
  */
REXPORT SEXP flair_stackDepth2(SEXP bytecode) {
  CodeBuffer cb(bytecode);
  StackDepth::Result const *r = StackDepth::analyze(bytecode);
  IntVector result(r->size());
  for (size_t i = 0; i < r->size(); ++i)
    result.set(i, (*r)[i]);
  delete r;
  return result;
}

/** Returns a character list of all local variables of the given function.
 */
REXPORT SEXP flair_locals(SEXP closure) {
  CodeBuffer cb(closure);
  LocalVariables::Result *r = LocalVariables::execute(cb);
  std::vector<SEXP> const &l = r->locals();
  CharacterVector result(l.size());
  for (size_t i = 0; i < l.size(); ++i)
    result.set(i, PRINTNAME(l[i]));
  delete r;
  return result;
}

/** Scrubs unnecessary INVISIBLE_OP instructions.
 */
REXPORT SEXP flair_invisible(SEXP bytecode) {
  return Invisible::execute(bytecode);
}

/** Returns a character list of all super variables the function writes to.
 */
REXPORT SEXP flair_supers(SEXP closure) {
  CodeBuffer cb(closure);
  LocalVariables::Result *r = LocalVariables::execute(cb);
  std::vector<SEXP> const &s = r->supers();
  CharacterVector result(s.size());
  for (size_t i = 0; i < s.size(); ++i)
    result.set(i, PRINTNAME(s[i]));
  delete r;
  return result;
}
