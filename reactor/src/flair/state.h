#ifndef STATE_H
#define STATE_H

#include <map>
#include <vector>
#include <memory>
#include <algorithm>

#include "reactor.h"
#include "bytecode/instruction.h"

namespace reactor {

//
/** State for abstract values on stack and variables.

  The abstract value must support:

  - default constructor initializing it to a state that makes sense (top or
  bottom generally)
  - copy constructor
  - lattice comparison operator <= returning true if dominated
  - canMergeWith method that returns true if the merge can be performed
  - mergeWith that performs the merge. There must always be a way to merge two
  values together. However in normal cases merge will not be called unless
  canMergeWith returned true.


  TODO we might also want to have another map for super variables. But I am not
  yet sure how to implement this efficiently and usefully. Perhaps having just
  one map for the supers, which would work unless someone will be shadowing
  functions or removing from environments.

  As for the environments - we might even consider an entirely different scheme,
  where we will track environments rather than variables in frame, but the
  interface to the analyses should remain the same.
 */
template <typename ABSTRACT_VALUE> class AbstractState {
public:
  typedef ABSTRACT_VALUE Value;

  AbstractState() = default;

  /** Copy constructor. Copies the stack and all variables from the original.
   */
  AbstractState(AbstractState<ABSTRACT_VALUE> const &from) {
    for (ABSTRACT_VALUE const &v : from.stack_)
      stack_.push_back(v);
    for (auto &kv : from.variables_)
      variables_.insert(kv);
#ifdef DEBUG
    incomming_ = from.incomming_;
#endif
  }

  /** Returns the abstract value of index-th stack element from top.

Top has index of 1 (this allows nice disambiguation of 0 being both SEXP and
size_t).
   */
  ABSTRACT_VALUE &operator[](size_t index) {
    assert_(index > 0 and index <= stack_.size());
    return stack_[stack_.size() - index];
  }

  /** Returns the abstract value of index-th stack element from top.

Top has index of 1 (this allows nice disambiguation of 0 being both SEXP and
size_t).
*/
  ABSTRACT_VALUE const &operator[](size_t index) const {
    assert_(index > 0 and index <= stack_.size());
    return stack_[stack_.size() - index];
  }

  /** Returns the abstract value of given variable.

    If the variable has not been initialized yet, creates new abstract value and
    returns it. Therefore it is important for abstract value default
    constructors to always construct the bottom state.
   */
  ABSTRACT_VALUE &operator[](SEXP name) { return variables_[name]; }

  /** Returns the abstract value of given variable.

    If the variable has not been initialized yet, creates new abstract value and
    returns it. Therefore it is important for abstract value default
    constructors to always construct the bottom state.
   */
  ABSTRACT_VALUE const &operator[](SEXP name) const {
    return variables_.at(name);
  }

  /** Pops the value off the stack and returns it.
   */
  ABSTRACT_VALUE pop() {
    assert_(not stack_.empty());
    ABSTRACT_VALUE result(stack_.back());
    stack_.pop_back();
    return result;
  }

  /** Pushes the given value on stack.
   */
  size_t push(ABSTRACT_VALUE const &value) {
    stack_.push_back(value);
    return stack_.size() - 1;
  }

  /** Returns the current stack depth.
   */
  size_t stackDepth() const { return stack_.size(); }

  /** Returns true if the other state is already included in current.

    This means that all its stack values are dominated by the values in current,
    all variables are present in current as well and are dominated too. To
    assess the dominance, abstract values must override the <= operator.
   */
  bool includes(AbstractState<ABSTRACT_VALUE> const &other) {
    assert_(stack_.size() == other.stack_.size());
    // check the stack values
    for (size_t i = 0; i < stack_.size(); ++i)
      if (not(other.stack_[i] <= stack_[i]))
        return false;
    // check the variables - they must be present and smaller or equal
    for (auto &kv : other.variables_) {
      auto i = variables_.find(kv.first);
      if (i == variables_.end())
        return false;
      if (not(kv.second <= i->second))
        return false;
    }
    return true;
  }

  /** Merges the state with another one, returning the merged state.

    If the state cannot be merged, returns null.
   */
  AbstractState<ABSTRACT_VALUE> *
  mergeWith(AbstractState<ABSTRACT_VALUE> const &other) {
    assert_(stack_.size() == other.stack_.size());
    std::unique_ptr<AbstractState<ABSTRACT_VALUE>> result(
        new AbstractState<ABSTRACT_VALUE>());
    // first merge the stack values
    for (size_t i = 0; i < stack_.size(); ++i) {
      if (stack_[i].canMergeWith(other.stack_[i]))
        result->stack_.push_back(stack_[i].mergeWith(other.stack_[i]));
      else
        return nullptr;
    }
    // now for each variable we have create the merge
    for (auto &kv : variables_) {
      auto i = other.variables_.find(kv.first);
      // only copy if the variable is only in current
      if (i == other.variables_.end()) {
        result->variables_.insert(kv);
      } else {
        // if in both, try the merge
        if (kv.second.canMergeWith(i->second))
          result->variables_.insert({kv.first, kv.second.mergeWith(i->second)});
        else
          return nullptr;
      }
    }
    // and finally copy all variables from other that are not in current
    auto own = variables_.begin();
    auto their = other.variables_.begin();
    while (own != variables_.end() and their != other.variables_.end()) {
      if (their->first < own->first) {
        result->variables_.insert(*their);
        ++their;
      } else if (own->first < their->first) {
        ++own;
      } else {
        ++own;
        ++their;
      }
    }
    return result.release();
  }

  /** Proper behavior wrt the stack check.

    In release builds the method is empty and therefore will disappear after the
    inlining. In debug builds it checks that the analysis correctly updated the
    stack in terms of stack height as declared by the bytecode definitions.
   */
  void checkAfter(encoding::OpcodeOnly const &ins) {
#ifdef DEBUG
    Instruction const &i = Instruction::get(ins.opcode);
    assert_eq(incomming_ - i.stackOut() + i.stackIn(), stack_.size(),
              "Analysis stack state after instruction does not correspond to "
              "the declared stack properties");
    incomming_ = stack_.size();
#endif
  }

  /** Generic stack handler for instructions.

    Uses the information about instruction's stack behavior to pop and push the
    appropriate number of stack values. The supplied value is used as the
    inserted value.
   */
  void genericStackHandler(encoding::OpcodeOnly const &ins,
                           ABSTRACT_VALUE const &value) {
    Instruction const &i = Instruction::get(ins.opcode);
    for (size_t j = 0; j < i.stackOut(); ++j)
      pop();
    for (size_t j = 0; j < i.stackIn(); ++j)
      push(value);
  }

  /** Sets all variables to given abstract state. Leaves stack as is.

    Useful after a generic call when we can't be sure whether it has changed our
    environment or not.
   */
  void setAllVariables(ABSTRACT_VALUE const &value) {
    for (auto i : variables_)
      i.second = value;
  }

private:
  /** Variables in current environment.
   */
  std::map<SEXP, ABSTRACT_VALUE> variables_;

  /** Variables on stack.
   */
  std::vector<ABSTRACT_VALUE> stack_;

/** incomming stack height for the checkAfter method
 */
#ifdef DEBUG
  size_t incomming_ = 0;
#endif
};

} // namespace reactor

#endif // STATE_H
