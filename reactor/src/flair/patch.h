#ifndef PATCH_H
#define PATCH_H

#include <vector>
#include <list>

#include "reactor.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"

namespace reactor {

class Patch {
public:
  Patch(CodeBuffer &code) : code_(code) {}

  /** Returns true if the given pc in the old code will still be valid after the
    patch is applied, that is if the instruction has not been deleted by the
    patch already.

    If the instruction is a jump, whose target address will change after the
    patch, it is still considered to be valid.
   */
  bool isValid(size_t pc) const {
    return std::find(erased_.begin(), erased_.end(), pc) == erased_.end();
  }

  /** Removes the specified instruction from the code as part of the patch.

   */
  void erase(size_t pc) {
    assert_true(isValid(pc), "Cannot erase already erased instruction");
    erased_.insert(std::upper_bound(erased_.begin(), erased_.end(), pc), pc);
  }

  /** Returns new code chunk that can be filled with instruction that will be
    inserted at the given pc.

    The patch inserts the instructions in chunks. Each chunk consists of a
    consecutive list of instructions that will be inserted starting from the
    given pc in the original code. Instructions inside a single chunk can
    referece each other using their pc in the chunk offseted by the number of
    instructions in the original code. Any references to the original code
    should use the old original code addresses.

    If more than one chunks are applied to same original pc, the chunks will be
    chained in the order they were inserted to the patch, i.e. inserting chunks
    A, B, C from address 10 will result in the code being:

    original code 9
    -- pc of original code 10 --
    chunk A
    chunk B
    chunk C
    original code 10
    */
  CodeBuffer::Code &insertChunk(size_t pc) {
    std::list<CodeBuffer::Code> &chunks = inserted_[pc];
    chunks.push_back(CodeBuffer::Code());
    return chunks.back();
  }

  /** When a constant is added, it is immediately added to the original code as
    this does not violate any of the analyses.

    Returns the index of the given constant in the constant pool.
   */
  size_t addConstant(SEXP constant) { return code_.constants.add(constant); }

  /** Applies the patch to the code view it was created with.
   */
  void apply() {
    Applier a(*this);
    a.apply();
    erased_.clear();
    inserted_.clear();
  }

  /** Returns true if the patch has no changes in it.
   */
  bool empty() const { return erased_.empty() and inserted_.empty(); }

private:
  friend class Applier;

  /** Dispatcher to insert and recalculate jumps in code to be inserted in the
    patch while applying the changes.

    The dispatcher inserts all the instructions into the code. Any jumps that go
    inside the inserted code must be fixed to work in the result. Recognizing
    these jumps is simple too as any jump target that is higher than the
    original code size is treated as an internal jump to address offseted by the
    original code size.

    Any jumps to original addresses must either be fixed if the targets are
    already known, or added to the fixup list where they will be fixed at the
    end of patch apply.
   */
  class Applier : public ins::Dispatcher<Applier, Applier> {
  public:
    static void initialize() {
      bind<Jump>(&Applier::jump);
      bind<ConditionalJump2>(&Applier::conditionalJump2);
      bind<STARTFOR_OP::Predicate>(&Applier::STARTFOR);
    }

    Applier(Patch &patch) : patch_(patch) {}

    /** Applies the associated patch to the code.
     */
    void apply() {
      size_t originalPc = 1;
      pc_ = 1;
      originalSize_ = patch_.code_.code.size();
      size_t erases = 0;
      while (originalPc < originalSize_) {
        // first check if there is anything that should be inserted
        auto inserts = patch_.inserted_.find(originalPc);
        if (inserts != patch_.inserted_.end()) {
          for (CodeBuffer::Code &chunk : inserts->second) {
            // for each chunk, remember its chunk start as current pc
            chunkStart_ = pc_;
            // insert the chunk at once to avoid extra mem copies
            patch_.code_.code.insert(pc_, chunk);
            // walk the inserted code and update jumps
            for (; pc_ < chunkStart_ + chunk.size() - 1;
                 pc_ += patch_.code_.code.instructionSize(pc_))
              dispatch(*this, patch_.code_, pc_);
          }
        }
        // update the transitions map
        transitions_[originalPc] = pc_;
        // now check if we need to erase the original pc instruction
        size_t size = patch_.code_.code.instructionSize(pc_);
        if (erases < patch_.erased_.size() and
            patch_.erased_[erases] == originalPc) {
          // erase the instruction and move originalPc, but not pc_
          patch_.code_.code.erase(pc_);
          ++erases;
        } else {
          // do not erase the instruction, increase pc and originalPc
          pc_ += size;
        }
        originalPc += size;
      }
      // fixup the instructions now that all original addresses are known
      for (size_t i : fixups_)
        dispatch(*this, patch_.code_, i);
    }

  private:
    /** Checks given target jump and returns the value it should be replaced
      with.

      If the jump is within the chunk (that is its traget is greater than
      original code size, it is updated to point to the proper instruction once
      the chunk is inserted in the code, otherwise if the target address in the
      patched code is already known the target is patched.

      If the target is not known yet, the instruction is added to fixups that
      will be updated when all code is patched.
     */
    size_t processJump(size_t target) {
      if (target < originalSize_) {
        auto i = transitions_.find(target);
        if (i != transitions_.end()) {
          return i->second;
        } else {
          // add to fixups, we do not know the target yet
          fixups_.push_back(pc_);
          // just return the target for now, it will be replaced when it becomes
          // known
          return target;
        }
      } else {
        // it's jump within the chunk
        // offset target by the beginning of the currently running chunk
        return target - originalSize_ - 1 + chunkStart_;
      }
    }

    void jump(Driver &driver, encoding::R1 const &ins) {
      const_cast<encoding::R1 &>(ins).arg1 = processJump(ins.arg1);
    }

    void conditionalJump2(Driver &driver, encoding::R2 const &ins) {
      const_cast<encoding::R2 &>(ins).arg2 = processJump(ins.arg2);
    }

    void STARTFOR(Driver &driver, encoding::R3 const &ins) {
      const_cast<encoding::R3 &>(ins).arg3 = processJump(ins.arg3);
    }

    Patch &patch_;

    size_t originalSize_;
    size_t pc_;
    size_t chunkStart_;

    /** During the apply phase, this lists known indices of the original
      instructions.

      Not being in the map means the instruction has not been fixed yet. If the
      instruction no longer exists, its address is the next instruction after in
      the new code (this can either be an inserted instruction, or next original
      instruction).
     */
    std::map<size_t, size_t> transitions_;

    std::vector<size_t> fixups_;
  };

  /** Ordered vector containing the erased instructions from the original code.

    TODO this is perhaps not the best representation.
   */
  std::vector<size_t> erased_;

  /** Map containing inserted code at each defined original pc.

    TODO this is perhaps not the best representation.
   */
  std::map<size_t, std::list<CodeBuffer::Code>> inserted_;

  /** Code the patch operates on.
   */
  CodeBuffer &code_;
};

} // namespace reactor

#endif // PATCH_H
