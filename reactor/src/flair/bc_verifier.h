#ifndef BC_VERIFIER
#define BC_VERIFIER

#include <vector>

#include "reactor.h"
#include "bytecode/instruction.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"
#include "stack_depth.h"

namespace reactor {

class BCVerifier : public StackDepth {
public:
  static bool check(CodeBuffer const &code) {
    Result *r = StackDepth::analyze(code);
    bool result = true;
    for (int i : r->data())
      if (i < StackDepth::Result::UNINITIALIZED) {
        result = false;
        break;
      }
    delete r;
    return result;
  }
};

} // namespace flair

#endif // BC_VERIFIER
