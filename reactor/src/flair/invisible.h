#ifndef INVISIBLE_H
#define INVISIBLE_H

#include "reactor.h"
#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"

namespace reactor {

/** Performs static analysis to remove unnecessary INVISIBLE_OP instructions.

  There are likely multiple scenarios when these INVISIBLE_OP instructions are
  unnecessary.
  So far, this handles the following:
  1) Variables popped off the stack.

  For scenario 1) above, dispatcher with bind 3 methods:
  1: INVISIBLE_OP - this will set a flag indicating current instruction is
  INVISIBLE_OP.
  2: POP_OP - Check flag (from 1: above) indicating the prior instruction was
  INVISIBLE_OP.
      If so, will remove the preceding INVISIBLE_OP & clear flag.
  3: Other Instructions - Clear the flag (from 1: above).

*/
class Invisible : public ins::Dispatcher<Invisible, Pass::Sequential> {
public:
  Invisible() : isInvis_(false) {}

  static void initialize() {
    bind<INVISIBLE_OP::Predicate>(&Invisible::INVISIBLE);
    bind<POP_OP::Predicate>(&Invisible::POP);
    bind<Instruction::Predicate>(&Invisible::OTHER);
  }

  static const CodeBuffer &execute(CodeBuffer const &code) {
    Invisible inv;
    Pass::Sequential pass(code);
    pass.execute(inv);
    return pass.code();
  }

private:
  bool isInvis_;

  static const INVISIBLE_OP::Encoding INV_OP;

  void INVISIBLE(Driver &d, INVISIBLE_OP::Encoding const &ins) {
    // Set a flag indicating current instruction is INVISIBLE_OP.
    isInvis_ = true;
  }

  void POP(Driver &d, POP_OP::Encoding const &ins) {
    if (isInvis_) {
      // Remove preceding INVISIBLE_OP.
      d.eraseAt(d.pc() - INV_OP.size);
    }
    isInvis_ = false;
  }

  void OTHER(Driver &d, encoding::OpcodeOnly const &ins) {
    // Reset flag.
    isInvis_ = false;
  }

}; // Invisible

} // namespace reactor

#endif // INVISIBLE_H
