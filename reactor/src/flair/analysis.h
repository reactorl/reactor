#ifndef ANALYSES_H
#define ANALYSES_H

#include "scheduler.h"

namespace reactor {

class Analysis : public Scheduler::Pass {
public:
  class All;

  template <typename ANALYSIS> class SequentialPass;

  template <typename ANALYSIS, typename STATE, typename INTERPRETER>
  class Abstract;

  template <typename ANALYSIS> class ForwardControlFlow;
  template <typename ANALYSIS, typename STATE>
  using Forward = Abstract<ANALYSIS, STATE, ForwardControlFlow<ANALYSIS>>;

  template <typename ANALYSIS> class BackwardControlFlow;
  template <typename ANALYSIS, typename STATE>
  using Backward = Abstract<ANALYSIS, STATE, BackwardControlFlow<ANALYSIS>>;

  /** By default analysis does not depend on any other analyses.
   */
  void initializeScheduling() override { preserve(scheduler().all()); }

  /** Invalidates the analysis and informs the scheduler.

    TODO we might want to tap in the invalidation hook to remove resources, etc.
   */
  void invalidate() const {
    if (valid_) {
      valid_ = false;
      scheduler().invalidate(const_cast<Analysis *>(this));
    }
  }

  /** Returns whether the analysis is valid or not.
   */
  bool isValid() const { return valid_; }

  void schedule(size_t pc) { UNREACHABLE; };
  void scheduleBranch(size_t pc) { UNREACHABLE; };

protected:
  /** Creates the analysis

    The analysis is automatically added to the set of associated analyses in the
    given scheduler.
   */
  Analysis(Scheduler &scheduler) : Scheduler::Pass(scheduler), valid_(false) {
    scheduler.analyses_.insert(this);
  }

  CodeBuffer const &code() const { return scheduler().code(); }

  /** Sets the analysis as valid. Should be called at the end of invoke()
   * method.
   */
  void setAsValid() { valid_ = true; }

private:
  friend class Scheduler;
  /** Determines if the analysis is valid, or not.
   */
  mutable bool valid_;
};

class Analysis::All : public Analysis {
public:
  void invoke() override {
    // pass
  }

  /** Returns the name of the analysis for debugging purposes.
   */
  char const *name() const override { return "**All**"; }

protected:
  friend class Scheduler;
  All(Scheduler &scheduler) : Analysis(scheduler) {}
};

// --------------------------------------------------------------------------------------------------------------------

/** Base class for analysis that only requires one sequential pass over the
 * instructions.
 */
template <typename ANALYSIS> class Analysis::SequentialPass : public Analysis {
public:
  void invoke() override {
    ANALYSIS *analysis = reinterpret_cast<ANALYSIS *>(this);
    for (pc_ = 1; pc_ < code().code.size();
         pc_ += code().code.instructionSize(pc_))
      analysis->dispatch(scheduler(), code(), pc_);
    setAsValid();
  }

protected:
  SequentialPass(Scheduler &scheduler) : Analysis(scheduler) {}

  /** Returns pc of currently analyzed instruction.
   */
  size_t pc() const { return pc_; }

private:
  size_t pc_;
};

} // namespace reactor
#endif // ANALYSES_H
