#ifndef LOCAL_VARS_H
#define LOCAL_VARS_H

#include <set>
#include <map>

#include "reactor.h"
#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"
#include "flair/pass.h"

#include "formals.h"

namespace reactor {

/** Identifies all local variables of a given closure.

        Local variables are:

        - arguments to the closure taken from the formals
        - symbols the function writes to using either SETVAR for simple
   assignments or STARTASSIGN for more complex ones

        The function also identifies variables from parent environments the
   function will write to.

        TODO This is not enough of course as for proper analysis we need to know
   exactly which variable will be read by any of the statements.
*/
class LocalVariables
    : public ins::Dispatcher<LocalVariables, Pass::Sequential> {
public:
  class Result {
  public:
    size_t localsSize() const { return locals_.size(); }

    size_t supersSize() const { return supers_.size(); }

    SEXP local(size_t index) const { return locals_[index]; }

    SEXP super(size_t index) const { return supers_[index]; }

    std::vector<SEXP> const &locals() const { return locals_; }

    std::vector<SEXP> const &supers() const { return supers_; }

    void intersectLocalsWith(Result const &other) {
      // grossly inefficient
      std::vector<SEXP> x;
      for (SEXP i : locals_)
        for (SEXP j : other.locals_)
          if (i == j)
            x.push_back(i);
      locals_ = std::move(x);
    }

  private:
    friend class LocalVariables;

    Result(std::set<SEXP> const &locals, std::set<SEXP> const &supers) {
      locals_.resize(locals.size());
      supers_.resize(supers.size());
      auto li = locals_.begin();
      for (auto i = locals.begin(); i != locals.end(); ++i)
        *(li++) = *i;
      auto si = supers_.begin();
      for (auto i = supers.begin(); i != supers.end(); ++i)
        *(si++) = *i;
    }

    std::vector<SEXP> locals_;
    std::vector<SEXP> supers_;
  };

  static Result *execute(CodeBuffer const &code) {
    LocalVariables li(code);
    Pass::Sequential pass(code);
    pass.execute(li);
    return new Result(li.locals_, li.supers_); // caller has the ownership
  }

  static void initialize() {
    bind<SETVAR_OP::Predicate>(&LocalVariables::SETVAR);
    bind<SETVAR2_OP::Predicate>(&LocalVariables::SETVAR2);
    bind<ENDASSIGN_OP::Predicate>(&LocalVariables::ENDASSIGN);
    bind<ENDASSIGN2_OP::Predicate>(&LocalVariables::ENDASSIGN2);
    bind<STARTFOR_OP::Predicate>(&LocalVariables::STARTFOR_OP);
  }

private:
  LocalVariables(CodeBuffer const &code) {
    Formals f(code.closure);
    for (size_t i = 0; i < f.size(); ++i)
      locals_.insert(f.name(i));
  }

  void SETVAR(Driver &d, SETVAR_OP::Encoding const &ins) {
    locals_.insert(ins.constant1(d.code()));
  }

  void SETVAR2(Driver &d, SETVAR_OP::Encoding const &ins) {
    supers_.insert(ins.constant1(d.code()));
  }

  void ENDASSIGN(Driver &d, ENDASSIGN_OP::Encoding const &ins) {
    locals_.insert(ins.constant1(d.code()));
  }

  void ENDASSIGN2(Driver &d, ENDASSIGN2_OP::Encoding const &ins) {
    supers_.insert(ins.constant1(d.code()));
  }

  void STARTFOR_OP(Driver &d, STARTFOR_OP::Encoding const &ins) {
    locals_.insert(ins.constant2(d.code()));
  }

  std::set<SEXP> locals_;
  std::set<SEXP> supers_;

}; // LocalVariables

} // namespace flair

#endif // LOCAL_VARS_H
