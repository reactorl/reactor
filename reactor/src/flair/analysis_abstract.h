#ifndef ANALYSIS_ABSTRACT_H
#define ANALYSIS_ABSTRACT_H

#include "analysis.h"
#include "analysis/MergePoints.h"
#include "analysis/PcMap.h"

#include "reporting.h"

namespace reactor {

/** Abstract analysis base.

  Responsible for direction agnosting instruction scheduling and managment of
  abstract states. CONTROL_FLOW template parameter must implement instruction
  fetching, thus defining execution order.

  For state, I currently assume only that it can do out-place merge. i.e. it has
  method STATE * merge(STATE const * with) that returns null if the merge fails.

  Different to standard forward analysis, when two states converge at a merge
  point, the analysis tries to merge. If the merge fails, the analysis keeps
  both of the states and continues with them.

  TODO how are the analysis results going to be accessed when more than one
  values are tracked? Are we going to return state? States? Do we have to merge
  in the end anyways?

*/

template <typename ANALYSIS, typename STATE, typename CONTROL_FLOW>
class Analysis::Abstract : public Analysis {
public:
  /** By default forward analysis depends on mergePoints analysis while not
   * invalidating any.
   */
  void initializeScheduling() override {
    Analysis::initializeScheduling();
    require(scheduler().mergePoints());
    require(scheduler().pcMapper());
  }

  /** Performs the forward analysis of the code.

    Analyses the instructions in order until fixpoint is reached. At each merge
    point, the following happens during the merge:

   */
  // TODO this would be easier if I could use basic blocks - I can attach
  // payload to the basic blocks and skip the map lookup based on pc
  void invoke() override {
    ANALYSIS *analysis = reinterpret_cast<ANALYSIS *>(this);
    for (auto s : controlFlow_.start(scheduler().mergePoints()))
      schedule(s, this->initialState());

    while (not q_.empty()) {
      // get the pc and the state from the queue
      pc_ = q_.back().first;
      currentState_ = q_.back().second;
      assert_(currentState_->size() > 0);
      Debug(STATES, STR("@ " << pc() << " we have " << currentState_->size()
                             << " states "));
      q_.pop_back();
      // if we are at merge point, perform the merge and check whether we need
      // to continue
      if (scheduler().mergePoints().isMergePoint(pc_)) {
        auto i = mergePoints_.find(pc_);
        // we have't yet arrived to this mergepoint, copy incomming and continue
        if (i == mergePoints_.end()) {
          Debug(STATES, STR(pc() << ": creating new mergepoint"));
          mergePoints_[pc_] = new States(*currentState_);
        } else {
          Debug(STATES, STR(pc() << ": merging with existing "
                                 << i->second->size() << " states "));
          // otherwise merge the incomming state into the stored merge point
          i->second->mergeInto(currentState_);
          // check if we have reached a fixpoint and terminate the branch if so
          if (currentState_->size() == 0) {
            Debug(STATES,
                  STR(pc() << ": fixpoint reached, deleting current state"));
            delete currentState_;
            currentState_ = nullptr;
            continue;
          }
        }
      }

      assert_(currentState_->size() > 0);
      // dispatch on the current instruction on all tracked states
      for (idx_ = 0; idx_ < currentState_->size(); ++idx_) {
        analysis->dispatch(scheduler(), code(), pc_);
        // call state's check after method
        state().checkAfter(code().code.template get<encoding::OpcodeOnly>(pc_));
      }
      // now determine what will be the next instruction
      controlFlow_.scheduleNext(analysis);
    }
    delete currentState_;
    // set the current pc to first instruction and state to the initial state
    pc_ = 1;
    currentState_ = initialState();
    setAsValid();
  }

  /** Returns the number of states tracked for the given pc.
   */
  size_t trackedStates(size_t pc) {
    moveCurrentState(pc);
    return currentState_->size();
  }

  /** Returns the incomming state to the given pc.

    This is a shorthand function when only one state is being tracked and fails
    an assertion when more than one tracked state is available for the pc.
   */
  STATE const &state(size_t pc) const {
    moveCurrentState(pc);
    assert_true(
        currentState_->size() == 1,
        "Shorthand state access can only be used when single state is tracked");
    return *currentState_->operator[](0);
  }

  /** Returns the index-th incomming state to the given instruction.
   */
  STATE const &state(size_t pc, size_t stateIndex) const {
    moveCurrentState(pc);
    return *currentState_->operator[](stateIndex);
  }

protected:
  friend CONTROL_FLOW;

  /** List of current states.

    The analysis may store more than one abstract state at a time. This is the
    object which holds all the current abstract states.
    */
  class States {
  public:
    States() = default;

    States(States const &states) {
      for (STATE *s : states)
        states_.push_back(new STATE(*s));
    }

    explicit States(STATE *state) { states_.push_back(state); }

    ~States() {
      for (STATE *state : states_)
        delete state;
    }

    size_t size() const { return states_.size(); }

    typename std::vector<STATE *>::iterator begin() { return states_.begin(); }

    typename std::vector<STATE *>::iterator end() { return states_.end(); }

    typename std::vector<STATE *>::const_iterator begin() const {
      return states_.begin();
    }

    typename std::vector<STATE *>::const_iterator end() const {
      return states_.end();
    }

    STATE const *operator[](size_t index) const { return states_[index]; }

    STATE *operator[](size_t index) { return states_[index]; }

    // TODO I should methods for compacting the framestate

  protected:
    /** Adds the given state to the vector.
     */
    void add(STATE *state) { states_.push_back(state); }

    /** Merges the provided state into the States in place.

      Returns true if the States changed, i.e. if there is a need to continue
      the analysis from the current merge point.

      The following may happen when a state is merged with States.

      1) the state is included in any of the states already present, in which
      case nothing happens

      2) the state can be merged with any of the already existing states, in
      which case the resulting state replaces the original state in States. The
      state objects may decide whether the merge is possible, or not. The
      intuition is that the merge is successful if "not much" information is
      lost, how much exactly depends on the state's implementation.

      3) if no merge is possible, then the state has to be added to the current
      list of states.

      If the merger results in a change to any state in States, the new state
      that has to be analysed is returned. Nullptr is returned if the States did
      not change and no further analysis is required.

     */
    STATE *mergeInto(STATE *state) {
      // check if the new state is included in any of the already known states
      for (STATE *existing : states_)
        if (existing->includes(*state))
          return nullptr;

      // check if a merge can be performed
      for (size_t i = 0; i < states_.size(); ++i) {
        STATE *s = states_[i]->mergeWith(*state);
        if (s != nullptr) {
          // if merge with returns self, we can just keep it as the merge
          // happened in place
          if (s != states_[i]) {
            delete states_[i];
            states_[i] = s;
          }
          return s;
        }
      }
      // if merge cannot be performed either, add state and return it
      states_.push_back(new STATE(*state));
      return state;
    }

    /** Merges incomming states into the current States object.

      Each state from incomming is merged to States (either it is already
      included in States, or it is merged with one of the states, or it is added
      to them unchanged). The incomming states object is altered so that it
      contains new set of states for which the analysis must continue.

      If a fixpoint has been reached, all states from the incomming object are
      removed.
     */
    void mergeInto(States *incomming) {
      for (auto i = incomming->states_.begin(); i != incomming->states_.end();
           /* no i++ for erase */) {
        STATE *s = mergeInto(*i);

        // if merge returned nullptr, the state is no longer useful, we should
        // delete it and erase from the state
        if (s == nullptr) {
          delete *i;
          i = incomming->states_.erase(i);
          break;
        }

        // or if the state has been merged, we should delete it, and replace
        // with a copy of the merged state
        if (s != *i) {
          delete *i;
          *i = s;
        }

        // or a copy of the state has been added, we must keep the state for
        // analysis
        i++;
      }
    }

  private:
    friend class Analysis;

    std::vector<STATE *> states_;
  };

  /** Enum to specify whether a conditional jump is allowed to be taken (allow),
    must be taken (force), or cannot be taken (deny).

    This is used as a return value of the allowJump callback routine which
    enables the analysis to choose to not explore certain branches if it can be
    sure about the jump.
   */
  enum class JumpHint {
    allow,
    force,
    deny,
  };

  Abstract(Scheduler &scheduler) : Analysis(scheduler) {}

  /** Returns the pc of currently analyzed instruction.
   */
  size_t pc() const { return pc_; }

  /** Moves the current state to given pc.

   */
  virtual void moveCurrentState(size_t targetPc) const {
    if (pc_ != targetPc) {
      ANALYSIS *analysis =
          reinterpret_cast<ANALYSIS *>(const_cast<Abstract *>(this));

      // first find the closest mergepoint
      auto find =
          controlFlow_.reverseIterator(scheduler().pcMapper(), targetPc);
      while (true) {
        if (*find == pc_) {
          // if we back all the way to current state without finding a merge
          // point we can use the state we already have
          break;
        } else if (scheduler().mergePoints().isMergePoint(*find)) {
          // if merge point has been reached, make the copy of the merge point
          // the current state
          delete currentState_;
          pc_ = *find;
          currentState_ = new States(*mergePoints_.at(pc()));
          break;
        } else if (controlFlow_.isEntryPoint(scheduler().mergePoints(),
                                             *find)) {
          // if we reach first instruction, make current state equal to the
          // current state
          delete currentState_;
          pc_ = *find;
          currentState_ = initialState();
          break;
        }
        ++find;
      }

      // we have a state from which we can move to the required pc
      auto cur = controlFlow_.iterator(scheduler().pcMapper(), pc_, targetPc);

      while (!cur.done()) {
        assert_(scheduler().pcMapper().isValidPc(pc()));

        // dispatch the instruction on all tracked states
        for (idx_ = 0; idx_ < currentState_->size(); ++idx_)
          analysis->dispatch(scheduler(), code(), pc_);

        pc_ = *++cur;
      }
    }
  }

  /** Returns the initial state of the analysis.

    Should always return new object.
   */
  virtual States *initialState() const abstract;

  /** Returns whether the conditional jump which has just been analyzed should
    be taken or not.

    By default the jump is allowed to be taken, but not forced. Changing the
    behaviour of this method allows the analysis not to explore dead branches.
   */
  virtual JumpHint allowJump() const { return JumpHint::allow; }

  void schedule(size_t pc) { schedule(pc, currentState_); }

  void scheduleBranch(size_t pc, const std::vector<JumpHint> &jumpHints) {
    States *jump = new States();
    size_t i = 0;
    assert_(jumpHints.size() == currentState_->size());
    for (JumpHint hint : jumpHints) {
      switch (hint) {
      case JumpHint::allow:
        // if the jump is allowed, the copy of the state must be pushed to the
        // other branch
        jump->add(new STATE(*currentState_->operator[](i)));
        ++i;
        break;
      case JumpHint::force:
        jump->add(currentState_->operator[](i));
        currentState_->states_.erase(currentState_->states_.begin() + i);
        break;
      case JumpHint::deny:
        // no need to do anything, the state stays in non-jump branch
        break;
      }
    }
    schedule(pc, jump);
  }

  void schedule(size_t pc, States *state) {
    q_.push_back(std::pair<size_t, States *>(pc, state));
  }

  STATE &state() { return *currentState_->states_[idx_]; }

  /** Jump hints from the analysis to the driver.

   When the instruction analyzed is a conditional jump, for each tracked state,
   the analysis may provide a hint whether to take the jump or not. The hits are
   then used by the control flow dispatcher to properly schedule the state
   tracking.
   */
  const std::vector<JumpHint> jumpHints(size_t pc) {
    // dispatch on the current instruction on all tracked states
    if (Instruction::isConditionalJump(code().code.opcode(pc))) {
      // if the terminating instruction is a conditional jump, we must decide
      // for each state whether to take it or not
      return std::vector<JumpHint>(currentState_->size(), allowJump());
    }
    return std::vector<JumpHint>(0);
  }

private:
  /** Helper class implementing the control flow dispatch on the code.

    Specializes on terminating instructions and for each pushes the possible
    nexts instruction into the analysis queue.
   */

  // This can be static in theory - but it's not much waste having it as a
  // member
  CONTROL_FLOW controlFlow_;

  mutable size_t pc_;

  std::deque<std::pair<size_t, States *>> q_;
  std::map<size_t, States *> mergePoints_;

  /** Vector of currently tracked states.
   */
  mutable States *currentState_;

  /** Index to the currently tracked states so that each dispatcher method lives
   * under the abstraction that only one state is tracked at a time.
   */
  mutable size_t idx_;
};

} // namespace reactor

#endif // ANALYSIS_FORWARD_H
