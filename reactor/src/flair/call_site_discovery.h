#ifndef CALL_SITE_DISCOVERY_H
#define CALL_SITE_DISCOVERY_H

#include "bytecode/code_buffer.h"
#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"
#include "flair/pass.h"

namespace reactor {

/** Description of a callsite.

  Each callsite is defined by the addresses of the functions responsible for
  loading the function on the stack, handling the arguments and their names and
  the call itself. In the current implementation, it is assumed that each
  argument (defined by index) can be pushed only once. GNU-R compiler delivers
  such code. We might need to revisit this decission should we ever violate this
  rule in any of the optimizations.
  */

class Callsite2 {
public:
  /** Returns the address of the GETFUN_OP instruction which pushes the function
   * object on the stack and initializes the argument lists and other call
   * structures on the stack.
   */
  size_t getfun() const { return getfun_; }

  /** Returns the address of the CALL_OP instruction that performs the call.
   */
  size_t call() const { return call_; }

  /** Returns the number of arguments the function takes.
   */
  size_t arguments() const { return arguments_.size(); }

  /** Returns the address of the instruction responsible for storing the
   * index-th argument to the arguments list.
   */
  size_t argument(size_t index) const { return arguments_[index]; }

  /** Returns the address of the instruction setting name of index-th argument.

    Assumes that the index-th argument is a keyword argument. Unexpected
    behavior otherwise.
   */
  size_t name(size_t index) const { return names_[index]; }

  /** Checks whether index-th argument is a keyword argument or not.
   */
  bool isKeywordArgument(size_t index) const {
    return names_.size() > index and names_[index] != 0;
  }

  /** Returns name of the function called.
   */
  SEXP function(CodeBuffer const &code) const {
    return code.constants[code.code.get<GETFUN_OP::Encoding>(getfun_).arg1];
  }

  /** Returns the name of index-th argument.

    Returns nullptr if the argument is not a keyword one.
   */
  SEXP name(size_t index, CodeBuffer const &code) const {
    if (names_.size() <= index)
      return nullptr;
    index = names_[index];
    if (index == 0)
      return nullptr;
    return code.constants[code.code.get<SETTAG_OP::Encoding>(index).arg1];
  }

private:
  friend class CallSiteDiscoveryState_;

  Callsite2(size_t getfunPc) : getfun_(getfunPc), call_(0) {}

  size_t getfun_;
  size_t call_;
  std::vector<size_t> arguments_;
  std::vector<size_t> names_;
};

/** Internat state for the call site discovery analysis. Contains the stack of
  opened calls and the position in their arguments definition.

  Note that currently R compiler does not emit code with hierarchical calls, but
  this is extremely likely to change once user defined functions are inlined.
 */
// can't forward declare inner classes so its defined outside. Not expected to
// be used by anyone other than the CallSiteDisctovery class.
class CallSiteDiscoveryState_ {
private:
  friend class CallSiteDiscovery;
  friend class Pass::Forward<CallSiteDiscoveryState_>;

  class Item {
  public:
    Callsite2 *site;
    size_t argumentIndex;
    Item(size_t pc) : site(new Callsite2(pc)), argumentIndex(0) {}
  };

  CallSiteDiscoveryState_() = default;

  CallSiteDiscoveryState_(CallSiteDiscoveryState_ const &from)
      : state_(from.state_) {}

  Item &current() { return *state_.begin(); }

  void push(size_t getfunPc) { state_.push_front(Item(getfunPc)); }

  Callsite2 *popAndSetCallPc(size_t pc) {
    Callsite2 *result = state_.begin()->site;
    state_.pop_front();
    result->call_ = pc;
    return result;
  }

  void setArgument(size_t pc) {
    Item &c = current();
    assert_eq(c.site->arguments_.size(), c.argumentIndex,
              "Argument is being specified twice");
    c.site->arguments_.push_back(pc);
    ++c.argumentIndex;
  }

  void setName(size_t pc) {
    Item &c = current();
    assert_true(c.site->names_.size() <= c.argumentIndex - 1,
                "Argument's name is being specified twice");
    c.site->names_.resize(c.argumentIndex);
    c.site->names_[c.argumentIndex - 1] = pc;
  }

  std::deque<Item> state_;
};

/** Call site discovery.

  In a single pass over the code, detects all callsites and returns their list.

  Note that the analysis expects the code to be well formed - i.e. pushargs
  happen only at correct stack locations, etc. The only thing checked is that no
  argument is written twice.
 */
class CallSiteDiscovery
    : public ins::Dispatcher<CallSiteDiscovery,
                             Pass::ForwardSinglePass<CallSiteDiscoveryState_>>,
      public Pass::ForwardSinglePass<CallSiteDiscoveryState_> {
public:
  typedef CallSiteDiscoveryState_ State;

  /** A result container for all callsites identified by the analysis.

    The result is responsible for deleting the callsite info found by the
    analysis.
   */
  class Result {
  public:
    ~Result() {
      for (Callsite2 *s : sites_)
        delete s;
    }

    size_t size() const { return sites_.size(); }

    Callsite2 const &operator[](size_t index) const { return *sites_[index]; }

    Result() = default;
    Result(Result const &) = delete;
    Result(Result &&) = delete;
    Result &operator=(Result const &) = delete;
    Result &operator=(Result &&) = delete;

  private:
    friend class CallSiteDiscovery;

    std::vector<Callsite2 *> sites_;
  };

  /** Analyzes the given code and returns a list of all callsites identified.

    Note the caller is responsible for disposing of the result object.
   */
  static Result *analyze(CodeBuffer const &code) {
    CallSiteDiscovery csd(code);
    csd.execute<CallSiteDiscovery>(new State());
    return csd.result_;
  }

  static void initialize() {
    bind<GETFUN_OP::Predicate>(&CallSiteDiscovery::GETFUN);
    bind<MAKEPROM_OP::Predicate>(&CallSiteDiscovery::MAKEPROM);
    bind<PUSHARG_OP::Predicate>(&CallSiteDiscovery::PUSHARG);
    bind<PUSHCONSTARG_OP::Predicate>(&CallSiteDiscovery::PUSHCONSTARG);
    bind<PUSHTRUEARG_OP::Predicate>(&CallSiteDiscovery::PUSHTRUEARG);
    bind<PUSHFALSEARG_OP::Predicate>(&CallSiteDiscovery::PUSHFALSEARG);
    bind<PUSHNULLARG_OP::Predicate>(&CallSiteDiscovery::PUSHNULLARG);
    bind<SETTAG_OP::Predicate>(&CallSiteDiscovery::SETTAG);
    bind<CALL_OP::Predicate>(&CallSiteDiscovery::CALL);
  }

private:
  CallSiteDiscovery(CodeBuffer const &code)
      : Pass::ForwardSinglePass<State>(code), result_(new Result()) {}

  void GETFUN(Driver &pass, GETFUN_OP::Encoding const &ins) {
    state().push(pc());
  }

  void MAKEPROM(Driver &pass, MAKEPROM_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void PUSHARG(Driver &pass, PUSHARG_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void PUSHCONSTARG(Driver &pass, PUSHCONSTARG_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void PUSHTRUEARG(Driver &pass, PUSHTRUEARG_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void PUSHFALSEARG(Driver &pass, PUSHFALSEARG_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void PUSHNULLARG(Driver &pass, PUSHNULLARG_OP::Encoding const &ins) {
    state().setArgument(pc());
  }

  void SETTAG(Driver &pass, SETTAG_OP::Encoding const &ins) {
    state().setName(pc());
  }

  void CALL(Driver &pass, CALL_OP::Encoding const &ins) {
    result_->sites_.push_back(state().popAndSetCallPc(pc()));
  }

  Result *result_;
};

} // namespace flair

#endif // CALL_SITE_DISCOVERY_H
