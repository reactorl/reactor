#include <iostream>
#include <vector>

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "utils.h"
#include "printer.h"
#include "inliner.h"
#include "call_site_discovery.h"

using namespace std;
using namespace reactor;

REXPORT SEXP flair_test_staticInline(SEXP caller, SEXP callee) {
  CodeBuffer cb(caller);
  CallSiteDiscovery::Result *r = CallSiteDiscovery::analyze(cb);
  // cout << "--------------------------------------------------------------- "
  //     << endl;
  // Printer::execute(cb);
  // cout << "Found " << r->size() << " callsites. " << endl;
  StaticInliner::inlineCallsite(cb, (*r)[0], callee);
  // Printer::execute(cb);
  return cb;
}
