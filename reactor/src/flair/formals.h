#ifndef FORMALS_H
#define FORMALS_H

#include "r/list.h"

namespace reactor {

/** Description of the formal arguments for R closure.

        Stores the symbols for the arguments and their default values if any in
   C++
        semantics. This means that argument names are stored as symbols and
   default
        values are nullptr when not present.
 */
class Formals {
public:
  /** Creates the formals description from given R closure.
   */
  Formals(SEXP function) {
    assert_(TYPEOF(function) == CLOSXP);
    RList f(FORMALS(function));
    if (f.size() == 0)
      return;
    CharacterVector names(f.names());
    assert_(f.size() == names.size());
    for (size_t i = 0; i < names.size(); ++i) {
      SEXP argSym = Rf_install(CHAR(names[i]));
      nameIndices_[argSym] = names_.size();
      names_.push_back(argSym);
      SEXP value = f[i];
      if (isEmptySymbol(value))
        defaultValues_.push_back(nullptr);
      else
        defaultValues_.push_back(value);
    }
  }

  size_t size() const { return names_.size(); }

  SEXP name(size_t index) const { return names_[index]; }

  bool hasDefaultValue(size_t index) const {
    return defaultValues_[index] != nullptr;
  }

  SEXP defaultValue(size_t index) const { return defaultValues_[index]; }

  /** Returns the index of the argument with given name.

          Returns -1 if the argument with specified name is not found.
          */
  int getIndex(SEXP name) const {
    auto i = nameIndices_.find(name);
    if (i == nameIndices_.end())
      return -1;
    else
      return i->second;
  }

private:
  std::unordered_map<SEXP, size_t, std::hash<SEXP>, sexp_equal_to> nameIndices_;
  std::vector<SEXP> names_;
  std::vector<SEXP> defaultValues_;
};

} // namespace flair

#endif // FORMALS_H
