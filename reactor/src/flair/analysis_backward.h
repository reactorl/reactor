#ifndef ANALYSIS_BACKWARD_H
#define ANALYSIS_BACKWARD_H

#include "analysis_abstract.h"
#include "analysis/MergePoints.h"
#include "analysis/PcMap.h"

namespace reactor {

// TODO: this is probably not yet the most efficient way of iterating
// backwards...
template <typename ANALYSIS> class Analysis::BackwardControlFlow {
public:
  std::set<size_t> start(const analysis::MergePoints &mergePoints) const {
    return mergePoints.exit();
  }

  void scheduleNext(ANALYSIS *analysis) {
    if (analysis->pc() == 1)
      return;

    if (!analysis->scheduler().mergePoints().isMergePoint(analysis->pc())) {
      size_t d = *++analysis->scheduler().pcMapper().riterator(analysis->pc());
      assert_(!Instruction::isConditionalJump(
                  analysis->scheduler().code().code.opcode(d)));
      analysis->schedule(d);
      return;
    }

    for (auto d :
         analysis->scheduler().mergePoints().dominators(analysis->pc())) {
      // TODO: do something saner with the jump hints
      analysis->scheduleBranch(d, {ANALYSIS::JumpHint::allow});
    }
  }

  analysis::Cursor reverseIterator(const analysis::PcMapper &map,
                                   size_t pc) const {
    return map.iterator(pc);
  }
  analysis::RCursor iterator(const analysis::PcMapper &map, size_t pc,
                             size_t target) const {
    return map.riterator(pc, target);
  }

  bool isEntryPoint(const analysis::MergePoints &mergePoints, size_t pc) const {
    return start(mergePoints).count(pc) > 0;
  }
};

} // namespace reactor

#endif // ANALYSIS_BACKWARD_H
