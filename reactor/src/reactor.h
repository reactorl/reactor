#ifndef FLAIR_COMMON_H
#define FLAIR_COMMON_H

/* Enable debugging stuff.

  TODO this should become part of cmake at some point so that we distinguish
  between debug and release builds.
 */
#define DEBUG 1

#include <R.h>
#include <Rdefines.h>
#undef length

#include "assert.h"
#include "r/sexp.h"

#include <functional>

#define abstract = 0

#define REXPORT extern "C"

#if DEBUG == 1
#define DEBUG_ONLY /* */
#else
#define DEBUG_ONLY                                                             \
  {}
#endif

namespace reactor {

template <typename T> class Maybe {
protected:
  bool _exists;
  const T *_t;

  Maybe() : _exists(false) {}
  Maybe(bool exists, const T *t) : _exists(exists), _t(t) {}

public:
  inline bool on(std::function<bool(const T *)> fun,
                 std::function<bool()> else_) const {
    if (_exists)
      return fun(_t);
    return else_();
  }

  inline void on(std::function<void(const T *)> fun,
                 std::function<void()> else_) const {
    if (_exists) {
      fun(_t);
      return;
    }
    else_();
  }
};

template <typename T> class None : public Maybe<T> {
public:
  None(const None &other) = delete;
  None() : Maybe<T>() {}
};

template <typename T> class Some : public Maybe<T> {
public:
  Some(const Some &other) = delete;
  Some(const T *t) : Maybe<T>(true, t) {}
  Some(const T &t) : Maybe<T>(true, &t) {}
};

class Protector {
public:
  Protector(const Protector &other) = delete;
  Protector(){};

  SEXP operator()(SEXP value) {
    PROTECT(value);
    ++protectedValues_;
    return value;
  }

  ~Protector() { UNPROTECT(protectedValues_); }

private:
  /* Prevents heap allocation. */
  void *operator new(size_t);
  void *operator new[](size_t);
  void operator delete(void *);
  void operator delete[](void *);

  unsigned protectedValues_ = 0;
};

struct sexp_equal_to {
  bool operator()(const SEXP &a, const SEXP &b) const {
    return R_compute_identical(a, b, 0);
  }
};

// Constant false funtion
const static std::function<bool()> false_c = []() { return false; };

} // namespace reactor

#endif
