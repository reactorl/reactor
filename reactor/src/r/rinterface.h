#ifndef RINTERFACE_H
#define RINTERFACE_H

#include "reactor.h"

#include "sexp.h"
#include "vector.h"
#include "list.h"

#include <functional>

namespace reactor {

inline bool equal(SEXP first, SEXP second) {
  return first == second or R_compute_identical(first, second, 0);
}

inline Rboolean rbool(bool b) { return b ? Rboolean::TRUE : Rboolean::FALSE; }

} // namespace reactor

#endif // RINTERFACE_H
