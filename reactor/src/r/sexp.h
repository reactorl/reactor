#ifndef RAPTOR_SEXP_H
#define RAPTOR_SEXP_H

#define SEXP_TYPE_LIST(V)                                                      \
  V(NILSXP, Nil)                                                               \
  V(SYMSXP, Symbol)                                                            \
  V(LISTSXP, List)                                                             \
  V(CLOSXP, Closure)                                                           \
  V(ENVSXP, Environment)                                                       \
  V(PROMSXP, Promise)                                                          \
  V(LANGSXP, Call)                                                             \
  V(SPECIALSXP, Special)                                                       \
  V(BUILTINSXP, Builtin)                                                       \
  V(CHARSXP, Char)                                                             \
  V(LGLSXP, LogicalVector)                                                     \
  V(INTSXP, IntVector)                                                         \
  V(REALSXP, RealVector)                                                       \
  V(CPLXSXP, ComplexVector)                                                    \
  V(STRSXP, CharacterVector)                                                   \
  V(DOTSXP, Dot)                                                               \
  V(ANYSXP, Any)                                                               \
  V(VECSXP, GenericVector)                                                     \
  V(EXPRSXP, Expression)                                                       \
  V(BCODESXP, Bytecode)                                                        \
  V(EXTPTRSXP, ExpressionVector)                                               \
  V(WEAKREFSXP, WeakRef)                                                       \
  V(RAWSXP, Raw)                                                               \
  V(S4SXP, S4)                                                                 \
  V(FUNSXP, Fun)

namespace reactor {

enum class sexp_t : SEXPTYPE {
#define SEXP_TYPE_ENTY(stype, name) name = stype,
  SEXP_TYPE_LIST(SEXP_TYPE_ENTY)

  __LAST_SEXP_TYPE
};

template <sexp_t t> bool isType(SEXP s) {
  return TYPEOF(s) == static_cast<SEXPTYPE>(t);
}
}

#endif
