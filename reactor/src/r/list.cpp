#include "list.h"

namespace reactor {

RListIter RList::begin() const { return RListIter(e); }

RListIter RList::end() const { return RListIter(R_NilValue); }
}
