#ifndef RAPTOR_LIST_H
#define RAPTOR_LIST_H

#include "reactor.h"
#include "assert.h"

namespace reactor {

class RListIter;
class RList;

class RList {
  SEXP e;

public:
  RList(const SEXP e) : e(e) {
    assert_(TYPEOF(e) == LISTSXP || TYPEOF(e) == LANGSXP ||
            TYPEOF(e) == NILSXP);
  }

  void doAdvance() { e = next(); }

  size_t size() const { return Rf_length(e); }

  SEXP names() const { return Rf_getAttrib(e, R_NamesSymbol); }

  const SEXP operator[](int idx) const {
    SEXP i = e;
    while (idx-- > 0) {
      i = CDR(i);
    }
    return CAR(i);
  }

  bool empty() const { return e == R_NilValue; }

  bool last() const { return empty() || next() == R_NilValue; }

  bool unary() const { return !empty() && last(); }

  bool binary() const { return !empty() && advance().unary(); }

  const SEXP cons() const { return e; }

  const SEXP next() const { return CDR(e); }

  const RList advance() const { return RList(next()); }

  const SEXP first() const { return value(); }

  const SEXP second() const { return advance().value(); }

  const SEXP value() const { return CAR(e); }

  const SEXP tag() const { return TAG(e); }

  bool operator!=(const RList &other) const { return e != other.e; }

  RListIter begin() const;

  RListIter end() const;
};

class RListIter {
  RList list;

public:
  RListIter(const RList &list) : list(list) {}

  bool operator!=(const RListIter &other) const { return list != other.list; }

  const RList &operator*() const { return list; }

  const RList &operator++() {
    list.doAdvance();
    return list;
  }
};
}

#endif
