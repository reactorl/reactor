#ifndef REACTOR_R_VECTOR
#define REACTOR_R_VECTOR

#include "reactor.h"

#include <vector>
#include <array>
#include <initializer_list>

namespace reactor {

template <typename container_t, sexp_t t> class RVectorIter;

template <typename container_t_, sexp_t t> class RVector {
public:
  typedef container_t_ container_t;

  RVector(RVector &) = delete;

  RVector(const SEXP s) : s(s) { assert_(isType<t>(s)); }

  RVector(size_t length)
      : s(Rf_allocVector(static_cast<SEXPTYPE>(t), length)) {}

  template <size_t length>
  RVector(std::array<container_t, length> initValues)
      : RVector(length) {
    for (int i = 0; i < length; ++i)
      set(i, initValues[i]);
  }

  RVector(const std::vector<container_t> initValues)
      : RVector(initValues.size()) {
    for (int i = 0; i < initValues.size(); ++i)
      set(i, initValues[i]);
  }

  RVector(const std::initializer_list<container_t> initValues)
      : RVector(initValues.size()) {
    int i = 0;
    for (auto e : initValues)
      set(i++, e);
  }

  container_t operator[](size_t idx) const;

  void set(size_t idx, container_t v) const;

  RVectorIter<container_t, t> begin() const {
    return RVectorIter<container_t, t>(*this);
  }

  RVectorIter<container_t, t> end() const {
    return RVectorIter<container_t, t>(*this, size());
  }

  size_t size() const { return Rf_length(s); }

  operator SEXP() const { return s; }

protected:
  const SEXP s;
};

template <typename container_t, sexp_t t> class RVectorIter {
  const RVector<container_t, t> &v;
  size_t idx;

public:
  RVectorIter(const RVector<container_t, t> &v, size_t idx) : v(v), idx(idx) {}
  RVectorIter(const RVector<container_t, t> &v) : v(v), idx(0) {}

  bool operator!=(RVectorIter<container_t, t> &other) const {
    return v != other.v || idx != other.idx;
  }

  const container_t operator*() const { return v[idx]; }
  const RVectorIter &operator++() {
    idx++;
    return *this;
  }
};

typedef RVector<int, sexp_t::LogicalVector> LogicalVector;

template <> inline int LogicalVector::operator[](size_t idx) const {
  return LOGICAL(s)[idx];
}

template <> inline void LogicalVector::set(size_t idx, int v) const {
  LOGICAL(s)[idx] = v;
}

typedef RVector<SEXP, sexp_t::CharacterVector> CharacterVector;

template <> inline SEXP CharacterVector::operator[](size_t idx) const {
  return STRING_ELT(s, idx);
}

template <> inline void CharacterVector::set(size_t idx, SEXP v) const {
  SET_STRING_ELT(s, idx, v);
}

typedef RVector<int, sexp_t::IntVector> IntVector;

template <> inline int IntVector::operator[](size_t idx) const {
  return INTEGER(s)[idx];
}

template <> inline void IntVector::set(size_t idx, int v) const {
  INTEGER(s)[idx] = v;
}

typedef RVector<double, sexp_t::RealVector> RealVector;

template <> inline double RealVector::operator[](size_t idx) const {
  return REAL(s)[idx];
}

template <> inline void RealVector::set(size_t idx, double v) const {
  REAL(s)[idx] = v;
}

typedef RVector<SEXP, sexp_t::GenericVector> GenericVector;

template <> inline SEXP GenericVector::operator[](unsigned long idx) const {
  return VECTOR_ELT(s, idx);
}
template <> inline void GenericVector::set(size_t idx, SEXP v) const {
  SET_ELEMENT(s, idx, v);
}
}

#endif
