#include "symbols.h"

namespace reactor {

#define DEF_SYM_DEF(s, v) SEXP Symbol::s = Rf_install(v);
SYMBOL_LIST(DEF_SYM_DEF)

bool Symbol::loaded = false;

void Symbol::forceLoad() {
  if (loaded)
    return;

#define DEF_SYM_VAL(s, v) s = Rf_install(v);
  SYMBOL_LIST(DEF_SYM_VAL)

  loaded = true;
}
}
