#ifndef CLOSURE_H
#define CLOSURE_H

#include "r/list.h"
#include "bytecode/code_buffer.h"
#include "raptor/raptoR.h"

namespace reactor {

namespace r {

/** R's closure as seen from flair.

  */
class Closure {
public:
  /** Formal arguments of the closure with their names and default values.
   */
  class Formals {
  public:
    size_t size() const { return names_.size(); }

    SEXP name(size_t index) const { return names_[index]; }

    bool hasDefaultValue(size_t index) const {
      return defaultValues_[index] != nullptr;
    }

    SEXP defaultValue(size_t index) const { return defaultValues_[index]; }

  private:
    friend class Closure;

    Formals(SEXP function) {
      assert_(TYPEOF(function) == CLOSXP);
      RList f(FORMALS(function));
      if (f.size() == 0)
        return;
      CharacterVector names(f.names());
      assert_(f.size() == names.size());
      for (size_t i = 0; i < names.size(); ++i) {
        SEXP name = Rf_install(CHAR(names[i]));
        names_.push_back(name);
        SEXP value = f[i];
        if (isEmptySymbol(value))
          defaultValues_.push_back(nullptr);
        else
          defaultValues_.push_back(value);
      }
    }
    std::vector<SEXP> names_;
    std::vector<SEXP> defaultValues_;
  };

  Closure(SEXP function) : sexp(function) {
    assert_(TYPEOF(function) == CLOSXP);
  }

  ~Closure() {
    delete formals_;
    delete code_;
    delete scheduler_;
  }

  SEXP const sexp;

  /** Returns the formal arguments of the closure.
   */
  Formals const &formals() const {
    if (formals_ == nullptr)
      formals_ = new Formals(sexp);
    return *formals_;
  }

  CodeBuffer::Code const &code() const { return getBytecode().code; }

  CodeBuffer::Code &code() { return getBytecode().code; }

  CodeBuffer::Constants const &constants() const {
    return getBytecode().constants;
  }

  CodeBuffer::Constants &constants() { return getBytecode().constants; }

  /** Returns the scheduler associated with the closure.

    The scheduler is lazily created.
   */
  Scheduler &scheduler() {
    if (scheduler_ == nullptr)
      scheduler_ = new Scheduler(getBytecode());
    return *scheduler_;
  }

private:
  /** Obtains the bytecode of the closure if it has already been compiled,
     compiles lazily if not.

   */
  CodeBuffer &getBytecode() const {
    if (code_ == nullptr) {
      SEXP body = BODY(sexp);
      if (TYPEOF(body) == BCODESXP) {
        code_ = new CodeBuffer(sexp);
      } else {
        // the function has not been compiled yet, we must compile it to obtain
        code_ = new CodeBuffer(RCompilerInterface::function(sexp));
      }
    }
    return *code_;
  }

  mutable Formals *formals_ = nullptr;

  mutable CodeBuffer *code_ = nullptr;

  mutable Scheduler *scheduler_ = nullptr;
};

} // namespace r

} // namespace reactor

#endif // CLOSURE_H
