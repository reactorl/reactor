#ifndef RAPTOR_SYMBOLS_H
#define RAPTOR_SYMBOLS_H

#include "reactor.h"

namespace reactor {

#define SYMBOL_LIST(V)                                                         \
  V(dcoldcol, "::")                                                            \
  V(trippledcol, ":::")                                                        \
  V(value, "value")                                                            \
  V(stmp, "*tmp*")                                                             \
  V(svtmp, "*vtmp*")                                                           \
  V(switch_, "switch")                                                         \
  V(list, "list")                                                              \
  V(c, "c")                                                                    \
  V(dollar, "$")                                                               \
  V(dollarassign, "$<-")                                                       \
  V(is_char, "is.character")                                                   \
  V(is_compl, "is.complex")                                                    \
  V(is_double, "is.double")                                                    \
  V(is_int, "is.integer")                                                      \
  V(is_logic, "is.logical")                                                    \
  V(is_name, "is.name")                                                        \
  V(is_null, "is.null")                                                        \
  V(is_obj, "is.object")                                                       \
  V(is_sym, "is.symbol")                                                       \
  V(dot_call, ".Call")                                                         \
  V(seq_len, "seq_len")                                                        \
  V(seq_along, "seq_along")                                                    \
  V(dcol, ":")                                                                 \
  V(not_, "!")                                                                 \
  V(or_, "|")                                                                  \
  V(and_, "&")                                                                 \
  V(gt, ">")                                                                   \
  V(lt, "<")                                                                   \
  V(ge, ">=")                                                                  \
  V(le, "<=")                                                                  \
  V(ne, "!=")                                                                  \
  V(eq, "==")                                                                  \
  V(plus, "+")                                                                 \
  V(minus, "-")                                                                \
  V(times, "*")                                                                \
  V(div, "/")                                                                  \
  V(expt, "**")                                                                \
  V(carret, "^")                                                               \
  V(exp, "exp")                                                                \
  V(sqrt, "sqrt")                                                              \
  V(log, "log")                                                                \
  V(land_, "&&")                                                               \
  V(lor_, "||")                                                                \
  V(next, "next")                                                              \
  V(repeat, "repeat")                                                          \
  V(break_, "break")                                                           \
  V(block, "{")                                                                \
  V(function, "function")                                                      \
  V(bracket, "(")                                                              \
  V(return_, "return")                                                         \
  V(if_, "if")                                                                 \
  V(rassign, "<-")                                                             \
  V(assign, "=")                                                               \
  V(supAssign, "<<-")                                                          \
  V(for_, "for")                                                               \
  V(while_, "while")                                                           \
  V(subset, "[")                                                               \
  V(subset2, "[[")                                                             \
  V(subassign, "[<-")                                                          \
  V(subassign2, "[[<-")

class Symbol {
  static bool loaded;

public:
  static void forceLoad();

#define DEF_SYM_DEF(s, _) static SEXP s;
  SYMBOL_LIST(DEF_SYM_DEF)
#undef DEF_SYM_DEF
};
}

#endif
