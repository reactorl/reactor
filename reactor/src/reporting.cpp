#include "reporting.h"

#include <iostream>

namespace reactor {

size_t Debug::indentation = 0;
std::set<Debug::Type> Debug::enabled = {Debug::Type::SCHEDULING};

void log(std::string const &what) { std::cout << what; }

std::ostream &logStream() { return std::cout; }

void BaseMessage::report() const noexcept { std::cerr << *this; }

namespace exception {
namespace c_str {
#define V(name, format, ...)                                                   \
  extern const char name##_format[] = format;                                  \
  extern const char name[] = #name;
MESSAGE_LIST(V)
#undef V
} // namespace c_str

} // namespace exception

} // namespace reactor
