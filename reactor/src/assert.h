#ifndef FLAIR_ASSERT_H
#define FLAIR_ASSERT_H

#include "reporting.h"
#include "r/sexp.h"

namespace flair {

/** Simple assert when no message is required.
 */
#define assert_(WHAT)                                                          \
  if (not(WHAT)) {                                                             \
    ERROR(::reactor::exception::AssertFail(#WHAT));                            \
  }

#define assert_true(a, msg)                                                    \
  if (!(a)) {                                                                  \
    ERROR(::reactor::exception::AssertFail(msg));                              \
  }

#define assert_eq(a, b, msg)                                                   \
  if ((a) != (b)) {                                                            \
    ERROR(::reactor::exception::AssertFail(msg));                              \
  }

#define assert_neq(a, b, msg)                                                  \
  if ((a) == (b)) {                                                            \
    ERROR(::reactor::exception::AssertFail(msg));                              \
  }

#define assert_type(e, type)                                                   \
  if (!isType<type>(e)) {                                                      \
    ERROR(::reactor::exception::SexpTypeFail(                                  \
        Rf_type2char(TYPEOF(e)), Rf_type2char(static_cast<SEXPTYPE>(type))));  \
  }

#define UNREACHABLE ERROR(::reactor::exception::Unreachable(""))
#define NOT_IMPLEMENTED ERROR(::reactor::exception::NotImplemented(""))

} // namespace flair

#endif
