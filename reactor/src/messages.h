#ifndef MESSAGES_H
#define MESSAGES_H

#define MESSAGE_LIST(V)                                                        \
  V(IOError, "IO error occured when accessing file {0}", std::string)          \
  V(AssertFail, "Assertion failed: {0}", std::string)                          \
  V(SexpTypeFail, "Unexpected SEXP Type: got {0} expected {1}", std::string,   \
    std::string)                                                               \
  V(MatchError, "Match Case {0} was not handled at {1}:{2}", std::string,      \
    std::string, std::string)                                                  \
  V(Unreachable, "Attempt to execute unreachable code", std::string)           \
  V(NotImplemented, "Not implemented", std::string)                            \
  V(NotFound, "{0} not found", std::string)                                    \
  V(OOM, "OOM: we ran out of memory: {0}", std::string)                        \
  V(DeprecatedBC, "Deprecated Bytecode {0} ecountered", std::string)           \
  V(InvalidSExpr, "Ivalid Expression encountered: {0}", std::string)

#endif // MESSAGES_H
