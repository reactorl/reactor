#ifndef H_SEXP_MATCH
#define H_SEXP_MATCH

#include <sstream>

#include "reactor.h"

#include "macros.h"
#include "r/sexp.h"

namespace reactor {

class MatchStatement {
  const char *file;
  int line;

public:
  const SEXP subject;

  MatchStatement(const char *file, int line, SEXP subject)
      : file(file), line(line), subject(subject) {}
  ~MatchStatement() {}

  void fallThroughFail() {
    std::stringstream s;
    s << line;
    ERROR(exception::MatchError(Rf_type2char(TYPEOF(subject)), file, s.str()));
  }
};

#define MatchStatement(subject)                                                \
  MatchStatement __match_statement__(__FILE__, __LINE__, subject);             \
  SEXP _ __attribute__((unused)) = subject

#define __SwitchSEXP(e) switch (static_cast<sexp_t>(TYPEOF(e)))

#define Match(e)                                                               \
  {                                                                            \
    MatchStatement(e);                                                         \
    __SwitchSEXP(e) {                                                          \
    default:                                                                   \
      __match_statement__.fallThroughFail();                                   \
      break;

#define Match_(e)                                                              \
  {                                                                            \
    MatchStatement(e);                                                         \
    __SwitchSEXP(e) {

#define EndMatch                                                               \
  break;                                                                       \
  }                                                                            \
  }

#define Else() endCase default : {

#define endCase                                                                \
  break;                                                                       \
  }

#define Case(...) endCase __DISPATCH(Case, __VA_ARGS__)(__VA_ARGS__)

#define Case1(__name) case sexp_t::__name: {

#define Case2(__name, __car)                                                   \
  case sexp_t::__name: {                                                       \
    SEXP __car = CAR(__match_statement__.subject);

#define Case3(__name, __car, __cdr)                                            \
  case sexp_t::__name: {                                                       \
    SEXP __car __attribute__((unused)) = CAR(__match_statement__.subject);     \
    SEXP __cdr = CDR(__match_statement__.subject);

#define Case4(__name, __car, __cdr, __tag)                                     \
  case sexp_t::__name: {                                                       \
    SEXP __car __attribute__((unused)) = CAR(__match_statement__.subject);     \
    SEXP __cdr __attribute__((unused)) = CDR(__match_statement__.subject);     \
    SEXP __tag = TAG(__match_statement__.subject);

#define TrapBytecodeAndPromise()                                               \
  Case(Bytecode) {                                                             \
    ERROR(                                                                     \
        exception::InvalidSExpr("cannot compile byte code literals in code")); \
  }                                                                            \
  Case(Promise) {                                                              \
    ERROR(exception::InvalidSExpr("cannot compile promise literals in code")); \
  }
}

#endif
