#ifndef RAPTOR_INLINE_H
#define RAPTOR_INLINE_H

#include <functional>
#include <unordered_map>

#include "bytecode/opcodes.h"
#include "context.h"

namespace reactor {

class RLoopContextT;
class RList;

struct ptr_eq_hash {
  size_t operator()(const SEXP &s) const {
    return std::hash<uintptr_t>()(reinterpret_cast<uintptr_t>(s));
  }
};

class RCompiler;

typedef std::function<bool(RCompiler *cmp, Context::Any *c, const SEXP,
                           const SEXP, RList &)> inliner_t;

typedef const SEXP symbol_t;

class RInliner {

  std::unordered_map<symbol_t, inliner_t, ptr_eq_hash> inliner;

  template <Opcode op>
  static bool inline_dollar(
      RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
      const RList &args,
      std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs);
  template <Opcode op_binary, Opcode op_unary>
  static bool inline_arithmetic(RCompiler *cmp, Context::Any *c,
                                const SEXP call, const SEXP function,
                                const RList &args);
  template <Opcode op_binary, Opcode op_unary>
  static bool inline_arithmetic_special(RCompiler *cmp, Context::Any *c,
                                        const SEXP call, const SEXP function,
                                        const RList &args);
  template <Opcode op>
  static bool inline_unary(RCompiler *cmp, Context::Any *c, const SEXP call,
                           const SEXP function, const RList &args);
  template <Opcode op>
  static bool inline_unary_put(RCompiler *cmp, Context::Any *c, const SEXP call,
                               const SEXP function, const RList &args);
  template <Opcode op>
  static bool inline_binary_put(RCompiler *cmp, Context::Any *c,
                                const SEXP call, const SEXP function,
                                const RList &args);
  template <Opcode op1, Opcode op2>
  static bool inline_logical(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args);
  template <Opcode op, Opcode start_assignment, Opcode end_assignment>
  static bool inline_assign(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args);
  template <Opcode start_assignment, Opcode end_assignment>
  static bool inline_complex_assign(RCompiler *cmp, Context::Any *c,
                                    const SEXP target, const SEXP call,
                                    const SEXP function, const RList &args);
  static bool inline_getter_call(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args);
  static bool inline_setter_call(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args, const SEXP value);

  static bool inline_if(RCompiler *cmp, Context::Any *c, const SEXP call,
                        const SEXP function, const RList &args);
  static bool inline_bracket(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args);
  static bool inline_function(RCompiler *cmp, Context::Any *c, const SEXP call,
                              const SEXP function, const RList &args);
  static bool inline_block(RCompiler *cmp, Context::Any *c, const SEXP call,
                           const SEXP function, const RList &args);
  static void inline_repeat_body(RCompiler *cmp, Context::LoopT *c,
                                 const SEXP body);
  static bool inline_repeat(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args);
  static bool inline_while(RCompiler *cmp, Context::Any *c, const SEXP call,
                           const SEXP function, const RList &args);
  static void inline_while_body(RCompiler *cmp, Context::LoopT *c,
                                const SEXP call, const SEXP body,
                                const SEXP condition);
  static bool inline_for(RCompiler *cmp, Context::Any *c, const SEXP call,
                         const SEXP function, const RList &args);
  static void inline_for_body(RCompiler *cmp, Context::LoopT *c,
                              const SEXP body);
  static bool inline_return(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args);
  static bool inline_break(RCompiler *cmp, Context::Any *c, const SEXP call,
                           const SEXP function, const RList &args);
  static bool inline_next(RCompiler *cmp, Context::Any *c, const SEXP call,
                          const SEXP function, const RList &args);
  static bool inline_special(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args);
  static bool try_inline_setter(RCompiler *cmp, Context::Any *c,
                                const SEXP call, const SEXP function,
                                const RList &args);
  static bool try_inline_getter(
      RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
      const RList &args,
      std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs);
  static bool inline_setter(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args);
  static bool inline_getter(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args);
  template <Opcode start_op, Opcode vec_op, Opcode mat_op, Opcode n_op>
  static bool inline_sub_dispatch_(
      RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
      const RList &args,
      std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs);

  static bool startsNewContext(const SEXP funSymbol);
  static bool preservesContext(const SEXP funSymbol);
  static bool leakesContext(const SEXP funSymbol);
  static bool checkEscapingLoopCtx(const RList &args, bool topScope_);
  static bool checkEscapingLoopCtx(const RList &args);

  static const SEXP getAssignedVar(const SEXP lhs);
  static const SEXP getAssignFunName(RCompiler *cmp, Context::Any *c,
                                     const SEXP call, const SEXP function,
                                     const RList &args);
  static const SEXP getAssignFun(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args, const SEXP value);

public:
  RInliner();

  bool tryInlineCall(const SEXP call, const SEXP function, const SEXP args,
                     RCompiler *cmp, Context::Any *c) const;
};
}

#endif
