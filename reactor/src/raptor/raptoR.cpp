#include "raptoR.h"

using namespace reactor;

SEXP raptoR_compile(SEXP args) {
  return reactor::RCompilerInterface::expression(args);
}

SEXP raptoR_compileFunction(SEXP args) {
  return reactor::RCompilerInterface::function(args);
}
