#ifndef FIXUP_H
#define FIXUP_H

#include "bytecode/dispatcher.h"
#include "bytecode/rbc.h"

namespace reactor {

class Fixup : public ins::Dispatcher<Fixup, Fixup> {
public:
  typedef size_t Label;

  void apply(CodeBuffer &code) {
    for (size_t pc : jumps_)
      dispatch(*this, code, pc);
  }

  size_t addLabel() {
    labels_.push_back(0);
    return labels_.size() - 1;
  }

  size_t label(size_t index) const { return labels_[index]; }

  void setLabel(Label l, size_t pc) { labels_[l] = pc; }

  size_t add(size_t pc) {
    jumps_.push_back(pc);
    return pc;
  }

  static void initialize() {
    bind<GOTO_OP::Predicate>(&Fixup::jump);
    bind<BRIFNOT_OP::Predicate>(&Fixup::conditionalJump);
    bind<AND1ST_OP::Predicate>(&Fixup::conditionalJump);
    bind<OR1ST_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBASSIGN_N_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBASSIGN2_N_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBSET_N_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBSET2_N_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBSET_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTSUBSET2_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTC_OP::Predicate>(&Fixup::conditionalJump);
    bind<STARTFOR_OP::Predicate>(&Fixup::startFor);
    bind<STEPFOR_OP::Predicate>(&Fixup::jump);
  }

  void jump(encoding::R1 const &ins) {
    const_cast<encoding::R1 &>(ins).arg1 = fixup_(ins.arg1);
  }

  void conditionalJump(encoding::R2 const &ins) {
    const_cast<encoding::R2 &>(ins).arg2 = fixup_(ins.arg2);
  }

  void startFor(encoding::R3 const &ins) {
    const_cast<encoding::R3 &>(ins).arg3 = fixup_(ins.arg3);
  }

private:
  void jump(Driver &d, encoding::R1 const &ins) { jump(ins); }

  void conditionalJump(Driver &d, encoding::R2 const &ins) {
    conditionalJump(ins);
  }

  void startFor(Driver &d, encoding::R3 const &ins) { startFor(ins); }

  size_t fixup_(size_t old) { return labels_[old]; }

  std::vector<size_t> labels_;
  std::vector<size_t> jumps_;
};

} // namespace flair

#endif // FIXUP_H
