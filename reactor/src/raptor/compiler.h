#ifndef RAPTOR_COMPILER_H
#define RAPTOR_COMPILER_H

#include <stack>

#include "inline.h"

#include "context.h"
#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "fixup.h"

namespace reactor {

class RCompilerHelper {
public:
  static bool missingDotsTagged(const RList &list);
  static bool missingDotsTagged1(const RList &list);
  static int ddVal(SEXP symbol);
};

class RCompiler {
protected:
  enum class ConstantUsage { PUSH, LOAD };

  Context::Any *c;

  static const RInliner inliner;

  void compileMakeProm(const SEXP e);
  void compileTag(const SEXP tag);
  void compileConstant(const SEXP e, ConstantUsage u);
  bool tryInlineCall(const SEXP call, const SEXP function, const SEXP args);
  void compileCall(const SEXP call, const SEXP function, const SEXP args);
  void compileCallExpression(const SEXP call, const SEXP function,
                             const SEXP args);
  void compileCallSymbol(const SEXP call, const SEXP function, const SEXP args);
  void compileCallCall(const SEXP call, const SEXP args);
  void compileSymbol(const SEXP e);

public:
  RCompiler(Context::Any *c) : c(c) {}

  const SEXP doProtect(const SEXP e);

  void compile(const SEXP e);

  void compileExpression(const SEXP e);
  void compileFunction(const SEXP e);

  void compileCallArg(const SEXP e, const SEXP tag);

  SEXP assemble();
};
}

#endif
