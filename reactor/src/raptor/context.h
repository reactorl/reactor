#ifndef RAPTOR_CONTEXT_H
#define RAPTOR_CONTEXT_H

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "fixup.h"

namespace reactor {

namespace Context {

/* LoopInfo
 * Provides Loop jump labels
 */

class LoopInfo {
public:
  LoopInfo(const LoopInfo &) = delete;

  LoopInfo(Fixup::Label breakTarget, Fixup::Label nextTarget)
      : end(breakTarget), next(nextTarget) {}

  const Fixup::Label end;
  const Fixup::Label next;
};

/* ContextData
 * Stores per Context Data
 */
class ContextData : public ins::Dispatcher<ContextData, ContextData> {
  Protector protect;
  CodeBuffer code;
  Fixup fixup;

  void jump(Driver &d, encoding::R1 const &ins) {
    if (fixup.label(ins.arg1) == 0)
      fixup.add(code.code.size() - GOTO_OP::_.size());
    else
      fixup.jump(ins);
  }

  void conditionalJump(Driver &d, encoding::R2 const &ins) {
    if (fixup.label(ins.arg2) == 0)
      fixup.add(code.code.size() - BRIFNOT_OP::_.size());
    else
      fixup.conditionalJump(ins);
  }

  void startFor(Driver &d, encoding::R3 const &ins) {
    if (fixup.label(ins.arg3) == 0)
      fixup.add(code.code.size() - STARTFOR_OP::_.size());
    else
      fixup.startFor(ins);
  }

public:
  ContextData() = default;
  ContextData(const ContextData &) = delete;

  RArgRepr put(const SEXP e) { return code.constants.add(e); }
  RArgRepr putProtect(const SEXP e) {
    protect(e);
    return code.constants.add(e);
  }
  SEXP doProtect(const SEXP e) {
    protect(e);
    return e;
  }

  template <typename INS_ENCODING> void add(INS_ENCODING const &ins) {
    size_t pc = code.code.size();
    code.code.append(ins);
    dispatch(*this, code, pc);
  }

  Fixup::Label createLabel() { return fixup.addLabel(); }

  void putLabel(Fixup::Label l) { fixup.setLabel(l, code.code.size()); }

  const SEXP assemble() {
    // TODO(o) find out how to iterate the instructions backwards to find out
    //         if it terminates by RETURN
    add(RETURN_OP::create());
    fixup.apply(code);
    return code;
  }

  static void initialize() {
    bind<GOTO_OP::Predicate>(&ContextData::jump);
    bind<BRIFNOT_OP::Predicate>(&ContextData::conditionalJump);
    bind<AND1ST_OP::Predicate>(&ContextData::conditionalJump);
    bind<OR1ST_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBSET_N_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBSET2_N_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBASSIGN_N_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBASSIGN2_N_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBSET_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTSUBSET2_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTC_OP::Predicate>(&ContextData::conditionalJump);
    bind<STARTFOR_OP::Predicate>(&ContextData::startFor);
    bind<STEPFOR_OP::Predicate>(&ContextData::jump);
  }
};

/* Context::Any
 * Common Supertype of all contexts
 */
class Any {
public:
  Any(const Any &) = delete;
  Any() {}
  ~Any() {}

  virtual RArgRepr put(const SEXP e) abstract;
  virtual RArgRepr putProtect(const SEXP e) abstract;
  virtual SEXP doProtect(const SEXP e) abstract;

  virtual void add(encoding::R0 const &ins) abstract;
  virtual void add(encoding::R1 const &ins) abstract;
  virtual void add(encoding::R2 const &ins) abstract;
  virtual void add(encoding::R3 const &ins) abstract;
  virtual void add(encoding::R4 const &ins) abstract;

  virtual Fixup::Label createLabel() abstract;
  virtual void putLabel(Fixup::Label l) abstract;

  virtual bool nonLocalReturn() const { return false; }

  virtual const Maybe<LoopInfo> loopInfo() const { return None<LoopInfo>(); }

  void addReturn() {
    if (nonLocalReturn()) {
      add(RETURNJMP_OP::create());
    } else {
      add(RETURN_OP::create());
    }
  }

  virtual const SEXP assemble() abstract;
};

/* Context
 * actual Context implementation, template parameters defining Supertype and
 * Storage type.
 */
template <typename StorageType, typename BaseClass>
class Context : public BaseClass {
  StorageType data;

public:
  Context() : data() {}
  Context(StorageType d) : data(d) {}

  RArgRepr put(const SEXP e) { return data.put(e); }
  RArgRepr putProtect(const SEXP e) { return data.putProtect(e); }
  SEXP doProtect(const SEXP e) { return data.doProtect(e); }

  void add(encoding::R0 const &ins) final { data.add(ins); }
  void add(encoding::R1 const &ins) final { data.add(ins); }
  void add(encoding::R2 const &ins) final { data.add(ins); }
  void add(encoding::R3 const &ins) final { data.add(ins); }
  void add(encoding::R4 const &ins) final { data.add(ins); }

  Fixup::Label createLabel() final { return data.createLabel(); }

  void putLabel(Fixup::Label l) final { data.putLabel(l); }

  const SEXP assemble() override { return data.assemble(); }
};

/* Scoped
 * Context with an outer Scope
 */
template <typename Data, typename Base>
class Scoped : public Context<Data, Base> {
  Any *parent;

public:
  Scoped(Any *c) : Context<Data, Base>(), parent(c) {}
  Scoped(Any *c, Data d) : Context<Data, Base>(d), parent(c) {}

  virtual bool nonLocalReturn() const override {
    return parent->nonLocalReturn();
  }
};

/* The different Context implementations */

class Root : public Context<ContextData, Any> {
public:
  Root() : Context<ContextData, Any>(){};
};

class Promise : public Scoped<ContextData, Any> {
public:
  Promise(Any *c) : Scoped<ContextData, Any>(c) {}

  virtual bool nonLocalReturn() const final { return true; }
};

/* LoopT
 * Supertype of Loop contexts
 */

class LoopT : public Any {
public:
  virtual const Maybe<LoopInfo> loopInfo() const final {
    return Some<LoopInfo>(getLoopInfo());
  }

  virtual const LoopInfo &getLoopInfo() const abstract;
};

/* Loop
 * Normal Loop Context
 */
class Loop : public Scoped<ContextData, LoopT> {
  LoopInfo loopInfo_;

public:
  Loop(Any *c)
      : Scoped<ContextData, LoopT>(c),
        loopInfo_(this->createLabel(), this->createLabel()) {}

  virtual const LoopInfo &getLoopInfo() const final { return loopInfo_; }

  virtual bool nonLocalReturn() const final { return true; }
};

/* InlineLoop
 * Proxies outer Context
 */
class InlineLoop : public Scoped<Any &, LoopT> {
  LoopInfo loopInfo_;

public:
  InlineLoop(Any *c)
      : Scoped<Any &, LoopT>(c, *c),
        loopInfo_(c->createLabel(), c->createLabel()) {}

  virtual const LoopInfo &getLoopInfo() const final { return loopInfo_; }
};

} // namespace Context

} // namespace reactor

#endif
