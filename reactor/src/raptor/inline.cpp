#include "inline.h"

#include "compiler.h"

#include "context.h"
#include "r/list.h"
#include "r/symbols.h"
#include "sexpMatch.h"
#include "assert.h"

namespace reactor {

RInliner::RInliner() {
  Symbol::forceLoad();
  assert_true(Symbol::block, "Symbols not yet initialized");

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::dollar, &RInliner::inline_getter));

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::block, &RInliner::inline_block));
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::function,
                                                &RInliner::inline_function));
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::bracket,
                                                &RInliner::inline_bracket));

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::if_, &RInliner::inline_if));

  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::assign,
      &RInliner::inline_assign<Opcode::SETVAR_OP, Opcode::STARTASSIGN_OP,
                               Opcode::ENDASSIGN_OP>));
  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::rassign,
      &RInliner::inline_assign<Opcode::SETVAR_OP, Opcode::STARTASSIGN_OP,
                               Opcode::ENDASSIGN_OP>));
  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::supAssign,
      &RInliner::inline_assign<Opcode::SETVAR2_OP, Opcode::STARTASSIGN2_OP,
                               Opcode::ENDASSIGN2_OP>));

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::subset, &RInliner::inline_getter));
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::subset2,
                                                &RInliner::inline_getter));
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::subassign,
                                                &RInliner::inline_setter));
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::subassign2,
                                                &RInliner::inline_setter));

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::while_, &RInliner::inline_while));
  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::repeat, &RInliner::inline_repeat));
  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::for_, &RInliner::inline_for));

  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::break_, &RInliner::inline_break));
  inliner.insert(
      std::pair<symbol_t, inliner_t>(Symbol::next, &RInliner::inline_next));

  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::return_,
                                                &RInliner::inline_return));

  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::land_,
      &RInliner::inline_logical<Opcode::AND1ST_OP, Opcode::AND2ND_OP>));
  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::lor_,
      &RInliner::inline_logical<Opcode::OR1ST_OP, Opcode::OR2ND_OP>));

  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::plus,
      &RInliner::inline_arithmetic<Opcode::ADD_OP, Opcode::UPLUS_OP>));
  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::minus,
      &RInliner::inline_arithmetic<Opcode::SUB_OP, Opcode::UMINUS_OP>));

  inliner.insert(std::pair<symbol_t, inliner_t>(
      Symbol::log, &RInliner::inline_arithmetic_special<Opcode::LOGBASE_OP,
                                                        Opcode::LOG_OP>));

#define UNARY_OP_LIST(V)                                                       \
  V(is_char, ISCHARACTER_OP)                                                   \
  V(is_compl, ISCOMPLEX_OP)                                                    \
  V(is_double, ISDOUBLE_OP)                                                    \
  V(is_int, ISINTEGER_OP)                                                      \
  V(is_logic, ISLOGICAL_OP)                                                    \
  V(is_name, ISSYMBOL_OP)                                                      \
  V(is_null, ISNULL_OP)                                                        \
  V(is_obj, ISOBJECT_OP)                                                       \
  V(is_sym, ISSYMBOL_OP)

#define INIT_UNARY_OP(symbol, opcode)                                          \
  inliner.insert(std::pair<symbol_t, inliner_t>(                               \
      Symbol::symbol, &RInliner::inline_unary<Opcode::opcode>));

  UNARY_OP_LIST(INIT_UNARY_OP)

#define UNARY_PUT_OP_LIST(V)                                                   \
  V(exp, EXP_OP)                                                               \
  V(sqrt, SQRT_OP)                                                             \
  V(not_, NOT_OP)                                                              \
  V(seq_along, SEQALONG_OP)                                                    \
  V(seq_len, SEQLEN_OP)

#define INIT_UNARY_PUT_OP(symbol, opcode)                                      \
  inliner.insert(std::pair<symbol_t, inliner_t>(                               \
      Symbol::symbol, &RInliner::inline_unary_put<Opcode::opcode>));

  UNARY_PUT_OP_LIST(INIT_UNARY_PUT_OP)

#define BINARY_PUT_OP_LIST(V)                                                  \
  V(times, MUL_OP)                                                             \
  V(div, DIV_OP)                                                               \
  V(carret, EXPT_OP)                                                           \
  V(eq, EQ_OP)                                                                 \
  V(ne, NE_OP)                                                                 \
  V(lt, LT_OP)                                                                 \
  V(gt, GT_OP)                                                                 \
  V(le, LE_OP)                                                                 \
  V(ge, GE_OP)                                                                 \
  V(and_, AND_OP)                                                              \
  V(or_, OR_OP)                                                                \
  V(dcol, COLON_OP)

#define INIT_BINARY_PUT_OP(symbol, opcode)                                     \
  inliner.insert(std::pair<symbol_t, inliner_t>(                               \
      Symbol::symbol, &RInliner::inline_binary_put<Opcode::opcode>));

  BINARY_PUT_OP_LIST(INIT_BINARY_PUT_OP)

#define SPECIAL_OP_LIST(V) V(switch_)

#define INIT_SPECIAL_OP(symbol)                                                \
  inliner.insert(std::pair<symbol_t, inliner_t>(Symbol::symbol,                \
                                                &RInliner::inline_special));

  SPECIAL_OP_LIST(INIT_SPECIAL_OP)

  inliner.max_load_factor(2);
}

template <Opcode op>
bool RInliner::inline_unary(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args) {
  if (!args.unary())
    return false;
  if (RCompilerHelper::missingDotsTagged(args))
    return false;
  cmp->compile(args.first());
  c->add(encoding::R0(op));
  return true;
}

template <Opcode op>
bool RInliner::inline_unary_put(RCompiler *cmp, Context::Any *c,
                                const SEXP call, const SEXP function,
                                const RList &args) {

  if (!args.unary())
    return false;
  if (RCompilerHelper::missingDotsTagged(args))
    return false;

  cmp->compile(args.first());
  c->add(encoding::R1(op, c->put(call)));
  return true;
}

template <Opcode op>
bool RInliner::inline_binary_put(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args) {

  if (!args.binary())
    return false;
  if (RCompilerHelper::missingDotsTagged(args))
    return false;

  cmp->compile(args.first());
  cmp->compile(args.second());
  c->add(encoding::R1(op, c->put(call)));
  return true;
}

template <Opcode op_binary, Opcode op_unary>
bool RInliner::inline_arithmetic(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args) {
  if (args.unary())
    return RInliner::inline_unary_put<op_unary>(cmp, c, call, function, args);
  if (args.binary())
    return RInliner::inline_binary_put<op_binary>(cmp, c, call, function, args);
  return false;
}

template <Opcode op_binary, Opcode op_unary>
bool RInliner::inline_arithmetic_special(RCompiler *cmp, Context::Any *c,
                                         const SEXP call, const SEXP function,
                                         const RList &args) {
  if (args.unary())
    return RInliner::inline_unary_put<op_unary>(cmp, c, call, function, args);
  if (args.binary())
    return RInliner::inline_binary_put<op_binary>(cmp, c, call, function, args);
  return inline_special(cmp, c, call, function, args);
}

template <Opcode op1, Opcode op2>
bool RInliner::inline_logical(RCompiler *cmp, Context::Any *c, const SEXP call,
                              const SEXP function, const RList &args) {
  assert_(function == Symbol::land_ || function == Symbol::lor_);

  if (!args.binary())
    return false;

  Fixup::Label skip = c->createLabel();
  RArgRepr i = c->put(call);
  cmp->compile(args[0]);
  c->add(encoding::R2(op1, i, skip));
  cmp->compile(args[1]);
  c->add(encoding::R1(op2, i));
  c->putLabel(skip);
  return true;
}

bool RInliner::inline_getter(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args) {
  auto cmpS = [](RCompiler *cmp, Context::Any *c, SEXP s) { cmp->compile(s); };
  if (try_inline_getter(cmp, c, call, function, args, cmpS))
    return true;
  return inline_special(cmp, c, call, function, args);
}

bool RInliner::inline_setter(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args) {
  if (try_inline_setter(cmp, c, call, function, args))
    return true;
  return inline_special(cmp, c, call, function, args);
}

bool RInliner::try_inline_setter(RCompiler *cmp, Context::Any *c,
                                 const SEXP call, const SEXP function,
                                 const RList &args) {
  auto setupLhs = [](RCompiler *, Context::Any *, SEXP) {};
  if (function == Symbol::subassign) {
    return inline_sub_dispatch_<
        Opcode::STARTSUBASSIGN_N_OP, Opcode::VECSUBASSIGN_OP,
        Opcode::MATSUBASSIGN_OP, Opcode::SUBASSIGN_N_OP>(cmp, c, call, function,
                                                         args, setupLhs);
  }
  if (function == Symbol::subassign2) {
    return inline_sub_dispatch_<
        Opcode::STARTSUBASSIGN2_N_OP, Opcode::VECSUBASSIGN2_OP,
        Opcode::MATSUBASSIGN2_OP, Opcode::SUBASSIGN2_N_OP>(
        cmp, c, call, function, args, setupLhs);
  }
  if (function == Symbol::dollarassign) {
    return inline_dollar<Opcode::DOLLARGETS_OP>(cmp, c, call, function, args,
                                                setupLhs);
  }
  return false;
}

bool RInliner::try_inline_getter(
    RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
    const RList &args,
    std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs) {
  if (function == Symbol::subset) {
    return inline_sub_dispatch_<Opcode::STARTSUBSET_N_OP, Opcode::VECSUBSET_OP,
                                Opcode::MATSUBSET_OP, Opcode::SUBSET_N_OP>(
        cmp, c, call, function, args, setupLhs);
  }
  if (function == Symbol::subset2) {
    return inline_sub_dispatch_<
        Opcode::STARTSUBSET2_N_OP, Opcode::VECSUBSET2_OP, Opcode::MATSUBSET2_OP,
        Opcode::SUBSET2_N_OP>(cmp, c, call, function, args, setupLhs);
  }
  if (function == Symbol::dollar) {
    return inline_dollar<Opcode::DOLLAR_OP>(cmp, c, call, function, args,
                                            setupLhs);
  }
  return false;
}

template <Opcode start_op, Opcode vec_op, Opcode mat_op, Opcode n_op>
bool RInliner::inline_sub_dispatch_(
    RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
    const RList &args,
    std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs) {
  assert_(function == Symbol::subset || function == Symbol::subset2 ||
          function == Symbol::subassign || function == Symbol::subassign2);

  if (!args.empty() && !RCompilerHelper::missingDotsTagged(args)) {
    SEXP store = args[0];
    RList idx = args.advance();

    if (!idx.empty()) {
      Fixup::Label endLabel = c->createLabel();
      RArgRepr i = c->put(call);
      {
        setupLhs(cmp, c, store);
        c->add(encoding::R2(start_op, i, endLabel));
      }

      int nIdx = 0;
      {
        for (auto i : idx) {
          nIdx++;
          cmp->compile(i.value());
        }
      }

      {
        switch (nIdx) {
        case 1:
          c->add(encoding::R1(vec_op, i));
          break;
        case 2:
          c->add(encoding::R1(mat_op, i));
          break;
        default:
          c->add(encoding::R2(n_op, i, nIdx));
          break;
        }
        c->putLabel(endLabel);
      }
      return true;
    }
  }
  return false;
}

bool RInliner::inline_break(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args) {
  c->loopInfo().on(
      [c](const Context::LoopInfo *loopInfo) {
        c->add(GOTO_OP::create(loopInfo->end));
      },
      [c, call]() { c->add(CALLSPECIAL_OP::create(c->put(call))); });
  return true;
}

bool RInliner::inline_next(RCompiler *cmp, Context::Any *c, const SEXP call,
                           const SEXP function, const RList &args) {
  c->loopInfo().on(
      [c](const Context::LoopInfo *loopInfo) {
        c->add(GOTO_OP::create(loopInfo->next));
      },
      [c, call]() { c->add(CALLSPECIAL_OP::create(c->put(call))); });
  return true;
}

bool RInliner::inline_special(RCompiler *cmp, Context::Any *c, const SEXP call,
                              const SEXP function, const RList &args) {
  RArgRepr i = c->put(call);
  c->add(CALLSPECIAL_OP::create(i));
  return true;
}

bool RInliner::inline_return(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args) {
  assert_eq(function, Symbol::return_, "Expected 'return'");

  SEXP val;
  if (args.empty()) {
    val = R_NilValue;
  } else if (args.unary()) {
    if (RCompilerHelper::missingDotsTagged1(args))
      return inline_special(cmp, c, call, function, args);
    val = args.value();
  } else {
    return inline_special(cmp, c, call, function, args);
  }

  cmp->compile(val);
  c->addReturn();
  return true;
}

bool RInliner::startsNewContext(const SEXP funSymbol) {
  return isType<sexp_t::Symbol>(funSymbol) &&
         (funSymbol == Symbol::function || funSymbol == Symbol::for_ ||
          funSymbol == Symbol::while_ || funSymbol == Symbol::repeat);
}

bool RInliner::preservesContext(const SEXP funSymbol) {
  return isType<sexp_t::Symbol>(funSymbol) &&
         (funSymbol == Symbol::bracket || funSymbol == Symbol::block ||
          funSymbol == Symbol::if_);
}

bool RInliner::leakesContext(const SEXP funSymbol) {
  return isType<sexp_t::Symbol>(funSymbol) &&
         (funSymbol == Symbol::break_ || funSymbol == Symbol::next);
}

bool RInliner::checkEscapingLoopCtx(const RList &args) {
  return checkEscapingLoopCtx(args, true);
}

bool RInliner::checkEscapingLoopCtx(const RList &args, bool topScope) {
  for (auto st : args) {
    Match_(st.value()) {
      Case(Call, fun, args) {
        if (startsNewContext(fun)) {
          return false;
        }

        if (leakesContext(fun)) {
          return !topScope;
        }

        if (checkEscapingLoopCtx(RList(args),
                                 topScope && preservesContext(fun))) {
          return true;
        }
      }
      Else() { continue; }
    }
    EndMatch
  }

  return false;
}

void RInliner::inline_for_body(RCompiler *cmp, Context::LoopT *c,
                               const SEXP body) {

  Fixup::Label loopHead = c->createLabel();
  const Context::LoopInfo &loop = c->getLoopInfo();

  c->putLabel(loopHead);
  cmp->compile(body);
  c->add(POP_OP::create());

  c->putLabel(loop.next);
  c->add(STEPFOR_OP::create(loopHead));

  c->putLabel(loop.end);
}

bool RInliner::inline_for(RCompiler *cmp, Context::Any *c, const SEXP call,
                          const SEXP function, const RList &args) {
  assert_eq(function, Symbol::for_, "Expected 'for'");

  SEXP it = args[0];
  SEXP seq = args[1];
  SEXP body = args[2];

  cmp->compile(seq);
  RArgRepr i = c->put(it);
  RArgRepr callI = c->put(call);

  if (checkEscapingLoopCtx(args)) {

    Fixup::Label skip = c->createLabel();
    c->add(STARTFOR_OP::create(callI, i, skip));
    c->putLabel(skip);

    SEXP cbody;
    {
      Context::Loop lc(c);
      RCompiler cmp(&lc);

      lc.add(GOTO_OP::create(lc.getLoopInfo().next));
      inline_for_body(&cmp, &lc, body);
      lc.add(ENDLOOPCNTXT_OP::create());

      cbody = cmp.assemble();
    }

    RArgRepr bi = c->putProtect(cbody);
    c->add(STARTLOOPCNTXT_OP::create(bi));
  } else {
    Context::InlineLoop lc(c);
    RCompiler cmp(&lc);

    lc.add(STARTFOR_OP::create(callI, i, lc.getLoopInfo().next));
    inline_for_body(&cmp, &lc, body);
  }

  c->add(ENDFOR_OP::create());
  c->add(INVISIBLE_OP::create());

  return true;
}

void RInliner::inline_while_body(RCompiler *cmp, Context::LoopT *c,
                                 const SEXP call, const SEXP body,
                                 const SEXP condition) {

  const Context::LoopInfo &loop = c->getLoopInfo();

  c->putLabel(loop.next);
  cmp->compile(condition);

  RArgRepr i = c->put(call);
  c->add(BRIFNOT_OP::create(i, loop.end));

  cmp->compile(body);
  c->add(POP_OP::create());
  c->add(GOTO_OP::create(loop.next));

  c->putLabel(loop.end);
}

bool RInliner::inline_while(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args) {
  assert_eq(function, Symbol::while_, "Expected 'while'");

  SEXP condition = args[0];
  SEXP body = args[1];

  if (checkEscapingLoopCtx(args)) {
    SEXP cbody;
    {
      Context::Loop lc(c);
      RCompiler cmp(&lc);
      inline_while_body(&cmp, &lc, call, body, condition);
      lc.add(ENDLOOPCNTXT_OP::create());
      cbody = cmp.assemble();
    }

    RArgRepr i = c->putProtect(cbody);
    c->add(STARTLOOPCNTXT_OP::create(i));
  } else {
    Context::InlineLoop lc(c);
    RCompiler cmp(&lc);

    inline_while_body(&cmp, &lc, call, body, condition);
  }

  c->add(LDNULL_OP::create());
  c->add(INVISIBLE_OP::create());
  return true;
}

void RInliner::inline_repeat_body(RCompiler *cmp, Context::LoopT *c,
                                  const SEXP body) {

  const Context::LoopInfo &loop = c->getLoopInfo();

  c->putLabel(loop.next);
  cmp->compile(body);
  c->add(POP_OP::create());

  c->add(GOTO_OP::create(loop.next));
  c->putLabel(loop.end);
}

bool RInliner::inline_repeat(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args) {
  assert_eq(function, Symbol::repeat, "Expected 'repeat'");

  SEXP body = args[0];

  if (checkEscapingLoopCtx(args)) {
    SEXP cbody;
    {
      Context::Loop lc(c);
      RCompiler cmp(&lc);

      inline_repeat_body(&cmp, &lc, body);

      lc.add(ENDLOOPCNTXT_OP::create());
      cbody = cmp.assemble();
    }

    RArgRepr i = c->putProtect(cbody);
    c->add(STARTLOOPCNTXT_OP::create(i));
  } else {
    Context::InlineLoop lc(c);
    RCompiler cmp(&lc);

    inline_repeat_body(&cmp, &lc, body);
  }

  c->add(LDNULL_OP::create());
  c->add(INVISIBLE_OP::create());
  return true;
}

bool RInliner::inline_block(RCompiler *cmp, Context::Any *c, const SEXP call,
                            const SEXP function, const RList &args) {
  assert_eq(function, Symbol::block, "Expected {");

  if (args.empty()) {
    c->add(LDNULL_OP::create());
  } else {
    for (auto e : args) {
      cmp->compile(e.value());
      if (!e.last())
        c->add(POP_OP::create());
    }
  }
  return true;
}

bool RInliner::inline_function(RCompiler *cmp, Context::Any *c, const SEXP call,
                               const SEXP function, const RList &args) {
  assert_eq(function, Symbol::function, "Expected 'function'");

  return false;
  SEXP formals = args[0];
  SEXP body = args[1];
  SEXP closure = Rf_allocVector(VECSXP, 2);
  RArgRepr cli = c->putProtect(closure);

  SEXP cbody;
  {
    RCompiler cmp(c);
    cmp.compileExpression(body);
    cbody = cmp.assemble();
  }

  SET_VECTOR_ELT(closure, 0, formals);
  SET_VECTOR_ELT(closure, 1, cbody);
  c->add(MAKECLOSURE_OP::create(cli));
  return true;
}

bool RInliner::inline_bracket(RCompiler *cmp, Context::Any *c, const SEXP call,
                              const SEXP function, const RList &args) {
  assert_eq(function, Symbol::bracket, "Expected (");

  // TODO: more than 1?
  if (args.last() && args[0] != R_DotsSymbol) {
    cmp->compile(args[0]);
    return true;
  }
  return false;
}

bool RInliner::inline_if(RCompiler *cmp, Context::Any *c, const SEXP call,
                         const SEXP function, const RList &args) {
  assert_eq(function, Symbol::if_, "Expected if");

  SEXP test = args[0];
  SEXP then = args[1];

  bool has_else = !args.binary();

  Fixup::Label l_else = c->createLabel();
  Fixup::Label l_end = c->createLabel();
  {
    cmp->compile(test);
    RArgRepr cl = c->put(call);
    c->add(BRIFNOT_OP::create(cl, l_else));
  }
  {
    cmp->compile(then);
    c->add(GOTO_OP::create(l_end));
  }
  {
    c->putLabel(l_else);
    if (has_else) {
      cmp->compile(args[2]);
    } else {
      c->add(LDNULL_OP::create());
      c->add(INVISIBLE_OP::create());
    }
    c->putLabel(l_end);
  }
  return true;
}

template <Opcode op>
bool RInliner::inline_dollar(
    RCompiler *cmp, Context::Any *c, const SEXP call, const SEXP function,
    const RList &args,
    std::function<void(RCompiler *, Context::Any *, SEXP)> setupLhs) {

  if (!args.binary() || RCompilerHelper::missingDotsTagged(args))
    return false;

  SEXP val = args[0];
  SEXP sym = args[1];

  Match_(sym) {
    Case(Symbol) {
      setupLhs(cmp, c, val);
      // order of argument evaluation is not fixed in C++, causes bugs
      RArgRepr t = c->put(call);
      c->add(encoding::R2(op, t, c->put(_)));
      return true;
    }
    Else() { return false; }
  }
  EndMatch;
  UNREACHABLE;
}

// Search for the most left bottom node in the lhs of a complex assignment. This
// will ultimately be the target for binding the result.
const SEXP RInliner::getAssignedVar(const SEXP lhs) {
  Match_(lhs) {
    Case(Symbol) {
      if (_ == R_MissingArg)
        ERROR(exception::InvalidSExpr("Complex assignment to missing arg"));
      return _;
    }
    Case(CharacterVector) {
      // Returns the first string as symbol (like as.name)
      return Rf_install(CHAR(STRING_ELT(_, 0)));
    }
    Case(Call, _, args) { return (getAssignedVar(RList(args)[0])); }
    Else() {
      ERROR(
          exception::InvalidSExpr("Complex assignment with non-symbol target"));
    }
  }
  EndMatch;
  UNREACHABLE;
}

template <Opcode assignment, Opcode start_assignment, Opcode end_assignment>
bool RInliner::inline_assign(RCompiler *cmp, Context::Any *c, const SEXP call,
                             const SEXP function, const RList &args) {
  assert_(function == Symbol::assign || function == Symbol::rassign ||
          function == Symbol::supAssign);

  if (!args.binary())
    return inline_special(cmp, c, call, function, args);

  const SEXP lhs = args[0];
  const SEXP val = args[1];

  const SEXP target = getAssignedVar(lhs);

  Match_(lhs) {
    Case(Symbol) {
      cmp->compile(val);
      c->add(encoding::R1(assignment, c->put(target)));
      c->add(INVISIBLE_OP::create());
      return true;
    }
    Case(CharacterVector) {
      cmp->compile(val);
      c->add(encoding::R1(assignment, c->put(target)));
      c->add(INVISIBLE_OP::create());
      return true;
    }
    Case(Call) {
      {
        return inline_complex_assign<start_assignment, end_assignment>(
            cmp, c, target, call, function, args);
      }
    }
    Else() { return inline_special(cmp, c, call, function, args); }
  }
  EndMatch;
  UNREACHABLE;
}

template <Opcode start_assignment, Opcode end_assignment>
bool RInliner::inline_complex_assign(RCompiler *cmp, Context::Any *c,
                                     const SEXP target, const SEXP call,
                                     const SEXP function, const RList &args) {
  SEXP lhs = args[0];
  SEXP val = args[1];

  cmp->compile(val);
  c->add(encoding::R1(start_assignment, c->put(target)));

  // Components is the decomposition of getters that have to be called in
  // sequence for the complex assignment.
  std::vector<SEXP> components;
  while (lhs) {
    Match_(lhs) {
      Case(Call, fun, args) {
        SEXP inlined_getter = c->doProtect(Rf_lcons(fun, R_NilValue));
        SEXP inlined_args = Rf_lcons(Symbol::stmp, CDR(args));
        SETCDR(inlined_getter, inlined_args);

        components.push_back(inlined_getter);
        lhs = CAR(args);
      }
      Else() { lhs = NULL; }
    }
    EndMatch;
  }

  // Call the getter inside out
  for (auto i = components.rbegin();
       i != components.rend() && (i + 1) != components.rend(); ++i) {
    // TODO(o) need a shorthand for this match
    Match(*i) {
      Case(Call, fun, args) { inline_getter_call(cmp, c, _, fun, args); }
    }
    EndMatch;
  }

  // Assign rvalue to the extracted lvalue
  Match(components.at(0)) {
    Case(Call, fun, args) { inline_setter_call(cmp, c, _, fun, args, val); }
  }
  EndMatch;

  // Call setters from outside in (using another temporary *vtmp*)
  for (auto i = ++components.begin(); i != components.end(); ++i) {
    Match(*i) {
      Case(Call, fun, args) {
        inline_setter_call(cmp, c, _, fun, args, Symbol::svtmp);
      }
    }
    EndMatch;
  }

  c->add(encoding::R1(end_assignment, c->put(target)));
  c->add(INVISIBLE_OP::create());
  return true;
}

// Converts symbol bla to bla<-
const SEXP RInliner::getAssignFunName(RCompiler *cmp, Context::Any *c,
                                      const SEXP call, const SEXP function,
                                      const RList &args) {
  Match_(function) {
    Case(Symbol) {
      std::stringstream ss;
      ss << CHAR(PRINTNAME(_)) << "<-";
      return Rf_install(ss.str().c_str());
    }
    Case(Call, fun, a) {
      RList args(a);
      if (args.binary() &&
          (fun == Symbol::dcoldcol || fun == Symbol::trippledcol) &&
          isType<sexp_t::Symbol>(args.first()) &&
          isType<sexp_t::Symbol>(args.second())) {

        // Create a new function: a::b becomes a::b<-
        std::stringstream ss;
        ss << CHAR(PRINTNAME(args.second())) << "<-";

        SEXP afun = c->doProtect(Rf_lcons(fun, R_NilValue));
        SEXP newFunArg = Rf_lcons(args.first(), R_NilValue);
        SETCDR(afun, newFunArg);
        SEXP newSymArg = Rf_lcons(Rf_install(ss.str().c_str()), R_NilValue);
        SETCDR(newFunArg, newSymArg);
        return afun;
      }
    }
    Else() {}
  }
  EndMatch;
  return R_NilValue;
}

// Convert f(a, b) <- c to f<-(*tmp*, value = c) for inlined setters
const SEXP RInliner::getAssignFun(RCompiler *cmp, Context::Any *c,
                                  const SEXP call, const SEXP function,
                                  const RList &args, const SEXP value) {

  const SEXP assignFun = getAssignFunName(cmp, c, call, function, args);

  if (isType<sexp_t::Nil>(assignFun)) {
    ERROR(exception::InvalidSExpr("LHS function expression cannot be converted "
                                  "to a complex assignment function"));
  }

  const SEXP assignCall = c->doProtect(Rf_lcons(assignFun, R_NilValue));

  // First argument (the complex rvalue) is replaced with *tmp*
  SEXP newArgs = assignCall;
  const SEXP t = Rf_lcons(Symbol::stmp, R_NilValue);
  SETCDR(newArgs, t);
  newArgs = t;

  // Duplicate the rest of the old langsexp (no protect needed since the head
  // is already protected)
  for (auto a : args.advance()) {
    const SEXP cpy = Rf_lcons(a.value(), R_NilValue);
    SETCDR(newArgs, cpy);
    SET_TAG(cpy, a.tag());
    newArgs = cpy;
  }

  // Add the value argument
  SETCDR(newArgs, Rf_lcons(value, R_NilValue));
  newArgs = CDR(newArgs);
  SET_TAG(newArgs, Symbol::value);

  return assignCall;
}

bool RInliner::inline_setter_call(RCompiler *cmp, Context::Any *c,
                                  const SEXP call, const SEXP function,
                                  const RList &args_, const SEXP value) {

  Match(getAssignFun(cmp, c, call, function, args_, value)) {
    Case(Call, fun) {
      RList args = args_.advance();

      if (try_inline_setter(cmp, c, _, fun, args_))
        return true;

      if (isType<sexp_t::Symbol>(fun)) {
        c->add(GETFUN_OP::create(c->put(fun)));
      } else {
        cmp->compile(fun);
        c->add(CHECKFUN_OP::create());
      }

      c->add(PUSHNULLARG_OP::create());
      // Caveat: those have to be the original args (no value=). its implicit...
      for (auto el : args) {
        cmp->compileCallArg(el.value(), el.tag());
      }
      // order of argument evaluation is not fixed in C++, causes bugs
      RArgRepr t = c->put(_);
      c->add(SETTER_CALL_OP::create(t, c->put(value)));
    }
  }
  EndMatch;

  return true;
}

bool RInliner::inline_getter_call(RCompiler *cmp, Context::Any *c,
                                  const SEXP call, const SEXP fun,
                                  const RList &args) {

  auto insertDup = [](RCompiler *cmp, Context::Any *c,
                      SEXP s) { c->add(DUP2ND_OP::create()); };

  if (!try_inline_getter(cmp, c, call, fun, args, insertDup)) {
    if (isType<sexp_t::Symbol>(fun)) {
      c->add(GETFUN_OP::create(c->put(fun)));
    } else {
      cmp->compile(fun);
      c->add(CHECKFUN_OP::create());
    }
    c->add(PUSHNULLARG_OP::create());

    for (auto el : RList(args.advance())) {
      cmp->compileCallArg(el.value(), el.tag());
    }

    c->add(GETTER_CALL_OP::create(c->put(call)));
  }

  c->add(SWAP_OP::create());

  return true;
}

bool RInliner::tryInlineCall(const SEXP call, const SEXP function,
                             const SEXP args, RCompiler *cmp,
                             Context::Any *c) const {
  auto list = RList(args);
  if (inliner.count(function) != 0) {
    return inliner.at(function)(cmp, c, call, function, list);
  }
  return false;
}
}
