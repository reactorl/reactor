#include "compiler.h"

#include "reactor.h"
#include "context.h"
#include "r/symbols.h"
#include "sexpMatch.h"
#include "r/sexp.h"
#include "bytecode/rbc.h"
#include "r/list.h"

namespace reactor {

void RCompiler::compileCall(const SEXP call, const SEXP function,
                            const SEXP args) {
  Match_(function) {
    Case(Symbol) {
      if (!tryInlineCall(call, function, args)) {
        compileCallSymbol(call, function, args);
      }
    }
    Case(Call, innerFunction) {
      if (isType<sexp_t::Symbol>(innerFunction) &&
          (innerFunction == Symbol::next || innerFunction == Symbol::break_)) {
        compile(function);
      } else {
        compileCallExpression(call, function, args);
      }
    }
    Else() { compileCallExpression(call, function, args); }
  }
  EndMatch
}

void RCompiler::compileMakeProm(const SEXP e) {
  SEXP function;
  {
    Context::Promise pc(c);
    RCompiler cmp(&pc);
    cmp.compileExpression(e);
    function = cmp.assemble();
  }
  RArgRepr funPos = c->putProtect(function);
  c->add(MAKEPROM_OP::create(funPos));
}

void RCompiler::compileCallArg(const SEXP e, const SEXP tag) {
  Match_(e) {
    TrapBytecodeAndPromise();
    Case(Symbol) {
      if (e == R_DotsSymbol) {
        c->add(DODOTS_OP::create());
        return;
      } else if (e == R_MissingArg) {
        c->add(DOMISSING_OP::create());
      } else {
        compileMakeProm(e);
      }
    }
    Case(Call) { compileMakeProm(e); }
    Else() { compileConstant(e, ConstantUsage::PUSH); }
  }
  EndMatch;
  compileTag(tag);
}

void RCompiler::compileTag(const SEXP tag) {
  if (tag != R_NilValue && tag != R_BlankString) {
    assert_type(tag, sexp_t::Symbol);

    RArgRepr tagPos = c->put(tag);
    c->add(SETTAG_OP::create(tagPos));
  }
}

void RCompiler::compileConstant(const SEXP e, ConstantUsage u) {
  if (e == R_NilValue) {
    if (u == ConstantUsage::PUSH)
      c->add(PUSHNULLARG_OP::create());
    else
      c->add(LDNULL_OP::create());
  } else {
    RArgRepr pos = c->put(e);
    if (u == ConstantUsage::PUSH)
      c->add(PUSHCONSTARG_OP::create(pos));
    else
      c->add(LDCONST_OP::create(pos));
  }
}

void RCompiler::compileCallExpression(const SEXP call, const SEXP function,
                                      const SEXP args) {
  compile(function);
  c->add(CHECKFUN_OP::create());

  compileCallCall(call, args);
}

void RCompiler::compileCallSymbol(const SEXP call, const SEXP function,
                                  const SEXP args) {
  assert_type(function, sexp_t::Symbol);

  RArgRepr funNamePos = c->put(function);
  c->add(GETFUN_OP::create(funNamePos));

  compileCallCall(call, args);
}

void RCompiler::compileCallCall(const SEXP call, const SEXP args) {
  for (auto el : RList(args)) {
    compileCallArg(el.value(), el.tag());
  }

  RArgRepr funPos = c->put(call);
  c->add(CALL_OP::create(funPos));
}

/* C/P from envir.c */
int RCompilerHelper::ddVal(SEXP symbol) {
  const char *buf;
  char *endp;
  int rval;

  buf = CHAR(PRINTNAME(symbol));
  if (!strncmp(buf, "..", 2) && strlen(buf) > 2) {
    buf += 2;
    rval = (int)strtol(buf, &endp, 10);
    if (*endp != '\0')
      return 0;
    else
      return rval;
  }
  return 0;
}

void RCompiler::compileSymbol(const SEXP e) {
  assert_type(e, sexp_t::Symbol);

  if (e == R_DotsSymbol) {
    c->add(DOTSERR_OP::create());
  } else if (RCompilerHelper::ddVal(e) > 0) {
    c->add(DDVAL_OP::create(c->put(e)));
  } else {
    RArgRepr namePos = c->put(e);
    c->add(GETVAR_OP::create(namePos));
  }
}

bool RCompilerHelper::missingDotsTagged(const RList &list) {
  for (auto r : list) {
    if (missingDotsTagged1(r))
      return true;
  }
  return false;
}

bool RCompilerHelper::missingDotsTagged1(const RList &r) {
  return r[0] == R_MissingArg || r[0] == R_DotsSymbol || r.tag() != R_NilValue;
}

void RCompiler::compile(const SEXP e) {
  Match_(e) {
    TrapBytecodeAndPromise();
    Case(Call, fun, args) { compileCall(e, fun, args); }
    Case(Symbol) { compileSymbol(e); }
    Else() { compileConstant(e, ConstantUsage::LOAD); }
  }
  EndMatch
}

void RCompiler::compileExpression(const SEXP e) {
  c->put(e);
  compile(e);
}

void RCompiler::compileFunction(const SEXP e) {
  Match(e) {
    Case(Closure, _, body) {
      compileExpression(body);
      // c->put(Rf_install("Compiled by raptoR"));
    }
  }
  EndMatch
}

SEXP RCompiler::assemble() { return c->assemble(); }

const RInliner RCompiler::inliner;
bool RCompiler::tryInlineCall(const SEXP call, const SEXP function,
                              const SEXP args) {
  return inliner.tryInlineCall(call, function, args, this, c);
}
}
