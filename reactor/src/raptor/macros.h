#ifndef MACRO_FOO_H
#define MACRO_FOO_H

#define __ARG6(_0, _1, _2, _3, _4, _5, ...) _5
#define __VA_NUM_ARGS(...) __ARG6(__VA_ARGS__, 5, 4, 3, 2, 1, _EOL)

#define __DISPATCH(fun, ...) __DISPATCH_(fun, __VA_NUM_ARGS(__VA_ARGS__))
#define __DISPATCH_(fun, nargs) __DISPATCH__(fun, nargs)
#define __DISPATCH__(fun, nargs) fun##nargs

#endif
