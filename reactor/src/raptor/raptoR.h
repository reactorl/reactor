#ifndef RAPTOR_COMPILER_IF_H
#define RAPTOR_COMPILER_IF_H

#include "compiler.h"

#include "reactor.h"
#include "context.h"

namespace reactor {

class RCompilerInterface {
public:
  static SEXP function(const SEXP e) {
    {
      Context::Root c;
      RCompiler compiler(&c);
      compiler.compileFunction(e);
      return compiler.assemble();
    }
  }

  static SEXP expression(const SEXP e) {
    {
      Context::Root c;
      RCompiler compiler(&c);
      compiler.compileExpression(e);
      return compiler.assemble();
    }
  }
};
}

extern "C" {
extern SEXP raptoR_compile(SEXP args);
extern SEXP raptoR_compileFunction(SEXP args);
}
#endif
