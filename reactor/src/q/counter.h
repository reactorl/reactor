#ifndef BYTECODE_COUNTER
#define BYTECODE_COUNTER

#include <iomanip>

#include "reactor.h"
#include "bytecode/dispatcher.h"
#include "flair/pass.h"

namespace reactor {

class BytecodeCounter
    : public ins::Dispatcher<BytecodeCounter, Pass::Sequential> {

public:
  static void initialize() {
    bind<Instruction::Predicate>(&BytecodeCounter::count);
  }

  static size_t count(Pass::Sequential pass) {
    BytecodeCounter c;
    pass.execute(c);
    return c.count_;
  }

  void count(Pass::Sequential &l, encoding::OpcodeOnly const &ins) { count_++; }

  size_t count_ = 0;
};

} // namespace flair

#endif // PRINTER
