#ifndef H_REPORTING_
#define H_REPORTING_

#include "reactor.h"
#include "messages.h"

#include <string>
#include <sstream>
#include <tuple>
#include <set>
#include <iostream>
#include <iomanip>

/** Produces a string out its single argument that specifies the string
 * concatenation using the iostream's << operator. This macro avoids the need
 * for explicit conversions of non-string based types. However as iostreams are
 * comparatively slower than the printf statement, this macro should not be used
 * anywhere in time critical applications. Main purpose is to aid easier
 * debugging.
*/
#define STR(WHAT)                                                              \
  ((static_cast<std::ostringstream &>(                                         \
        std::ostringstream().seekp(0, std::ios_base::cur) << WHAT)).str())

// RS.h macro pollution
#undef ERROR
#undef WARNING

#define ERROR(what)                                                            \
  {                                                                            \
    auto _e_ = what;                                                           \
    _e_.setPosition(__FILE__, __LINE__);                                       \
    throw _e_;                                                                 \
  }
#define WARNING(what)                                                          \
  {                                                                            \
    auto _e_ = what;                                                           \
    _e_.setPosition(__FILE__, __LINE__);                                       \
    std::cerr << _e_ << std::endl;                                             \
  }
#define NOTICE(what)                                                           \
  {                                                                            \
    auto _e_ = what;                                                           \
    _e_.setPosition(__FILE__, __LINE__);                                       \
    std::cout << _e_ << std::endl;                                             \
  }

#ifdef DEBUG

#define Debug(type, what) Debug::out(Debug::Type::type, what)
#define DebugIndent() Debug::indent()
#define DebugUnindent() Debug::unindent()

#else

#define Debug(level, what)                                                     \
  {}
#define DebugIndent()                                                          \
  {}
#define DebugUnindent()                                                        \
  {}

#endif

namespace reactor {

class Debug {
public:
  enum class Type : int { SCHEDULING, STATES };

  static void out(Type type, std::string const &message) {
    if (enabled.count(type))
      out(message);
  }

  static void out(std::string const &message) {
    std::cout << std::setw(indentation) << " " << message << std::endl;
  }

  /** Increases debug messages indent by 4.
   */
  static void indent() { indentation += 4; }

  /** Decreases debug messages indent by 4.
   */
  static void unindent() { indentation -= 4; }

private:
  static size_t indentation;
  static std::set<Type> enabled;
};

void log(std::string const &what);

std::ostream &logStream();

class BaseMessage : public std::runtime_error {
public:
  char const *file() const noexcept { return file_; }

  unsigned line() const noexcept { return line_; }

  virtual char const *type() const noexcept { return "BaseMessage"; }

  void setPosition(char const *file, unsigned line) {
    file_ = file;
    line_ = line;
  }

  void report() const noexcept;

protected:
  BaseMessage(std::string const &what) noexcept : std::runtime_error(what),
                                                  file_(nullptr),
                                                  line_(-1) {}

  BaseMessage(char const *file, unsigned line, std::string const &what) noexcept
      : std::runtime_error(what),
        file_(file),
        line_(line) {}

private:
  char const *file_;
  unsigned line_;
}; // BaseMessage

inline std::ostream &operator<<(std::ostream &stream,
                                BaseMessage const &message) {
  stream << message.type() << " raised at " << message.file() << ":"
         << message.line() << std::endl;
  stream << message.what() << std::endl;
  return stream;
}

template <char const *const TYPE, char const *const FORMAT, typename... ARGS>
class Message : public BaseMessage {
public:
  Message(ARGS... args)
      : BaseMessage(Format<static_cast<std::ptrdiff_t>(sizeof...(ARGS)-1),
                           ARGS...>::format(std::string(FORMAT),
                                            std::tuple<ARGS...>(args...))) {}

  Message(char const *file, unsigned line, ARGS... args)
      : BaseMessage(file, line,
                    Format<static_cast<std::ptrdiff_t>(sizeof...(ARGS)-1),
                           ARGS...>::format(std::string(FORMAT),
                                            std::tuple<ARGS...>(args...))) {}

  virtual char const *type() const noexcept override { return TYPE; }

private:
  /** Loop unrolling for the replacement of the message by its arguments.
  */
  template <std::ptrdiff_t INDEX, typename... ARGS2> class Format {
  public:
    static inline std::string format(std::string str,
                                     std::tuple<ARGS2...> args) noexcept {
      std::string toReplace = STR("{" << INDEX << "}");
      size_t f = str.find(toReplace);
      if (f != str.npos)
        str.replace(f, toReplace.length(), STR(std::get<INDEX>(args)));
      return Format<INDEX - 1, ARGS2...>::format(str, args);
    }
  };

  /** Loop unrolling termination.
  */
  template <typename... ARGS2> class Format<-1, ARGS2...> {
  public:
    static inline std::string format(std::string str,
                                     std::tuple<ARGS2...> args) noexcept {
      return str;
    }
  };

}; // Message

namespace exception {
/** Contains the compile time constant strings required for the message
declarations.

Because the string template arguments require to be compile time constants, the
name of each message type, as well as the format string are stored in the
rr:message::c_str namespace.
*/
namespace c_str {
#define V(name, format, ...)                                                   \
  extern const char name##_format[];                                           \
  extern const char name[];
MESSAGE_LIST(V)
#undef V

} // namespace c_str
#define V(name, format, ...)                                                   \
  typedef Message<c_str::name, c_str::name##_format, __VA_ARGS__> name;
MESSAGE_LIST(V)
#undef V

} // namespace exception

} // namespace reactor

#endif // H_REPORTING_
