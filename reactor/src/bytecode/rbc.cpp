#include "rbc.h"
#include "reporting.h"

namespace reactor {

// Instruction

Instruction *Instruction::insns_[NUM_OPCODES];

std::string Instruction::opcodeToString(Opcode opcode) {
  switch (opcode) {
#define OPCODE_TO_STRING(NAME, ...)                                            \
  case Opcode::NAME:                                                           \
    return #NAME;
    R_OPCODE_LIST(OPCODE_TO_STRING)
#undef OPCODE_TO_STRING
  default:
    return STR("Unknown instruction - opcode " << static_cast<int>(opcode));
  }
}

/* Definition of the static singletons for the instruction types.
 */
#define OPCODE_SINGLETON(NAME, ID, ENCODING, TYPE, STACK_READ, STACK_WRITE,    \
                         BRANCH_ARG)                                           \
  NAME const NAME::_;
R_OPCODE_LIST(OPCODE_SINGLETON)
#undef OPCODE_SINGLETON

} // namespace reactor
