#ifndef BYTECODE_PRINTER
#define BYTECODE_PRINTER

#include <iomanip>

#include "reactor.h"
#include "bytecode/code_buffer.h"
#include "bytecode/rbc.h"
#include "bytecode/dispatcher.h"
#include "flair/pass.h"

namespace reactor {

class BytecodePrinter
    : public ins::Dispatcher<BytecodePrinter, Pass::Sequential const> {

public:
  BytecodePrinter(std::ostream &stream) : stream(stream) {}
  BytecodePrinter() : stream(std::cout) {}

  static void initialize() {
    bind<InstructionR0::Predicate>(&BytecodePrinter::R0);
    bind<InstructionR1::Predicate>(&BytecodePrinter::R1);
    bind<InstructionR2::Predicate>(&BytecodePrinter::R2);
    bind<InstructionR3::Predicate>(&BytecodePrinter::R3);
    bind<InstructionR4::Predicate>(&BytecodePrinter::R4);
  }

private:
  void R0(Pass::Sequential const &l, encoding::R0 const &ins) {
    stream << std::setw(5) << l.pc() << " ";
    stream << Instruction::opcodeToString(ins.opcode) << std::endl;
  }

  void R1(Pass::Sequential const &l, encoding::R1 const &ins) {
    stream << std::setw(5) << l.pc() << " ";
    stream << Instruction::opcodeToString(ins.opcode) << " " << ins.arg1
           << std::endl;
  }

  void R2(Pass::Sequential const &l, encoding::R2 const &ins) {
    stream << std::setw(5) << l.pc() << " ";
    stream << Instruction::opcodeToString(ins.opcode) << " " << ins.arg1 << " "
           << ins.arg2 << std::endl;
  }

  void R3(Pass::Sequential const &l, encoding::R3 const &ins) {
    stream << std::setw(5) << l.pc() << " ";
    stream << Instruction::opcodeToString(ins.opcode) << " " << ins.arg1 << " "
           << ins.arg2 << " " << ins.arg3 << std::endl;
  }

  void R4(Pass::Sequential const &l, encoding::R4 const &ins) {
    stream << std::setw(5) << l.pc() << " ";
    stream << Instruction::opcodeToString(ins.opcode) << " " << ins.arg1 << " "
           << ins.arg2 << " " << ins.arg3 << " " << ins.arg4 << std::endl;
  }

  std::ostream &stream;
};

} // namespace flair

#endif // PRINTER
