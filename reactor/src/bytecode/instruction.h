#ifndef H_INSTRUCTION_
#define H_INSTRUCTION_

#include <cinttypes>
#include <string>

#include "opcodes.h"

#define PREDICATE(HANDLER_ARG, ...)                                            \
  typedef InstructionPredicate<HANDLER_ARG, __VA_ARGS__> Predicate

namespace reactor {

template <typename HANDLER_ARG, Opcode... OPCODES> class InstructionPredicate;

// ----------------------------------------------------------------------------------------------------------------

/** Base class for instruction manipulators in flair.
 *
 */
class Instruction {
public:
  PREDICATE(encoding::OpcodeOnly, Opcode::OPCODES_ALL_);

  virtual Opcode opcode() const abstract;

  virtual size_t size() const abstract;

  virtual size_t stackIn() const abstract;

  virtual size_t stackOut() const abstract;

  virtual bool doesBranch() const abstract;

  virtual size_t branchArg() const abstract;

  static Instruction const &get(Opcode opcode) {
    return *insns_[static_cast<unsigned>(opcode)];
  }

  static std::string opcodeToString(Opcode opcode);

  /** Returns true if the instruction is a conditional jump one,
     false otherwise.
     TODO: unify with the predicates
   */
  static bool isConditionalJump(Opcode opcode) {
    switch (opcode) {
    case Opcode::BRIFNOT_OP:
    case Opcode::AND1ST_OP:
    case Opcode::OR1ST_OP:
    case Opcode::STARTSUBSET_OP:
    case Opcode::STARTSUBSET2_OP:
    case Opcode::STARTC_OP:
    case Opcode::STARTSUBASSIGN_N_OP:
    case Opcode::STARTSUBASSIGN2_N_OP:
    case Opcode::STARTSUBSET_N_OP:
    case Opcode::STARTSUBSET2_N_OP:
    case Opcode::STEPFOR_OP:
      return true;
    default:
      return false;
    }
  }

  // static unsigned encodingSize(Opcode opcode);

protected:
  /* Associate an instruction object to an opcode. Only for concrete
   * instructions. */
  static void registerInstruction(Opcode opcode, Instruction *instruction) {
    insns_[static_cast<unsigned>(opcode)] = instruction;
  }

  /* Side-effect: bind an instruction to its opcode in the instruction set
   * table. */
  Instruction(Opcode opcode) { registerInstruction(opcode, this); }

private:
  static Instruction *insns_[NUM_OPCODES];
};

} // namespace reactor

#endif // H_INSTRUCTION_
