#ifndef RBC_H
#define RBC_H

#include <cinttypes>

#include "instruction.h"
#include "code_buffer.h"

namespace reactor {

namespace encoding {
#pragma pack(push, 1) // disable padding

/** Simple encoding for R bytecode instructions containing only the opcode.
*/
typedef OpcodeOnly R0;

/** R instruction containing an opcode and a single argument.
*/
struct R1 : public R0 {
  RArgRepr arg1;
  R1(Opcode opcode, RArgRepr arg1) : R0(opcode), arg1(arg1) {}

  static const size_t size = 2;

  SEXP constant1(CodeBuffer const &code) const { return code.constants[arg1]; }
};

/** R Instruction with an opcode and two arguments.
*/
struct R2 : public R0 {
  RArgRepr arg1;
  RArgRepr arg2;
  R2(Opcode opcode, RArgRepr arg1, RArgRepr arg2)
      : R0(opcode), arg1(arg1), arg2(arg2) {}

  static const size_t size = 3;

  SEXP constant1(CodeBuffer const &code) const { return code.constants[arg1]; }

  SEXP constant2(CodeBuffer const &code) const { return code.constants[arg2]; }
};

/** R Instruction with an opcode and three arguments.
*/
struct R3 : public R0 {
  RArgRepr arg1;
  RArgRepr arg2;
  RArgRepr arg3;
  R3(Opcode opcode, RArgRepr arg1, RArgRepr arg2, RArgRepr arg3)
      : R0(opcode), arg1(arg1), arg2(arg2), arg3(arg3) {}

  static const size_t size = 4;

  SEXP constant1(CodeBuffer const &code) const { return code.constants[arg1]; }

  SEXP constant2(CodeBuffer const &code) const { return code.constants[arg2]; }

  SEXP constant3(CodeBuffer const &code) const { return code.constants[arg3]; }
};

/** R Instruction with an opcode and four arguments.
*/
struct R4 : public R0 {
  RArgRepr arg1;
  RArgRepr arg2;
  RArgRepr arg3;
  RArgRepr arg4;
  R4(Opcode opcode, RArgRepr arg1, RArgRepr arg2, RArgRepr arg3, RArgRepr arg4)
      : R0(opcode), arg1(arg1), arg2(arg2), arg3(arg3), arg4(arg4) {}

  static const size_t size = 5;

  SEXP constant1(CodeBuffer const &code) const { return code.constants[arg1]; }

  SEXP constant2(CodeBuffer const &code) const { return code.constants[arg2]; }

  SEXP constant3(CodeBuffer const &code) const { return code.constants[arg3]; }

  SEXP constant4(CodeBuffer const &code) const { return code.constants[arg4]; }
};

static_assert(R0::size == sizeof(R0) / sizeof(OpcodeRepr),
              "Invalid encoding size for R0");
static_assert(R1::size == sizeof(R1) / sizeof(OpcodeRepr),
              "Invalid encoding size for R1");
static_assert(R2::size == sizeof(R2) / sizeof(OpcodeRepr),
              "Invalid encoding size for R2");
static_assert(R3::size == sizeof(R3) / sizeof(OpcodeRepr),
              "Invalid encoding size for R3");
static_assert(R4::size == sizeof(R4) / sizeof(OpcodeRepr),
              "Invalid encoding size for R4");

#pragma pack(pop) // reenable original padding value
} // namespace encoding

#define ENC_R0(NAME) , Opcode::NAME
#define ENC_R1(NAME)
#define ENC_R2(NAME)
#define ENC_R3(NAME)
#define ENC_R4(NAME)
#define ENC(NAME, ID, ENCODING, TYPE, STACK_OUT, STACK_IN, BRANCH_ARG)         \
  ENC_##ENCODING(NAME)

class InstructionR0 : public Instruction {
public:
  typedef encoding::R0 Encoding;
  typedef InstructionPredicate<encoding::R0 R_OPCODE_LIST(ENC)> Predicate;

  size_t size() const override { return encoding::R0::size; }

protected:
  InstructionR0(Opcode opcode) : Instruction(opcode) {}
};

#undef ENC_R0
#undef ENC_R1
#define ENC_R0(NAME)
#define ENC_R1(NAME) , Opcode::NAME

class InstructionR1 : public Instruction {
public:
  typedef encoding::R1 Encoding;
  typedef InstructionPredicate<encoding::R1 R_OPCODE_LIST(ENC)> Predicate;

  size_t size() const override { return encoding::R1::size; }

protected:
  InstructionR1(Opcode opcode) : Instruction(opcode) {}
};

#undef ENC_R1
#undef ENC_R2
#define ENC_R1(NAME)
#define ENC_R2(NAME) , Opcode::NAME

class InstructionR2 : public Instruction {
public:
  typedef encoding::R2 Encoding;
  typedef InstructionPredicate<encoding::R2 R_OPCODE_LIST(ENC)> Predicate;

  size_t size() const override { return encoding::R2::size; }

protected:
  InstructionR2(Opcode opcode) : Instruction(opcode) {}
};

#undef ENC_R2
#undef ENC_R3
#define ENC_R2(NAME)
#define ENC_R3(NAME) , Opcode::NAME

class InstructionR3 : public Instruction {
public:
  typedef encoding::R3 Encoding;
  typedef InstructionPredicate<encoding::R3 R_OPCODE_LIST(ENC)> Predicate;

  size_t size() const override { return encoding::R3::size; }

protected:
  InstructionR3(Opcode opcode) : Instruction(opcode) {}
};

#undef ENC_R3
#undef ENC_R4
#define ENC_R3(NAME)
#define ENC_R4(NAME) , Opcode::NAME

class InstructionR4 : public Instruction {
public:
  typedef encoding::R4 Encoding;
  typedef InstructionPredicate<encoding::R4 R_OPCODE_LIST(ENC)> Predicate;

  size_t size() const override { return encoding::R4::size; }

protected:
  InstructionR4(Opcode opcode) : Instruction(opcode) {}
};

#undef ENC_R0
#undef ENC_R1
#undef ENC_R2
#undef ENC_R3
#undef ENC_R4
#undef ENC

#define CREATE_R0(OPCODE)                                                      \
  static encoding::R0 create() { return encoding::R0(OPCODE); }
#define CREATE_R1(OPCODE)                                                      \
  static encoding::R1 create(RArgRepr arg1) {                                  \
    return encoding::R1(OPCODE, arg1);                                         \
  }
#define CREATE_R2(OPCODE)                                                      \
  static encoding::R2 create(RArgRepr arg1, RArgRepr arg2) {                   \
    return encoding::R2(OPCODE, arg1, arg2);                                   \
  }
#define CREATE_R3(OPCODE)                                                      \
  static encoding::R3 create(RArgRepr arg1, RArgRepr arg2, RArgRepr arg3) {    \
    return encoding::R3(OPCODE, arg1, arg2, arg3);                             \
  }
#define CREATE_R4(OPCODE)                                                      \
  static encoding::R4 create(RArgRepr arg1, RArgRepr arg2, RArgRepr arg3,      \
                             RArgRepr arg4) {                                  \
    return encoding::R4(OPCODE, arg1, arg2, arg3, arg4);                       \
  }

#define OPCODE_CLASS(NAME, ID, ENCODING, TYPE, STACK_OUT, STACK_IN,            \
                     BRANCH_ARG)                                               \
  class NAME : public TYPE {                                                   \
  public:                                                                      \
    /* PREDICATE(NAME, Opcode::NAME); */                                       \
    PREDICATE(encoding::ENCODING, Opcode::NAME);                               \
    typedef encoding::ENCODING Encoding;                                       \
    Opcode opcode() const override { return Opcode::NAME; }                    \
    size_t stackIn() const override { return STACK_IN; }                       \
    size_t stackOut() const override { return STACK_OUT; }                     \
    bool doesBranch() const override { return BRANCH_ARG != 0; }               \
    size_t branchArg() const override { return BRANCH_ARG; }                   \
    CREATE_##ENCODING(Opcode::NAME) static NAME const _;                       \
                                                                               \
  private:                                                                     \
    NAME(NAME const &) = delete;                                               \
    NAME &operator=(NAME const &) = delete;                                    \
    NAME() : TYPE(Opcode::NAME) {}                                             \
  };

R_OPCODE_LIST(OPCODE_CLASS)
#undef OPCODE_CLASS
#undef CREATE_R0
#undef CREATE_R1
#undef CREATE_R2
#undef CREATE_R3
#undef CREATE_R4

/** TODO Perhaps these two should be accompanied by fat instruction classes with
 * other semantic information. It would also allow us to use them in the manner
 * consitent to other predicates, i.e. ConditionalJumo::Predicate. But on the
 * other hand, user defined predicates will likely not follow this rule anyways.
 */
typedef InstructionPredicate<encoding::R1, Opcode::GOTO_OP> Jump;

// TODO: unify with Instruction::isConditionalJump
typedef InstructionPredicate<encoding::R1, Opcode::STEPFOR_OP> ConditionalJump1;

typedef InstructionPredicate<
    encoding::R2, Opcode::BRIFNOT_OP, Opcode::AND1ST_OP, Opcode::OR1ST_OP,
    Opcode::STARTSUBSET_OP, Opcode::STARTSUBSET2_OP, Opcode::STARTC_OP,
    Opcode::STARTSUBASSIGN_N_OP, Opcode::STARTSUBASSIGN2_N_OP,
    Opcode::STARTSUBSET_N_OP, Opcode::STARTSUBSET2_N_OP> ConditionalJump2;

// TODO there may be more unary operators
typedef InstructionPredicate<encoding::R1, Opcode::UMINUS_OP, Opcode::UPLUS_OP>
    UnaryOperator;

// TODO There may be more binary operators
typedef InstructionPredicate<encoding::R1, Opcode::ADD_OP, Opcode::SUB_OP,
                             Opcode::MUL_OP, Opcode::DIV_OP> BinaryOperator;

} // namespace reactor

#endif // RBC_H
