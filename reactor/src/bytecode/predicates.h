#ifndef H_PREDICATES_
#define H_PREDICATES_

#include <vector>

#include "opcodes.h"

/** Defines an unconditional predicate, i.e. a predicate that dispatches based
 * only on the instruction type.
 */
#define UNCONDITIONAL                                                          \
  static bool isConditional() { return false; }                                \
  static bool condition(BaseDispatcher &dispatcher, CodeBuffer const &code,    \
                        size_t pc) {                                           \
    return true;                                                               \
  }

/** Defines conditional predicate that dispatches on the instruction type
 * followed by an arbitrary function check. The body of this conditional check
 * follows the macro. The first argument to the check function is the analysis
 * object on which the dispatch occurs and the second is the actual location. */
#define CONDITIONAL                                                            \
  static bool isConditional() { return true; }                                 \
  static bool condition(BaseDispatcher &dispatcher, CodeBuffer const &code,    \
                        size_t pc) {

#define MATCHES(...)                                                           \
  static std::vector<size_t> const &matches() {                                \
    static std::vector<size_t> v({__VA_ARGS__});                               \
    return v;                                                                  \
  }

#define EXPECTS(INSTRUCTION_CLASS) typedef INSTRUCTION_CLASS ARG;

namespace reactor {

class BaseDispatcher;

class BaseAnalysis;

enum class Opcode;

/** Template for creation of opcode lists from template arguments.

First a base case for single opcode, then followed by the recursive definition.
*/

template <Opcode T>
std::vector<size_t> &MakeVector(std::vector<size_t> &opcodes) {
  opcodes.push_back(static_cast<size_t>(T));
  return opcodes;
}

template <Opcode T1, Opcode T2, Opcode... Tn>
std::vector<size_t> &MakeVector(std::vector<size_t> &opcodes) {
  MakeVector<T1>(opcodes);
  MakeVector<T2, Tn...>(opcodes);
  return opcodes;
}

template <Opcode... Tn> std::vector<size_t> CreateVector() {
  std::vector<size_t> o;
  MakeVector<Tn...>(o);
  return o;
}

// ----------------------------------------------------------------------------------------------------------------

/** Template for unconditional predicates for one or many instructions.
*/
template <typename HANDLER_ARG, Opcode... OPCODES> class InstructionPredicate {
public:
  typedef HANDLER_ARG ARG;
  static std::vector<size_t> const &matches() {
    static std::vector<size_t> v(CreateVector<OPCODES...>());
    return v;
  }
  UNCONDITIONAL;
};

} // namespace flair

#endif // H_PREDICATES_
