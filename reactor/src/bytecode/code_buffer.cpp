#include "code_buffer.h"

#include "dispatcher.h"

namespace {

using namespace reactor;

} // namespace

namespace reactor {

// Since R SEXP does not have a hashvalue we provide this really stupid but
// efficient hash function, which should make maps at least faster than using
// linear search (considering R_compute_identical is really slow).
size_t CodeBuffer::Constants::sexp_eq_hash::operator()(const SEXP &s) const {
  size_t b = std::hash<int>()(TYPEOF(s)) ^ std::hash<bool>()(OBJECT(s)) ^
             std::hash<bool>()(IS_S4_OBJECT(s));
  switch (TYPEOF(s)) {
  default:
  case NILSXP:
  case ENVSXP:
  case SYMSXP:
  case WEAKREFSXP:
  case BCODESXP:
  case EXTPTRSXP:
    return std::hash<uintptr_t>()(reinterpret_cast<uintptr_t>(s)) ^ b;
  case LGLSXP:
    return std::hash<int>()(Rf_xlength(s)) ^ std::hash<bool>()(*LOGICAL(s)) ^ b;
  case INTSXP:
    return std::hash<int>()(Rf_xlength(s)) ^ std::hash<int>()(*INTEGER(s)) ^ b;
  case CPLXSXP:
    return std::hash<int>()(Rf_xlength(s)) ^
           std::hash<double>()(COMPLEX(s)->i) ^
           std::hash<double>()(COMPLEX(s)->r) ^ b;
  case REALSXP:
    return std::hash<int>()(Rf_xlength(s)) ^ std::hash<double>()(*REAL(s)) ^ b;
  case EXPRSXP:
  case VECSXP:
    return std::hash<int>()(Rf_xlength(s)) ^
           std::hash<uintptr_t>()(
               reinterpret_cast<uintptr_t>(VECTOR_ELT(s, 0))) ^
           b;
  case STRSXP:
    return std::hash<int>()(Rf_xlength(s)) ^
           std::hash<char>()(*CHAR(STRING_ELT(s, 0))) ^ b;
  case CHARSXP:
  case RAWSXP:
    return std::hash<int>()(Rf_xlength(s)) ^ std::hash<char>()(*CHAR(s)) ^ b;
  case PROMSXP:
  case DOTSXP:
  case LANGSXP:
  case LISTSXP:
  case CLOSXP:
  case SPECIALSXP:
  case BUILTINSXP:
    return std::hash<uintptr_t>()(reinterpret_cast<uintptr_t>(CAR(s))) ^
           std::hash<uintptr_t>()(reinterpret_cast<uintptr_t>(CDR(s))) ^ b;
  case S4SXP:
    return b ^ std::hash<uintptr_t>()(reinterpret_cast<uintptr_t>(ATTRIB(s)));
  }
}

} // namespace reactor
