#ifndef H_OPCODES
#define H_OPCODES

#include <R.h>

#include <cstdint>

#include "reactor.h"

/** Define the GNU-R instruction set, each has:
 *    - opcode name  (string)
 *    - opcode value (unsigned short)
 *    - encoding (class)
 *    - parent (class)
      - stack out (number of arguments removed from stack)
      - stack in (number of arguments pushed to stack)
      - branch argument

 *  The opcoode name will be used in the code to refer to the instruction.
 *  The opcode value will occur in the instruction stream. The encoding is
 *  a data structure that is used to read and write the instruction and
 *  its arguments to the instruction steam. The parent is the class from
 *  which the instruction inherits. */

#define R_OPCODE_LIST(V)                                                       \
  V(BCMISMATCH_OP, 0, R0, InstructionR0, 0, 0, 0)                              \
  /* Terminates the function and returns the top value on the stack. */        \
  V(RETURN_OP, 1, R0, InstructionR0, 1, 0, 0)                                  \
  /* Jumps to the given address. */                                            \
  V(GOTO_OP, 2, R1, InstructionR1, 0, 0, 0)                                    \
  /* Given the top value on stack, conditionally jumps to the specified        \
     address. */                                                               \
  V(BRIFNOT_OP, 3, R2, InstructionR2, 1, 0, 2)                                 \
  /* Pops single value from the stack. */                                      \
  V(POP_OP, 4, R0, InstructionR0, 1, 0, 0)                                     \
  /* Duplicates the top of the stack. */                                       \
  V(DUP_OP, 5, R0, InstructionR0, 0, 1, 0)                                     \
  /* Prints stack top and pops. */                                             \
  V(PRINTVALUE_OP, 6, R0, InstructionR0, 1, 0, 0)                              \
  V(STARTLOOPCNTXT_OP, 7, R1, InstructionR1, 0, 0, 0)                          \
  V(ENDLOOPCNTXT_OP, 8, R0, InstructionR0, 0, 0, 0)                            \
  V(DOLOOPNEXT_OP, 9, R0, InstructionR0, 0, 0, 0)                              \
  V(DOLOOPBREAK_OP, 10, R0, InstructionR0, 0, 0, 0)                            \
  V(STARTFOR_OP, 11, R3, InstructionR3, 0, 0, 3)                               \
  V(STEPFOR_OP, 12, R1, InstructionR1, 0, 0, 1)                                \
  V(ENDFOR_OP, 13, R0, InstructionR0, 0, 0, 0)                                 \
  V(SETLOOPVAL_OP, 14, R0, InstructionR0, 0, 0, 0)                             \
  V(INVISIBLE_OP, 15, R0, InstructionR0, 0, 0, 0)                              \
  /* Pushes given constant to stack. */                                        \
  V(LDCONST_OP, 16, R1, InstructionR1, 0, 1, 0)                                \
  /* Pushes NULL constant to stack. */                                         \
  V(LDNULL_OP, 17, R0, InstructionR0, 0, 1, 0)                                 \
  /* Pushes TRUE constant to stack. */                                         \
  V(LDTRUE_OP, 18, R0, InstructionR0, 0, 1, 0)                                 \
  /* Pushes FALSE constant to stack. */                                        \
  V(LDFALSE_OP, 19, R0, InstructionR0, 0, 1, 0)                                \
  /* Pushes the contents of given variable to stack. */                        \
  V(GETVAR_OP, 20, R1, InstructionR1, 0, 1, 0)                                 \
  V(DDVAL_OP, 21, R1, InstructionR1, 0, 0, 0)                                  \
  /* Stores the argument on stack to given variable. Keeps the argument on     \
     stack. */                                                                 \
  V(SETVAR_OP, 22, R1, InstructionR1, 0, 0, 0)                                 \
  /* Pushes the closure (specified by constant lookup symbol, 0) and           \
     structures                                                                \
     necessary for a successful call on stack (arguments list, etc., 0). */    \
  V(GETFUN_OP, 23, R1, InstructionR1, 0, 3, 0)                                 \
  V(GETGLOBFUN_OP, 24, R1, InstructionR1, 0, 0, 0)                             \
  V(GETSYMFUN_OP, 25, R1, InstructionR1, 0, 0, 0)                              \
  V(GETBUILTIN_OP, 26, R1, InstructionR1, 0, 0, 0)                             \
  V(GETINTLBUILTIN_OP, 27, R1, InstructionR1, 0, 0, 0)                         \
  V(CHECKFUN_OP, 28, R0, InstructionR0, 0, 0, 0)                               \
  /* Creates promise from the given argument and stores it to the arguments    \
     list already on stack. If the callee is builtin function, promise is      \
     not created. Does nothing for special calls. */                           \
  V(MAKEPROM_OP, 29, R1, InstructionR1, 0, 0, 0)                               \
  /* Adds value for missing argument to the arguments list already on          \
     stack. */                                                                 \
  V(DOMISSING_OP, 30, R0, InstructionR0, 0, 0, 0)                              \
  /* Marks the last pushed argument as keyword argument with given name. */    \
  V(SETTAG_OP, 31, R1, InstructionR1, 0, 0, 0)                                 \
  V(DODOTS_OP, 32, R0, InstructionR0, 0, 0, 0)                                 \
  /* Pushes argument already on stack to arguments list. */                    \
  V(PUSHARG_OP, 33, R0, InstructionR0, 1, 0, 0)                                \
  /* Adds the given constant to the arguments list already on stack. */        \
  V(PUSHCONSTARG_OP, 34, R1, InstructionR1, 0, 0, 0)                           \
  /* Adds NULL constant to the arguments list already on stack. */             \
  V(PUSHNULLARG_OP, 35, R0, InstructionR0, 0, 0, 0)                            \
  /* Adds TRUE constant to the arguments list already on stack. */             \
  V(PUSHTRUEARG_OP, 36, R0, InstructionR0, 0, 0, 0)                            \
  /* Adds FALSE constant to the arguments list already on stack. */            \
  V(PUSHFALSEARG_OP, 37, R0, InstructionR0, 0, 0, 0)                           \
  /* Given the function and arguments list structures already on stack,        \
     performs the call and pushes the return value on stack. */                \
  V(CALL_OP, 38, R1, InstructionR1, 3, 1, 0)                                   \
  V(CALLBUILTIN_OP, 39, R1, InstructionR1, 0, 0, 0)                            \
  V(CALLSPECIAL_OP, 40, R1, InstructionR1, 0, 0, 0)                            \
  /* Creates closure from current environment, formals and body, both          \
     parts of the constant index. */                                           \
  V(MAKECLOSURE_OP, 41, R1, InstructionR1, 0, 1, 0)                            \
  /* Unary minus operator. */                                                  \
  V(UMINUS_OP, 42, R1, InstructionR1, 1, 1, 0)                                 \
  /* Unary plus operator. */                                                   \
  V(UPLUS_OP, 43, R1, InstructionR1, 1, 1, 0)                                  \
  /* Addition. */                                                              \
  V(ADD_OP, 44, R1, InstructionR1, 2, 1, 0)                                    \
  /* Subtraction. */                                                           \
  V(SUB_OP, 45, R1, InstructionR1, 2, 1, 0)                                    \
  /* Multiplication. */                                                        \
  V(MUL_OP, 46, R1, InstructionR1, 2, 1, 0)                                    \
  /* Division. */                                                              \
  V(DIV_OP, 47, R1, InstructionR1, 2, 1, 0)                                    \
  /* Exponent (binary). */                                                     \
  V(EXPT_OP, 48, R1, InstructionR1, 2, 1, 0)                                   \
  /* Square root (unary). */                                                   \
  V(SQRT_OP, 49, R1, InstructionR1, 1, 1, 0)                                   \
  /* Exponent (exp R function, unary) */                                       \
  V(EXP_OP, 50, R1, InstructionR1, 1, 1, 0)                                    \
  /* Equality test. */                                                         \
  V(EQ_OP, 51, R1, InstructionR1, 2, 1, 0)                                     \
  /* Inequality test. */                                                       \
  V(NE_OP, 52, R1, InstructionR1, 2, 1, 0)                                     \
  /* Less than. */                                                             \
  V(LT_OP, 53, R1, InstructionR1, 2, 1, 0)                                     \
  /* Less than or equal. */                                                    \
  V(LE_OP, 54, R1, InstructionR1, 2, 1, 0)                                     \
  /* Greater than or equal. */                                                 \
  V(GE_OP, 55, R1, InstructionR1, 2, 1, 0)                                     \
  /* Greater than. */                                                          \
  V(GT_OP, 56, R1, InstructionR1, 2, 1, 0)                                     \
  /* Bitwise and. */                                                           \
  V(AND_OP, 57, R1, InstructionR1, 2, 1, 0)                                    \
  /* Bitwise or. */                                                            \
  V(OR_OP, 58, R1, InstructionR1, 2, 1, 0)                                     \
  /* Not. */                                                                   \
  V(NOT_OP, 59, R1, InstructionR1, 1, 1, 0)                                    \
  /* Error emitted when ... are used in wrong context. */                      \
  V(DOTSERR_OP, 60, R0, InstructionR0, 0, 0, 0)                                \
  V(STARTASSIGN_OP, 61, R1, InstructionR1, 0, 0, 0)                            \
  V(ENDASSIGN_OP, 62, R1, InstructionR1, 0, 0, 0)                              \
  V(STARTSUBSET_OP, 63, R2, InstructionR2, 0, 0, 0)                            \
  V(DFLTSUBSET_OP, 64, R0, InstructionR0, 0, 0, 0)                             \
  V(STARTSUBASSIGN_OP, 65, R2, InstructionR2, 0, 0, 0)                         \
  V(DFLTSUBASSIGN_OP, 66, R0, InstructionR0, 0, 0, 0)                          \
  V(STARTC_OP, 67, R2, InstructionR2, 0, 0, 2)                                 \
  V(DFLTC_OP, 68, R0, InstructionR0, 0, 0, 0)                                  \
  V(STARTSUBSET2_OP, 69, R2, InstructionR2, 0, 0, 0)                           \
  V(DFLTSUBSET2_OP, 70, R0, InstructionR0, 0, 0, 0)                            \
  V(STARTSUBASSIGN2_OP, 71, R2, InstructionR2, 0, 0, 0)                        \
  V(DFLTSUBASSIGN2_OP, 72, R0, InstructionR0, 0, 0, 0)                         \
  V(DOLLAR_OP, 73, R2, InstructionR2, 0, 0, 0)                                 \
  V(DOLLARGETS_OP, 74, R2, InstructionR2, 0, 0, 0)                             \
  /* Checks if stack top is null. */                                           \
  V(ISNULL_OP, 75, R0, InstructionR0, 1, 1, 0)                                 \
  /* Checks if stack top is logical. */                                        \
  V(ISLOGICAL_OP, 76, R0, InstructionR0, 1, 1, 0)                              \
  /* Checks if stack top is integer. */                                        \
  V(ISINTEGER_OP, 77, R0, InstructionR0, 1, 1, 0)                              \
  /* Checks if stack top is double. */                                         \
  V(ISDOUBLE_OP, 78, R0, InstructionR0, 1, 1, 0)                               \
  /* Checks if stack top is complex. */                                        \
  V(ISCOMPLEX_OP, 79, R0, InstructionR0, 1, 1, 0)                              \
  /* Checks if stack top is character. */                                      \
  V(ISCHARACTER_OP, 80, R0, InstructionR0, 1, 1, 0)                            \
  /* Checks if stack top is symbol. */                                         \
  V(ISSYMBOL_OP, 81, R0, InstructionR0, 1, 1, 0)                               \
  /* Checks if stack top is object. */                                         \
  V(ISOBJECT_OP, 82, R0, InstructionR0, 1, 1, 0)                               \
  /* CHecks if stack top is numeric. */                                        \
  V(ISNUMERIC_OP, 83, R0, InstructionR0, 1, 1, 0)                              \
  V(VECSUBSET_OP, 84, R1, InstructionR1, 0, 0, 0)                              \
  V(MATSUBSET_OP, 85, R1, InstructionR1, 0, 0, 0)                              \
  V(VECSUBASSIGN_OP, 86, R1, InstructionR1, 0, 0, 0)                           \
  V(MATSUBASSIGN_OP, 87, R1, InstructionR1, 0, 0, 0)                           \
  V(AND1ST_OP, 88, R2, InstructionR2, 0, 0, 2)                                 \
  V(AND2ND_OP, 89, R1, InstructionR1, 0, 0, 0)                                 \
  V(OR1ST_OP, 90, R2, InstructionR2, 0, 0, 2)                                  \
  V(OR2ND_OP, 91, R1, InstructionR1, 0, 0, 0)                                  \
  V(GETVAR_MISSOK_OP, 92, R1, InstructionR1, 0, 0, 0)                          \
  V(DDVAL_MISSOK_OP, 93, R1, InstructionR1, 0, 0, 0)                           \
  V(VISIBLE_OP, 94, R0, InstructionR0, 0, 0, 0)                                \
  V(SETVAR2_OP, 95, R1, InstructionR1, 0, 0, 0)                                \
  V(STARTASSIGN2_OP, 96, R1, InstructionR1, 0, 0, 0)                           \
  V(ENDASSIGN2_OP, 97, R1, InstructionR1, 0, 0, 0)                             \
  V(SETTER_CALL_OP, 98, R2, InstructionR2, 0, 0, 0)                            \
  V(GETTER_CALL_OP, 99, R1, InstructionR1, 0, 0, 0)                            \
  V(SWAP_OP, 100, R0, InstructionR0, 0, 0, 0)                                  \
  V(DUP2ND_OP, 101, R0, InstructionR0, 0, 0, 0)                                \
  V(SWITCH_OP, 102, R4, InstructionR4, 0, 0, 0)                                \
  V(RETURNJMP_OP, 103, R0, InstructionR0, 0, 0, 0)                             \
  V(STARTSUBSET_N_OP, 104, R2, InstructionR2, 0, 0, 0)                         \
  V(STARTSUBASSIGN_N_OP, 105, R2, InstructionR2, 0, 0, 0)                      \
  V(VECSUBSET2_OP, 106, R1, InstructionR1, 0, 0, 0)                            \
  V(MATSUBSET2_OP, 107, R1, InstructionR1, 0, 0, 0)                            \
  V(VECSUBASSIGN2_OP, 108, R1, InstructionR1, 0, 0, 0)                         \
  V(MATSUBASSIGN2_OP, 109, R1, InstructionR1, 0, 0, 0)                         \
  V(STARTSUBSET2_N_OP, 110, R2, InstructionR2, 0, 0, 0)                        \
  V(STARTSUBASSIGN2_N_OP, 111, R2, InstructionR2, 0, 0, 0)                     \
  V(SUBSET_N_OP, 112, R2, InstructionR2, 0, 0, 0)                              \
  V(SUBSET2_N_OP, 113, R2, InstructionR2, 0, 0, 0)                             \
  V(SUBASSIGN_N_OP, 114, R2, InstructionR2, 0, 0, 0)                           \
  V(SUBASSIGN2_N_OP, 115, R2, InstructionR2, 0, 0, 0)                          \
  V(LOG_OP, 116, R1, InstructionR1, 0, 0, 0)                                   \
  V(LOGBASE_OP, 117, R1, InstructionR1, 0, 0, 0)                               \
  V(MATH1_OP, 118, R2, InstructionR2, 0, 0, 0)                                 \
  V(DOTCALL_OP, 119, R2, InstructionR2, 0, 0, 0)                               \
  V(COLON_OP, 120, R1, InstructionR1, 0, 0, 0)                                 \
  V(SEQALONG_OP, 121, R1, InstructionR1, 0, 0, 0)                              \
  V(SEQLEN_OP, 122, R1, InstructionR1, 0, 0, 0)                                \
  /* Creates a promise out of given argument and pushes it on the stack. This  \
     works in a similar manner to MAKEPROM_OP, but is deterministic and        \
     always pushes the promise on the stack. */                                \
  V(CREATE_PROMISE_OP, 123, R1, InstructionR1, 0, 1, 0)

namespace reactor {

/** Changed to integer because R opcodes seem to use integers as their
 * representation. */
typedef std::int32_t OpcodeRepr;

/** R arguments have the same storage type as their opcodes. */
typedef OpcodeRepr RArgRepr;

/** Enum of all bytecode opcodes FlaiR understands. Do not add new opcodes for
 * instructions here, but use the opcodes.h file instead.
*/
enum class Opcode : OpcodeRepr {
#define OPCODE_ENUM(name, id, ...) name = id,
  R_OPCODE_LIST(OPCODE_ENUM) OPCODES_ALL_ // last opcode and marker for last
                                          // opcode id, also marker for all
                                          // opcodes
};

const unsigned NUM_OPCODES = static_cast<unsigned>(Opcode::OPCODES_ALL_);

namespace encoding {
#pragma pack(push, 1) // disable padding

// ------------------------------------------------------------------------------------------------------------
/** Basic flair instruction encoding type.
 *
 * Contains only the opcode of the instruction.
 */
class OpcodeOnly {
public:
  Opcode opcode;
  OpcodeOnly(Opcode opcode) : opcode(opcode) {}
  /** Since R instruction sizes are defined in terms of opcode lengths and not
   * byte lengths, sizeof operator cannot be used and the encoding::size
   * constant should rather be used.
   */
  static const size_t size = 1;
};

static_assert(OpcodeOnly::size == sizeof(OpcodeOnly) / sizeof(OpcodeRepr),
              "Invalid encoding size for OpcodeOnly");

#pragma pack(pop)
} // namespace encoding

} // namespace reactor

#endif
