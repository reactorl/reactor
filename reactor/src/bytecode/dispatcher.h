#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <type_traits>

#include "reactor.h"
#include "code_buffer.h"
#include "predicates.h"

namespace reactor {

class BaseDispatcher {};

template <typename DISPATCHER, typename PASS, typename SWITCH>
class Dispatcher : public BaseDispatcher {
public:
  typedef PASS Driver;

  bool dispatch(PASS &pass, CodeBuffer const &code, size_t pc) {
    size_t index = SWITCH::getSwitchValue(code, pc);
    Record *d = dispatchTable_[index];
    if (d != nullptr)
      return d->dispatch(pass, *reinterpret_cast<DISPATCHER *>(this), code, pc);
    else
      return false;
  }

protected:
  typedef void (DISPATCHER::*HandlerPtr)(PASS &, encoding::OpcodeOnly const &);
  typedef bool (*PredicatePtr)(BaseDispatcher &, CodeBuffer const &, size_t pc);

  template <typename PREDICATE_T, typename ARGUMENT>
  static void bind(void (DISPATCHER::*handler)(PASS &, ARGUMENT)) {
    typedef typename std::remove_const<
        typename std::remove_reference<ARGUMENT>::type>::type handler_arg_t;
    static_assert(
        std::is_base_of<handler_arg_t, typename PREDICATE_T::ARG>::value,
        "bind: Handler signature mismatch with given instruction predicate");
    HandlerPtr h = reinterpret_cast<HandlerPtr>(handler);
    std::vector<size_t> const &matches = PREDICATE_T::matches();
    if (matches.size() == 1 and matches[0] == SWITCH::last) {
      for (unsigned i = 0; i < SWITCH::last; ++i)
        bindFor<PREDICATE_T>(i, h);
    } else {
      for (size_t op : matches)
        bindFor<PREDICATE_T>(op, h);
    }
  }

private:
  class Record {
  public:
    HandlerPtr const hptr;
    unsigned const matchSize;
    PredicatePtr const condition;
    Record *next;

    template <typename PREDICATE_T> static Record *create(HandlerPtr hptr) {
      size_t matchSize = PREDICATE_T::matches().size();
      if (matchSize == 1 and PREDICATE_T::matches()[0] == SWITCH::last)
        matchSize = SWITCH::last;
      return new Record(hptr, matchSize, PREDICATE_T::isConditional()
                                             ? PREDICATE_T::condition
                                             : nullptr);
    }

    ~Record() { delete next; }

    bool isConditional() const { return condition != nullptr; }

    bool dispatch(PASS &pass, DISPATCHER &handler, CodeBuffer const &code,
                  size_t pc) {
      if (not isConditional() or condition(handler, code, pc)) {
        (handler.*hptr)(pass, code.get<encoding::OpcodeOnly>(pc));
        return true;
      } else {
        return next == nullptr ? false
                               : next->dispatch(pass, handler, code, pc);
      }
    }

  private:
    Record(HandlerPtr hptr, size_t matchSize, PredicatePtr predicate)
        : hptr(hptr), matchSize(matchSize), condition(predicate),
          next(nullptr) {}
  };

  static void insertRecord(size_t index, Record *prev, Record *x) {
    if (prev == nullptr) {
      x->next = dispatchTable()[index];
      dispatchTable()[index] = x;
    } else {
      x->next = prev->next;
      prev->next = x;
    }
  }

  /** Bind the given handler to selected switch value.
    */
  template <typename PREDICATE_T>
  static void bindFor(size_t i, HandlerPtr handler) {
    if (dispatchTable()[i] == nullptr) {
      dispatchTable()[i] = Record::template create<PREDICATE_T>(handler);
    } else {
      Record *prev = nullptr;
      Record *current = dispatchTable()[i];
      Record *x = Record::template create<PREDICATE_T>(handler);
      while (current != nullptr) {
        // conditional ones first, ordered by increased matchSize
        if (x->isConditional()) {
          if (not current->isConditional() or
              current->matchSize > x->matchSize) {
            insertRecord(i, prev, x);
            return;
          }
          // then unconditional
        } else {
          if (not current->isConditional() and
              current->matchSize > x->matchSize) {
            insertRecord(i, prev, x);
            return;
          }
        }
        prev = current;
        current = current->next;
      }
      insertRecord(i, prev, x);
    }
  }

  static Record **dispatchTable() {
    static Record *table[SWITCH::last];
    return table;
  }

  static Record **createDispatchTable() {
    // call the initialize method of the template - this fills in all the
    // handlers & predicates once for all
    DISPATCHER::initialize();
    return dispatchTable();
  }

  /** Shorthand for the dispatch table. Since the shorthand is static, it also
   * triggers the initialization and registration of the handler methods when
   * appropriate.
   */
  static Record **const dispatchTable_;
};

template <typename HANDLER, typename PASS, typename SWITCH>
typename Dispatcher<HANDLER, PASS, SWITCH>::Record **const
    Dispatcher<HANDLER, PASS, SWITCH>::dispatchTable_ =
        Dispatcher<HANDLER, PASS, SWITCH>::createDispatchTable();

namespace ins {
class Switch {
public:
  typedef Opcode type;

  static const size_t last = static_cast<size_t>(Opcode::OPCODES_ALL_);

  static size_t getSwitchValue(CodeBuffer const &code, size_t pc) {
    return static_cast<size_t>(code.get<encoding::OpcodeOnly>(pc).opcode);
  }
};

template <typename HANDLER, typename PASS>
class Dispatcher : public ::reactor::Dispatcher<HANDLER, PASS, Switch> {};

} // nmespace ins

} // namespace reactor

#endif // DISPATCHER_H
