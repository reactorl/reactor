#ifndef CODE_BUFFER_H
#define CODE_BUFFER_H

#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "opcodes.h"

#include "r/vector.h"
#include "reporting.h"
#include "instruction.h"

#include "assert.h"

namespace reactor {

/** Given an already compiled function, returns its bytecode.
 */
inline SEXP functionBytecode(SEXP f) {
  SEXP body = BODY(f);
  assert_(TYPEOF(body) == BCODESXP);
  return body;
}

/** Code buffer
 */
class CodeBuffer {
public:
  class Code {
  public:
    const int BYTECODE_VERSION = 0x08;

    /** Creates an empty bytecode vector.

     The bytecode object will have size of 1 and will contain the bytecode
     version number as its only entry so that all fixup routines work properly.
     */
    Code() { code_.push_back(BYTECODE_VERSION); }

    /** Creates bytecode vector from existing R bytecode vector.
     */
    Code(const SEXP from) {
      const IntVector from_(from);
      code_.resize(from_.size());
      OpcodeRepr *i = code_.data();
      for (auto fi : from_)
        *(i++) = fi;
    }

    /** Returns the contents of the bytecode vector starting at index pc,
     * typecasted to the specified instruction encoding.
     */
    template <typename INS_ENCODING> INS_ENCODING &get(size_t pc) {
      return *(reinterpret_cast<INS_ENCODING *>(code_.data() + pc));
    }

    /** Returns the contents of the bytecode vector starting at index pc,
     * typecasted to the specified instruction encoding.
     */
    template <typename INS_ENCODING> INS_ENCODING const &get(size_t pc) const {
      return *(reinterpret_cast<INS_ENCODING const *>(code_.data() + pc));
    }

    template <typename INS_ENCODING>
    INS_ENCODING const &get(size_t pc, int diff) const {
      return *(reinterpret_cast<INS_ENCODING const *>(code_.data() + pc +
                                                      diff));
    }

    /** Returns the size of instruction at given pc.
     */
    size_t instructionSize(size_t pc) const {
      return Instruction::get(get<encoding::OpcodeOnly>(pc).opcode).size();
    }

    /** Appends given instruction to the bytecode vector and returns the index
     * at which the instruction starts.
     */
    template <typename INS_ENCODING> size_t append(INS_ENCODING const &ins) {
      size_t start = code_.size();
      code_.resize(code_.size() + INS_ENCODING::size);
      get<INS_ENCODING>(start) = ins;
      return start;
    }

    /** Inserts given instruction to the bytecode vector at given index and
     * returns the same index, i.e. the pc of the inserted instruction.
     */
    template <typename INS_ENCODING>
    size_t insert(size_t index, INS_ENCODING const &ins) {
      assert_(!isBytecodeIdentifierPos(index));
      OpcodeRepr const *start = reinterpret_cast<OpcodeRepr const *>(&ins);
      code_.insert(code_.begin() + index, start, start + INS_ENCODING::size);
      return index;
    }

    void erase(size_t index) {
      assert_(!isBytecodeIdentifierPos(index));
      auto start = code_.begin() + index;
      code_.erase(
          start,
          start + Instruction::get(static_cast<Opcode>(code_[index])).size());
    }

    /** Inserts given instruction into an already changed code.

      The index is the address where the instruction should have been inserted
      in the original code. Diff is the difference in the old code vs the
      updated one. Inserts the instruction at the appropriate location in the
      updated code (and returns the address) and updates the diff accordingly so
      that it can be used for any instructions afterwards.

      DEPRECATED
     */
    template <typename INS_ENCODING>
    size_t insert(size_t index, int &diff, INS_ENCODING const &ins) {
      OpcodeRepr const *start = reinterpret_cast<OpcodeRepr const *>(&ins);
      index += diff;
      assert_(!isBytecodeIdentifierPos(index));
      code_.insert(code_.begin() + index, start, start + INS_ENCODING::size);
      diff += Instruction::get(ins.opcode).size();
      return index;
    }

    /** Inserts all contents (modulo the version identifier) of the given code
     * at specified location.
     */
    size_t insert(size_t index, Code const &other) {
      assert_(not isBytecodeIdentifierPos(index));
      code_.insert(code_.begin() + index, other.code_.begin() + 1,
                   other.code_.end());
      return index;
    }

    /** Erases the instruction from already updated code.

      The index is the address where the instruction was in the old code, diff
      is the offset at its pc. Removes the instruction and updates the offset
      for all instructions with higher counters.
     */
    size_t erase(size_t index, int &diff) {
      index += diff;
      assert_(!isBytecodeIdentifierPos(index));
      auto start = code_.begin() + index;
      size_t size = Instruction::get(static_cast<Opcode>(code_[index])).size();
      code_.erase(start, start + size);
      diff -= size;
      return index;
    }

    /** Replaces existing instruction in updated code with a new one.

      Updates the diff argument for consequent instructions and returns the
      actual address of the new instruction in the updated code.
     */
    template <typename INS_ENCODING>
    size_t replace(size_t index, int &diff, INS_ENCODING const &ins) {
      size_t result = insert(index, diff, ins);
      erase(index, diff);
      return result;
    }

    /** Returns the opcode of the instruction at given address.
     */
    Opcode opcode(size_t index) const {
      return get<encoding::OpcodeOnly>(index).opcode;
    }

    Opcode opcode(size_t index, int diff) const {
      return get<encoding::OpcodeOnly>(index + diff).opcode;
    }

    operator SEXP() const {
      IntVector result(code_.size());
      for (size_t i = 0; i < code_.size(); ++i)
        result.set(i, code_[i]);
      return result;
    }

    size_t size() const { return code_.size(); }

    OpcodeRepr const &operator[](size_t index) const { return code_[index]; }

    OpcodeRepr &operator[](size_t index) { return code_[index]; }

  private:
    bool isBytecodeIdentifierPos(size_t index) { return index == 0; }

    std::vector<OpcodeRepr> code_;

  }; // Code

  /** A constant pool for the R bytecode buffer.

   TODO - check that this is GC safe and that we do not have to
   protect/unprotect the constants when we create new ones.
   */
  class Constants {
  public:
    /** Creates an empty constant pool object.
     */
    Constants() {}

    /** Creates a constant pool object from an existing const pool R object.

     Copies all constant SEXPs from the R's constant pool SEXP to the created
     constant pool object.
     */
    Constants(const SEXP from) {
      GenericVector from_(from);
      values_.resize(from_.size());
      SEXP *i = values_.data();
      SEXP *start = i;
      for (SEXP fi : from_) {
        *(i) = fi;
        valuesMap_[fi] = i - start;
        ++i;
      }
    }

    /** Returns the number of constants defined in the constant pool.
     */
    size_t size() const { return values_.size(); }

    /** Adds the given SEXP as a constant and returns its index.

     If same value already exists in the constant pool, returns the index of the
     constant, otherwise creates a new entry and returns its index.
     */
    unsigned add(SEXP constant) {
      auto i = valuesMap_.find(constant);
      if (i == valuesMap_.end()) {
        unsigned result = values_.size();
        valuesMap_[constant] = result;
        values_.push_back(constant);
        return result;
      } else {
        return i->second;
      }
    }

    /** Converts the constant pool to a list of constant pool SEXPs.
     */
    operator SEXP() const {
      GenericVector result(values_.size());
      for (size_t i = 0; i < values_.size(); ++i)
        result.set(i, values_[i]);
      return result;
    }

    size_t indexOf(SEXP constant) {
      auto i = valuesMap_.find(constant);
      return i->second;
    }

    /** Returns the index-th constant as SEXP.
     */
    SEXP &operator[](size_t index) { return values_[index]; }

    /** Returns the index-th constant as SEXP.
     */
    SEXP const &operator[](size_t index) const { return values_[index]; }

    std::vector<SEXP>::iterator begin() { return values_.begin(); }

    std::vector<SEXP>::const_iterator begin() const { return values_.begin(); }

    std::vector<SEXP>::iterator end() { return values_.end(); }

    std::vector<SEXP>::const_iterator end() const { return values_.end(); }

  private:
    struct sexp_eq_hash {
      size_t operator()(const SEXP &s) const;
    };

    std::unordered_map<SEXP, unsigned, sexp_eq_hash, sexp_equal_to> valuesMap_;

    std::vector<SEXP> values_;
  }; // Constants

public:
  /** Creates an empty code buffer.
   */
  CodeBuffer() {}

  CodeBuffer(const CodeBuffer &) = delete;

  /** Creates a code buffer from the given SEXP pointer.

    The SEXP can be either a bytecode, or a closure.
   */
  CodeBuffer(SEXP from)
      : code(R_bcDecode(BCODE_CODE(
            TYPEOF(from) == CLOSXP ? functionBytecode(from) : from))),
        constants(BCODE_CONSTS(TYPEOF(from) == CLOSXP ? functionBytecode(from)
                                                      : from)),
        closure(TYPEOF(from) == CLOSXP ? from : nullptr) {}

  /** Coverts the code buffer to a SEXP with BCODESEXP SEXPTYPE.
   */
  operator SEXP() const {
    Protector p;
    SEXP cs = p(constants);
    SEXP bc = p(code);
    SEXP bce = p(Rf_cons(p(R_bcEncode(bc)), cs));
    SET_TYPEOF(bce, BCODESXP);
    return bce;
  }

  bool operator==(CodeBuffer const &other) { return this == &other; }

  bool operator!=(CodeBuffer const &other) { return this != &other; }

  template <typename INS_ENCODING> INS_ENCODING &get(size_t pc) {
    return code.get<INS_ENCODING>(pc);
  }

  template <typename INS_ENCODING> INS_ENCODING const &get(size_t pc) const {
    return code.get<INS_ENCODING>(pc);
  }

  size_t size() { return code.size(); }

  /** Code vector of the buffer. Supports inserting, appending and erasing
   * instructions.
   */
  Code code;

  /** Constant pool of the buffer.
   */
  Constants constants;

  /** SEXP to closure to which the bytecode belongs, or nullptr if the bytecode
     does not correspond to any closure.
   */
  SEXP closure;

}; // CodeBuffer

} // namespace reactor

#endif // CODE_BUFFER_H
