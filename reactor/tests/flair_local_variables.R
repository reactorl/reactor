require("reactor")
require("compiler")

checkLocals = function(f, expected) {
		stopifnot(sort(flair.locals(f)) == sort(expected))
}


# only arguments




f = cmpfun(function(a, b, c, d) 2)
checkLocals(f, c("a", "b", "c", "d"))

# only write

f = cmpfun(function() a = 2)
checkLocals(f, c("a"))

# subset write

f = cmpfun(function() a[2] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[b] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[f()] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[b <- 2] = 2)
checkLocals(f, c("a", "b"))

f = cmpfun(function() a[3, b <- 2] = 2)
checkLocals(f, c("a", "b"))

# subscript write

f = cmpfun(function() a[[2]] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[[b]] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[[f()]] = 2)
checkLocals(f, c("a"))

f = cmpfun(function() a[[b <- 2]] = 2)
checkLocals(f, c("a", "b"))

f = cmpfun(function() a[[3, b <- 2]] = 2)
checkLocals(f, c("a", "b"))

# dollar write

f = cmpfun(function() a$b = 3)
checkLocals(f, c("a"))

# conditional write

f = cmpfun(function() if (a) b <- 1)
checkLocals(f, c("b"))

# complex assignments

f = cmpfun(function() names(a) <- 6)
checkLocals(f, c("a"))

f = cmpfun(function() names(a) <- b)
checkLocals(f, c("a"))

f = cmpfun(function() c(b(a)) <- 5)
checkLocals(f, c("a"))

f = cmpfun(function(a, b) for(i in a) if (b) c())
checkLocals(f, c("a", "b", "i"))
