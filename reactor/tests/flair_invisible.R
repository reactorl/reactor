require("reactor")

# Function for which raptor will generate extraneous INV_OPs.
f <- function(a) {
	b <- a
	c <- 1
	2
}

fc <- compileFun.raptoR(f)

# Show OP_CODES.
flair.disassemble(fc)

# Scrub unnecessary INV_OPs.
fc = flair.invisible(fc)

# Show scrubbed OP_CODES again.
flair.disassemble(fc)
