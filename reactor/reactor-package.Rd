\name{reactor-package}
\alias{reactor-package}
\alias{reactor}
\docType{package}
\title{
\packageTitle{reactor}
}
\description{
\packageDescription{reactor}
}
\details{

The DESCRIPTION file:
\packageDESCRIPTION{reactor}
\packageIndices{reactor}
~~ An overview of how to use the package, including the most important ~~
~~ functions ~~
}
\author{
\packageAuthor{reactor}

Maintainer: \packageMaintainer{reactor}
}
\references{
~~ Literature or other references for background information ~~
}
~~ Optionally other standard keywords, one per line, from file KEYWORDS in ~~
~~ the R documentation directory ~~
\keyword{ package }
\seealso{
~~ Optional links to other man pages, e.g. ~~
~~ \code{\link[<pkg>:<pkg>-package]{<pkg>}} ~~
}
\examples{
%% ~~ simple examples of the most important functions ~~
}
